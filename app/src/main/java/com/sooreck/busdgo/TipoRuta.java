package com.sooreck.busdgo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 16/06/2016.
 */
public class TipoRuta {

    private String nombre;
    private String colorAutobus;
    private int imagen;

    public TipoRuta(String nombre, String color, int imagen){

        this.nombre = nombre;
        this.colorAutobus = color;
        this.imagen = imagen;

    }

    public static final List<TipoRuta> TIPOSRUTAS = new ArrayList<>();

    static {
        TIPOSRUTAS.add(new TipoRuta("Rutas Azul/Amarillo", "Azul/Amarillo", R.drawable.header));
        TIPOSRUTAS.add(new TipoRuta("Rutas Azul/Rojo", "Azul/Rojo", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Libertad/Nazas", "Azul/Blanco", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Blancas", "Blanco", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Amarillas", "Amarillo", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Naranjas", "Naranja", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas 20 de Nov", "Azul", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Cafes", "Cafe", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Doradas", "Dorado", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Rojas", "Rojo", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Verdes", "Verde", R.drawable.header2));
        TIPOSRUTAS.add(new TipoRuta("Rutas Azules", "Azul", R.drawable.header2));
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColorAutobus() {
        return colorAutobus;
    }

    public void setColorAutobus(String color) {
        this.colorAutobus = color;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}
