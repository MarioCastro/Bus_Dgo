package com.sooreck.busdgo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by SoOreck on 28/06/2017.
 */
public class AdaptadorRutasFavoritas extends RecyclerView.Adapter<AdaptadorRutasFavoritas.ViewHolder> implements View.OnClickListener, onMoveAndSwipedListener{

    private final List<String> items;
    private View.OnClickListener listener;
    private Context context;
    private TextView header;
    private FragmentActivity fActivity;


    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(items, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        SharedPreferences prefs =  context.getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
        String rutasfavs = prefs.getString("rutas","");
        ArrayList<String> rutas;
        if (rutasfavs.equals("")){
            rutas = new ArrayList<>();
        }
        else {
            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
            rutas = new Gson().fromJson(rutasfavs,listType);
        }
        SharedPreferences.Editor edit = prefs.edit();
        Collections.swap(rutas, fromPosition, toPosition);
        String jsonFinal = new Gson().toJson(rutas);
        edit.putString("rutas",jsonFinal);
        edit.apply();
        return true;
    }

    @Override
    public void onItemDismiss(final int position) {
        final String r = items.get(position);
        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.title_dialogo_eliminar_fav))
                .setMessage(context.getString(R.string.info_text_dialogo_eliminar_fav)
                        +" "+items.get(position)+" "+ context.getString(R.string.info_text_dialogo_eliminar_fav_2))
                .setPositiveButton(context.getString(R.string.button_si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences prefs =  context.getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
                        String rutasfavs = prefs.getString("rutas","");
                        ArrayList<String> rutas;
                        if (rutasfavs.equals("")){
                            rutas = new ArrayList<>();
                        }
                        else {
                            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
                            rutas = new Gson().fromJson(rutasfavs,listType);
                        }
                        SharedPreferences.Editor edit = prefs.edit();
                        remove(rutas,items.get(position));
                        items.remove(position);
                        String jsonFinal = new Gson().toJson(rutas);
                        edit.putString("rutas",jsonFinal);
                        edit.apply();
                        notifyItemRemoved(position);
                        header.setText(items.size()+" "+context.getString(R.string.titulo_fragmento_rutas_fav));

                    }
                })
                .setNegativeButton(context.getString(R.string.button_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        items.remove(position);
                        notifyItemRemoved(position);
                        addItem(position,r);
                    }
                })
                .show();
    }

    public void addItem(int position, String r) {
        items.add(position, r);
        notifyItemInserted(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        private View mView;
        public TextView nombre;
        private ImageButton i_derecho;
        private int position;

        public ViewHolder(View v) {
            super(v);
            mView = v;
            nombre = (TextView) v.findViewById(R.id.nombre_de_ruta);
            i_derecho = (ImageButton)v.findViewById(R.id.img_ruta_event);
        }
    }


    public AdaptadorRutasFavoritas(List<String> items, Context context, TextView tv,FragmentActivity fa) {
        this.items = items;
        this.context = context;
        this.header = tv;
        this.fActivity = fa;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_lista_favoritos, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        viewHolder.position = i;
        String item = items.get(i);
        viewHolder.nombre.setText(item);
        viewHolder.i_derecho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragmentoGenerico;
                FragmentManager fragmentManager = fActivity.getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

                fragmentoGenerico = new FragmentoRutaMapa();

                Bundle bundle = new Bundle();
                bundle.putString("ruta", String.valueOf(items.get(viewHolder.getAdapterPosition())));
                fragmentoGenerico.setArguments(bundle);

                if (fragmentoGenerico != null) {
                    ft.replace(R.id.contenedor_principal, fragmentoGenerico,"FragmentoRutaMapa").addToBackStack(null).commit();

                }
            }
        });

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.anim_recycler_item_show);
        viewHolder.mView.startAnimation(animation);

    }

    private void remove(ArrayList<String> list, String ruta){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).equals(ruta)){
                list.remove(i);
            }
        }
    }


}
