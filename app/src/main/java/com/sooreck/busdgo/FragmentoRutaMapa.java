package com.sooreck.busdgo;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by SoOreck on 17/06/2016.
 */
public class FragmentoRutaMapa extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener{


    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorItem adaptador;

    private static final int LOCATION_REQUEST_CODE = 1;
    GoogleMap mapa;
    LatLng exampleLocation;
    EditText etnewdirection;

    MarkerOptions nuevascoordenadas;
    Marker longclick;

    Circle marcador;

    Polyline rutarClickeada;
    int colorRuta;
    float widtRuta;

    Ruta rutaObtenida;
    private boolean pintoRuta = false;

    private int heigthHeaderToolsMapa;

    private View view;

    private ArrayList<Marker> paradas = new ArrayList<>();
    private ArrayList<MarkerOptions> paradasOptions = new ArrayList<>();
    private boolean paradasMostradas = true;

    public FragmentoRutaMapa() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmento_ruta_mapa, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
        ActividadPrincipal.showItemMenu(false,true);

        reciclador = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
        reciclador.setLayoutManager(layoutManager);

        //etnewdirection = (EditText) view.findViewById(R.id.et_nueva_direccion);
        TextView tvRuta = (TextView) view.findViewById(R.id.TvRuta);

        Bundle args = getArguments();
        if (args != null) {
            String rutaAObtener = args.getString("ruta"); /*IdentificadorRuta*/
            tvRuta.setText(rutaAObtener);
            rutaObtenida = new Ruta();
            rutaObtenida = rutaObtenida.dameRuta(rutaAObtener);

            if (rutaObtenida != null) {
                Toast.makeText(getActivity(), rutaObtenida.getNombre(), Toast.LENGTH_SHORT).show();
                pintoRuta = true;
            } else Toast.makeText(getActivity(), String.valueOf("null"), Toast.LENGTH_SHORT).show();
            ActividadPrincipal.switchIcon(isFav());
        }

        heigthHeaderToolsMapa = ((LinearLayout) view.findViewById(R.id.headerToolsRutaMapa)).getHeight();
        return view;
    }


    @Override
    public void onMapReady(final GoogleMap map) {
        //Toast.makeText(getActivity(), String.valueOf("Iniciando Mapa"), Toast.LENGTH_SHORT).show();
        mapa = map;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapa.setOnMarkerClickListener(this);
        mapa.setOnMapClickListener(this);
        float densidad = getResources().getDisplayMetrics().density;
        mapa.setPadding(0,(heigthHeaderToolsMapa+((int)(88*densidad))),0,0);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Mostrar diálogo explicativo
            } else {
                // Solicitar permiso
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }
        }
        //map.setMyLocationEnabled(true);

        if (pintoRuta) {
            marcarRuta(map);
        }

       /* exampleLocation = new LatLng(24.009232, -104.661077);
        map.addMarker(new MarkerOptions().position(exampleLocation)
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                .title("Ejemplo de localizacion")
                .snippet("Here!")
                .draggable(false)
        );*/
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(24.009232, -104.661077), 13.0f));

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(final LatLng point) {
                if (longclick != null){
                    longclick.remove();
                    longclick = null;
                }
                if (marcador != null){
                    marcador.remove();
                    marcador = null;
                }

                Projection proj = map.getProjection();
                Point coord = proj.toScreenLocation(point);

               /* Toast.makeText(
                        getActivity(),
                                "Lat: " + point.latitude + "\n" +
                                "Lng: " + point.longitude + "\n" +
                                "X: " + coord.x + " - Y: " + coord.y,
                        Toast.LENGTH_SHORT).show();*/
                nuevascoordenadas = new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                        .title("Nuevo punto")
                        .snippet("prosigue la ruta")
                        .draggable(false);
                longclick = map.addMarker(nuevascoordenadas);
                pintarCirculo(new LatLng(point.latitude, point.longitude),40f);
                //etnewdirection.setText("");
                //etnewdirection.setText(String.valueOf(point.latitude) + ", " + String.valueOf(point.longitude));

            }
        });

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    Location location = map.getMyLocation();
                   // etnewdirection.setText(String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));

                } catch (NullPointerException ex) {
                    Toast.makeText(
                            getActivity(),
                            "Revisa que este activado tu GPS\n" +
                                    "e intenta de nuevo",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        map.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
            @Override
            public void onPolylineClick(Polyline polyline) {
                rutarClickeada = polyline;
                colorRuta = rutarClickeada.getColor();
                widtRuta = rutarClickeada.getWidth();
                rutarClickeada.setColor(Color.rgb(255,255,255));
            }
        });
    }

    /***********************************************************************************************
     **************** METODO PARA MARCAR LA RUTA RECIBIDA DEL FRAGMENTO ANTERIOR ******************/
    private void marcarRuta(GoogleMap map) {

        map.addMarker(rutaObtenida.getInicio());
        map.addMarker(rutaObtenida.getFin());
        map.addMarker(rutaObtenida.getBase());
        map.addPolyline(rutaObtenida.getRutaVuelta());
        map.addPolyline(rutaObtenida.getRutaIda());

        if (rutaObtenida.getIdentificador().equals("4")){
           Ruta r1 = new Ruta();
            map.addPolyline(r1.dameRutaById("4R").getRutaIda());
            //todo simbologia de retorno items.add(new Item("Inicio",R.drawable.ic_inicio) );
        }
        if (rutaObtenida.getIdentificador().equals("5")){
            Ruta r1 = new Ruta();
            map.addPolyline(r1.dameRutaById("5R").getRutaIda());}

        final ArrayList<Item> items = new ArrayList<>();
        if (rutaObtenida.getParadasOficiales().size() >= 1){
            items.add(new Item("Paradas",R.drawable.ic_paradas,rutaObtenida.getParadasOficiales().get(0) ) );

            for (int i=0; i<rutaObtenida.getParadasOficiales().size(); i++){
                MarkerOptions parada = new MarkerOptions()
                        .position(rutaObtenida.getParadasOficiales().get(i))
                        .title("Parada")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_paradas));
                Marker markerNew = map.addMarker(parada);
                paradas.add(markerNew);
                paradasOptions.add(parada);
            }
        }
        items.add(new Item("Inicio",R.drawable.ic_inicio,rutaObtenida.getInicio().getPosition()) );
        items.add(new Item("Fin",R.drawable.ic_fin,rutaObtenida.getFin().getPosition()) );
        items.add(new Item("Base",R.drawable.ic_base,rutaObtenida.getBase().getPosition()) );
        items.add(new Item("Ruta Inicio",R.drawable.ic_ruta_ida,rutaObtenida.getRutaIda().getPoints().get(0) ) );
        items.add(new Item("Ruta Regreso",R.drawable.ic_ruta_vuelta,rutaObtenida.getRutaVuelta().getPoints().get(0)) );

        adaptador = new AdaptadorItem(items);
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = reciclador.getChildAdapterPosition(v);
                if (posicion == 0){
                    showParadas(!paradasMostradas);
                }else{
                    LatLng latlong = items.get(posicion).getPuntoMarcador();
                    mapa.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong,17), 1000, null);
                    pintarCirculo(latlong,40);
                }
            }
        });
        reciclador.setAdapter(adaptador);

    }

    /***********************************************************************************************
     ****************** EVENTO QUE SE EJECUTA AL RECIBIR UNA RESPUESTA DE PERMISOS ****************/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            // ¿Permisos asignados?
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mapa.setMyLocationEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Error de permisos", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /***********************************************************************************************
     ******************* METODO PARA PINTAR CIRCULO AL REDEDOR DE UN MARCADOR *********************/
    private void pintarCirculo(LatLng center, double radius){
        if (marcador != null){
            marcador.remove();
            marcador = null;
        }
        CircleOptions circleOptions = new CircleOptions()
                .center(center)
                .radius(radius)
                .strokeColor(Color.parseColor("#FFFFFF"))
                .strokeWidth(4)
                .fillColor(Color.argb(32, 33, 150, 243));
        marcador = mapa.addCircle(circleOptions);
    }

    /***********************************************************************************************
     ********** EVENTO QUE SE EJECUTA AL DAR CLIK EN UN MARCADOR PARA PINTAR UN CIRCULO ***********/
    @Override
    public boolean onMarkerClick(Marker marker) {
        LatLng pos = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);
        pintarCirculo(pos,40);
        return false;
    }

    /***********************************************************************************************
     ******** EVENTO QUE SE EJECUTA AL DAR CLIK EN EL MAPA PARA QUITAR EL CIRCULO SI EXISTE *******/
    @Override
    public void onMapClick(LatLng latLng) {
        if (marcador != null){
            marcador.remove();
            marcador = null;
        }
        if (rutarClickeada != null){
            rutarClickeada.setColor(colorRuta);
            rutarClickeada.setWidth(widtRuta);
            rutarClickeada = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ActividadPrincipal.showItemMenu(false,false);
        ActividadPrincipal.switchIcon(isFav());
    }

    public void favoritosEvent(final MenuItem item){

        String ruta = rutaObtenida.getNombre();
        SharedPreferences prefs =  getActivity().getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
        String rutasfavs = prefs.getString("rutas","");
        ArrayList<String> rutas;
        if (rutasfavs.equals("")){
            rutas = new ArrayList<>();
        }
        else {
            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
            rutas = new Gson().fromJson(rutasfavs,listType);
        }
        SharedPreferences.Editor edit = prefs.edit();

        if (isFav()){
            item.setIcon(R.drawable.ic_star_outline_white_24dp);
            remove(rutas,ruta);
            Snackbar.make(view, "Ruta eliminada de favoritos", Snackbar.LENGTH_LONG)
                    .setAction("Deshacer", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            favoritosEvent(item);
                        }
                    })
                    .show();
        } else {
            item.setIcon(R.drawable.ic_star_white_24dp);
            rutas.add(ruta);
            Snackbar.make(view, "Ruta agregada a favoritos", Snackbar.LENGTH_LONG)
                    .setAction("Deshacer", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            favoritosEvent(item);
                        }
                    })
                    .show();
        }
        String jsonFinal = new Gson().toJson(rutas);
        edit.putString("rutas",jsonFinal);
        edit.apply();
    }

    public boolean isFav(){
        String ruta = rutaObtenida.getNombre();
        SharedPreferences prefs =  getActivity().getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
        String rutasfavs = prefs.getString("rutas","");
        ArrayList<String> rutas;
        if (rutasfavs.equals("")){
            return false;
        }
        else {
            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
            rutas = new Gson().fromJson(rutasfavs,listType);
        }
        return contains(rutas,ruta);

    }
    private boolean contains(ArrayList<String> list, String ruta){
        for (String s : list){
            if (s.equals(ruta)){
                return true;
            }
        }
        return false;
    }
    private void remove(ArrayList<String> list, String ruta){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).equals(ruta)){
                list.remove(i);
            }
        }
    }
    private void showParadas(boolean show){
        if (show){
            paradas.clear();
            for (MarkerOptions m : paradasOptions){
                paradas.add(mapa.addMarker(m));
            }
        }else{
            for (Marker m : paradas)
                m.remove();
        }
        paradasMostradas = !paradasMostradas;
    }
}//termina clase
