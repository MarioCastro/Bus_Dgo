package com.sooreck.busdgo;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by SoOreck on 21/08/2016.
 */
public class Item {

    private String titulo;
    private int imagen;
    LatLng puntoMarcador;

    public Item(){

    }

    public Item(String titulo, int imagen, LatLng puntoMarcador){
        this.titulo = titulo;
        this.imagen = imagen;
        this.puntoMarcador = puntoMarcador;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getImagen() {
        return imagen;
    }

    public LatLng getPuntoMarcador(){ return puntoMarcador; }
}
