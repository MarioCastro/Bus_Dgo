package com.sooreck.busdgo;

/**
 * Created by SoOreck on 28/06/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Fragmento que contiene las rutas favoritas agregadas por el usuario
 */

public class FragmentoRutasFavoritas extends Fragment {
    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorRutasFavoritas adaptador;
    private TextView header;

    private List<String> items;

    Ruta rutaObtenida;

    public FragmentoRutasFavoritas() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragmento_rutas_favoritas, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        reciclador.setLayoutManager(layoutManager);
        header = (TextView) view.findViewById(R.id.header_rutas_fav);
        //rutaObtenida = new Ruta();

        items = new ArrayList<>();

        SharedPreferences prefs =  getActivity().getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
        String rutasfavs = prefs.getString("rutas","");
        ArrayList<String> rutas;

        if (!rutasfavs.equals("")){
            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
            rutas = new Gson().fromJson(rutasfavs,listType);
            for (int i=0; i<rutas.size(); i++){
                //rutaObtenida = rutaObtenida.dameRutaById(rutas.get(i));
                items.add(rutas.get(i));
            }
        }
        else {
            items.add("No hay rutas favoritas");
        }
        if (items.size() == 0)
            items.add("No hay rutas favoritas");

        FragmentActivity aenviar = getActivity();
        adaptador = new AdaptadorRutasFavoritas(items,getActivity(),header, aenviar);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adaptador);
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(reciclador);

        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    int posicion = reciclador.getChildAdapterPosition(v);
                    Fragment fragmentoGenerico;
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

                    fragmentoGenerico = new FragmentoRutaMapa();

                    Bundle bundle = new Bundle();
                    bundle.putString("ruta", String.valueOf(items.get(posicion)));
                    fragmentoGenerico.setArguments(bundle);

                    if (fragmentoGenerico != null) {
                        ft.replace(R.id.contenedor_principal, fragmentoGenerico,"FragmentoRutaMapa").addToBackStack(null).commit();
                    /*fragmentManager
                            .beginTransaction()
                            .replace(R.id.contenedor_principal, fragmentoGenerico)
                            .commit();*/
                    }

            }
        });
        reciclador.setAdapter(adaptador);
        header.setText(items.size()+" "+getActivity().getString(R.string.titulo_fragmento_rutas_fav));

        return view;
    }

}
