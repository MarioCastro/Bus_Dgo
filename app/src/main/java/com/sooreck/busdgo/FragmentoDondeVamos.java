package com.sooreck.busdgo; /**
 * Created by SoOreck on 10/08/2016.
 */

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.projection.SphericalMercatorProjection;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragmento que contiene los diferentes tipos de rutas
 */

public class FragmentoDondeVamos extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener {

    private static final int LOCATION_REQUEST_CODE = 1;
    private static final int REQUEST_CODE = 10;
    private static final int REQUEST_CODE_2 = 11;
    GoogleMap mapa;

    Marker longclick;
    Circle circulo;

    MarkerOptions marcadorDePuntos;
    Marker marcadorA;
    Marker marcadorB;

    RadioButton pOrigen;
    RadioButton pDestino;

    private ProgressDialog pDialog;
    private String MSG;
    private int rutasEncontradas;
    private ArrayList<CharSequence> rutasEnc;
    private int rutasEncontradasOpcion2;
    private ArrayList<CharSequence> rutasEncOpcion2;
    private ArrayList<CharSequence> distanciaOrigenRuta;
    private String rutasOrigenDestino;
    private String rutasOrigen;
    private String rutasDestino;

    Ruta rutaObtenida;

    private int heigthHeaderTools;

    public static LatLng puntoDeRutaEncontrada;

    public FragmentoDondeVamos() {

    }

    /***********************************************************************************************
     ************************ EVENTO QUE SE EJECUTA AL CREARSE LA VISTA ***************************/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_a_donde_vamos, container, false);
        ActividadPrincipal.showItemMenu(true,false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);

        FloatingActionButton fbSearch = (FloatingActionButton) view.findViewById(R.id.search);
        TextView tvHeader = (TextView) view.findViewById(R.id.tvHeader);

        pOrigen = (RadioButton) view.findViewById(R.id.rbOrigen);
        pOrigen.setChecked(true);
        pDestino = (RadioButton) view.findViewById(R.id.rbDestino);

        pOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pOrigen.isChecked()){
                    pOrigen.setChecked(true);
                }
                pDestino.setChecked(false);
            }

        });
        pDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pDestino.isChecked()) {
                    pDestino.setChecked(true);
                }
                pOrigen.setChecked(false);
            }
        });

        fbSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marcadorA == null && marcadorB == null)
                    Snackbar.make(v, "Marca los puntos Origen y Destino", Snackbar.LENGTH_LONG)
                            .setAction("Ayuda", null)
                            .show();
                else
                if (marcadorA == null)
                    Snackbar.make(v, "Marca el punto de Origen", Snackbar.LENGTH_LONG)
                            .setAction("Ayuda", null)
                            .show();
                else
                if (marcadorB == null)
                    Snackbar.make(v, "Marca el punto Destino", Snackbar.LENGTH_LONG)
                            .setAction("Ayuda", null)
                            .show();
                else {

                    String lat = "", lng = "";
                    lat = String.valueOf(marcadorA.getPosition().latitude);
                    lng = String.valueOf(marcadorA.getPosition().longitude);
                    String lat2 = "", lng2 = "";
                    lat2 = String.valueOf(marcadorB.getPosition().latitude);
                    lng2 = String.valueOf(marcadorB.getPosition().longitude);
                    new BuscarRuta().execute(lat,lng,lat2,lng2);
                    Log.e("ATENCION1", "lat: " + String.valueOf(marcadorA.getPosition().latitude) + " Lng: "
                            + String.valueOf(marcadorA.getPosition().longitude));

                }


            }
        });
        heigthHeaderTools = ((LinearLayout) view.findViewById(R.id.headerTools)).getHeight();
        return view;
    }

    /***********************************************************************************************
     *********************** EVENTO QUE SE EJECUTA AL DESTRUIRSE LA VISTA **************************/
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ActividadPrincipal.showItemMenu(false,false);
    }

    /***********************************************************************************************
     ************************* EVENTO QUE SE EJECUTA AL CREARSE EL MAPA ***************************/
    @Override
    public void onMapReady(final GoogleMap map) {

        mapa = map;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapa.setOnMarkerClickListener(this);
        mapa.setOnMapClickListener(this);
        float densidad = getResources().getDisplayMetrics().density;
        mapa.setPadding(0,(heigthHeaderTools+((int)(88*densidad))),0,0);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Mostrar diálogo explicativo
            } else {
                // Solicitar permiso
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }
        }

        LatLng exampleLocation = new LatLng(24.009232, -104.661077);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(exampleLocation, 13.0f));

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(final LatLng point) {
                marcarPuntoObtenido(point);
            /*    if (longclick != null){
                    longclick.remove();
                    longclick = null;
                }
                if (circulo != null){
                    circulo.remove();
                    circulo = null;
                }


                String title = "";
                String snippet = "";
                BitmapDescriptor color = null;
                int indice = 0;

                if (pOrigen.isChecked()){
                    if (marcadorA != null) {
                        marcadorA.remove();
                        marcadorA = null;
                    }
                    title = "Punto A";
                    snippet = "Este es el punto donde te encuentras";
                    color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    indice = 1;
                }
                if (pDestino.isChecked()){
                    if (marcadorB != null) {
                        marcadorB.remove();
                        marcadorB = null;
                    }
                    title = "Punto B";
                    snippet = "Este es el punto destino";
                    color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
                    indice = 2;
                }

                Projection proj = map.getProjection();
                Point coord = proj.toScreenLocation(point);
                marcadorDePuntos = new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                        .icon(color)
                        .title(title)
                        .snippet(snippet)
                        .draggable(false);
                pintarMarcador(indice,marcadorDePuntos);
                pintarCirculo(new LatLng(point.latitude, point.longitude), 200); */

            }
        });

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    Location location = map.getMyLocation();

                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage("¿Desea utilizar su ubicación como punto destino?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Toast.makeText(
                                                    getActivity(),
                                                    "Sale!",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builder.create();

                } catch (NullPointerException ex) {
                    Toast.makeText(
                            getActivity(),
                            "Revisa que este activado tu GPS\n" +
                                    "e intenta de nuevo",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

    }//Metodo onMapReady

    /***********************************************************************************************
     ********** EVENTO QUE SE EJECUTA AL DAR CLIK EN UN MARCADOR PARA PINTAR UN CIRCULO ***********/
    @Override
    public boolean onMarkerClick(Marker marker) {
        LatLng pos = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);
        pintarCirculo(pos, 200);
        return false;
    }//Evento al dar click en un marcador

    /***********************************************************************************************
     ************ EVENTO QUE SE EJECUTA AL DAR CLIK EN EL MAPA PARA QUITAR EL CIRCULO *************/
    @Override
    public void onMapClick(LatLng latLng) {
        if (circulo != null){
            circulo.remove();
            circulo = null;
        }
    }//Termina Metodo onMapClick

    /***********************************************************************************************
     ******************* METODO PARA PINTAR CIRCULO AL REDEDOR DE UN MARCADOR *********************/
    private void pintarCirculo(LatLng center, double radius){
        if (circulo != null){
            circulo.remove();
            circulo = null;
        }
        CircleOptions circleOptions = new CircleOptions()
                .center(center)
                .radius(radius)
                .strokeColor(Color.parseColor("#FFFFFF"))
                .strokeWidth(4)
                .fillColor(Color.argb(32, 33, 150, 243));
        circulo = mapa.addCircle(circleOptions);
    }//Termina Metodo pintarCirculo

    /***********************************************************************************************
     ****************** EVENTO QUE SE EJECUTA AL RECIBIR UNA RESPUESTA DE PERMISOS ****************/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            // ¿Permisos asignados?
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mapa.setMyLocationEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Error de permisos", Toast.LENGTH_SHORT).show();
            }

        }
    }//TerminaOnRequest

    /***********************************************************************************************
     ******************** METODO QUE RECIBE LA RUTA CERCANA QUE DESEA MARCAR **********************/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DialogoRutasEncontradas.REQUEST_CODE){
            String rutaAMarcar = data.getExtras().getString("marcarRuta");
            if (rutaAMarcar != null && (!rutaAMarcar.equals("")) ){
                marcarRuta(rutaAMarcar);
            }
            Toast.makeText(getActivity(),rutaAMarcar , Toast.LENGTH_SHORT).show();
        }
        if (requestCode == DialogoRutasCerca.REQUEST_CODE){
            String rutaAMarcar = data.getExtras().getString("marcarRuta");
            if (rutaAMarcar != null && (!rutaAMarcar.equals("")) ){
                marcarRuta(rutaAMarcar);
            }
            Toast.makeText(getActivity(),rutaAMarcar , Toast.LENGTH_SHORT).show();
        }

    }

    /***********************************************************************************************
     ****************** METODO PARA PINTAR UN MARCADOR YA SEA ORIGEN O DESTINO ********************/
    private void pintarMarcador(int indice, MarkerOptions marker){
        if (indice == 1){
            marcadorA = mapa.addMarker(marcadorDePuntos);
        }
        if (indice == 2){
            marcadorB = mapa.addMarker(marcadorDePuntos);
        }
    }//PintarMarcador

    /***********************************************************************************************
     **************** METODO PARA PINTAR UN MARCADORES DESPUES DE LIMPIAR EL MAPA ******************/
    private void rePintarMarcador(int indice, MarkerOptions marker){
        if (indice == 1){
            marcadorA = mapa.addMarker(marker);
        }
        if (indice == 2){
            marcadorB = mapa.addMarker(marker);
        }
    }//PintarMarcador

    /***********************************************************************************************
     **************** METODO PARA INICIAR EL FRAGMENTO DIALOGO Y MOSTRAR RUTAS *******************/
    private void iniciarDialogoRutasEncontradas(){
        FragmentManager fragmentManager = getFragmentManager();
        Bundle args = new Bundle();
        args.putCharSequenceArrayList("rutasEncontradas",rutasEnc);
        args.putCharSequenceArrayList("distancias",distanciaOrigenRuta);
        args.putInt("numeroRutas",rutasEncontradas);
        DialogFragment dialogo = new DialogoRutasEncontradas();
        dialogo.setArguments(args);
        dialogo.setTargetFragment(this,REQUEST_CODE);
        dialogo.show(fragmentManager, "RutasDialog");
    }

    private void iniciarDialogoRutasCerca(){
        FragmentManager fragmentManager = getFragmentManager();
        Bundle args = new Bundle();
        args.putCharSequenceArrayList("rutasEncontradas",rutasEncOpcion2);
        args.putCharSequenceArrayList("distancias",distanciaOrigenRuta);
        args.putInt("numeroRutas",rutasEncontradasOpcion2);
        DialogFragment dialogo = new DialogoRutasCerca();
        dialogo.setArguments(args);
        dialogo.setTargetFragment(this,REQUEST_CODE);
        dialogo.show(fragmentManager, "RutasDialog");
    }

    private void iniciarDialogoNoRutasEncontradas(){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialogo = new DialogoNoRutas();
        dialogo.setTargetFragment(this,REQUEST_CODE_2);
        dialogo.show(fragmentManager, "NoRutasDialog");
    }

    /***********************************************************************************************
     **************** METODO PARA MARCAR LA RUTA RECIBIDA DEL FRAGMENTO DIALOGO ******************/
    private void marcarRuta(String rutaAObtener) {

        Marker a = marcadorA;
        Marker b = marcadorB;
        MarkerOptions marcadorOpcionesA = new MarkerOptions()
                .position(new LatLng(a.getPosition().latitude, a.getPosition().longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.mark_a))
                .title(a.getTitle())
                .snippet(a.getSnippet())
                .draggable(false);
        MarkerOptions marcadorOpcionesB = new MarkerOptions()
                .position(new LatLng(b.getPosition().latitude, b.getPosition().longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.mark_b))
                .title(b.getTitle())
                .snippet(b.getSnippet())
                .draggable(false);

        mapa.clear();

        rePintarMarcador(1,marcadorOpcionesA);
        rePintarMarcador(2,marcadorOpcionesB);

        rutaObtenida = new Ruta();
        rutaObtenida = rutaObtenida.dameRuta(rutaAObtener);
        if (rutaObtenida != null) {
            mapa.addMarker(rutaObtenida.getInicio());
            mapa.addMarker(rutaObtenida.getFin());
            mapa.addMarker(rutaObtenida.getBase());
            mapa.addPolyline(rutaObtenida.getRutaVuelta());
            mapa.addPolyline(rutaObtenida.getRutaIda());
            if (rutaObtenida.getIdentificador().equals("4")){
                Ruta r1 = new Ruta();
                mapa.addPolyline(r1.dameRutaById("4R").getRutaIda());
            }
            if (rutaObtenida.getIdentificador().equals("5")){
                Ruta r1 = new Ruta();
                mapa.addPolyline(r1.dameRutaById("5R").getRutaIda());}

            if (rutaObtenida.getParadasOficiales().size() >= 1){
                for (int i=0; i<rutaObtenida.getParadasOficiales().size(); i++){
                    MarkerOptions parada = new MarkerOptions()
                            .position(rutaObtenida.getParadasOficiales().get(i))
                            .title("Parada")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_paradas));
                    mapa.addMarker(parada);
                }
            }

        } else Toast.makeText(getActivity(), String.valueOf("null"), Toast.LENGTH_SHORT).show();
    }//Termina Metodo MarcarRuta

    /***********************************************************************************************
     ************ METODO PARA MARCAR EL PUNTO ORIGEN-DESTINO OBTENIDO DE LA BUSQUEDA **************/

    public void marcarPuntoObtenido(LatLng point){

        if (longclick != null){
            longclick.remove();
            longclick = null;
        }
        if (circulo != null){
            circulo.remove();
            circulo = null;
        }

        String title = "";
        String snippet = "";
        BitmapDescriptor color = null;
        int indice = 0;

        if (pOrigen.isChecked()){
            if (marcadorA != null) {
                marcadorA.remove();
                marcadorA = null;
            }
            title = "Punto A";
            snippet = "Este es el punto donde te encuentras";
            color = BitmapDescriptorFactory.fromResource(R.drawable.mark_a);
            indice = 1;
        }
        if (pDestino.isChecked()){
            if (marcadorB != null) {
                marcadorB.remove();
                marcadorB = null;
            }
            title = "Punto B";
            snippet = "Este es el punto destino";
            color = BitmapDescriptorFactory.fromResource(R.drawable.mark_b);
            indice = 2;
        }

        Projection proj = mapa.getProjection();
        Point coord = proj.toScreenLocation(point);
        //mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(exampleLocation, 13.0f));
        mapa.animateCamera(CameraUpdateFactory.newLatLng(point), 750, null);
        marcadorDePuntos = new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                .icon(color)
                .title(title)
                .snippet(snippet)
                .draggable(false);
        pintarMarcador(indice,marcadorDePuntos);
        pintarCirculo(new LatLng(point.latitude, point.longitude), 200);

    }

    /***********************************************************************************************
     ******* CLASE ASINCRONA PARA BUSCAR RUTAS CERCANAS EN BASE A UN PUNTO ORIGEN Y DESTINO ********
     **********************************************************************************************/
    class BuscarRuta extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Buscando Rutas Cerca...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {

            // Check for success tag
            int success;

            String latOrigen = args[0];
            String lngOrigen = args[1];
            String latDestino = args[2];
            String lngDestino = args[3];
            LatLng pointOrigen = new LatLng(Double.parseDouble(latOrigen),Double.parseDouble(lngOrigen));
            LatLng pointDestino = new LatLng(Double.parseDouble(latDestino),Double.parseDouble(lngDestino));

            Log.e("ATENCION2","lat: "+latOrigen+" Lng: "+lngOrigen);

            MSG = "";
            rutasOrigen = "";
            rutasOrigenDestino = "";
            rutasDestino = "";
            rutasEncontradas = 0;
            rutasEnc = new ArrayList<>();
            rutasEncOpcion2 = new ArrayList<>();
            distanciaOrigenRuta = new ArrayList<>();
            rutasEncontradasOpcion2 = 0;
            List<Ruta> rutas = Ruta.RUTAS;

            rutasOrigenDestino+="Rutas que pasan cerca de los dos puntos: \n";
            rutasOrigen+="Rutas que pasan cerca de ti: \n";
            rutasDestino+="Rutas que pasan cerca de tu destino: \n";
            for (int i=0; i<rutas.size(); i++){

            // todo Probando PolyUtilBusDgo
                boolean isLocationOnPathOrigen;
                isLocationOnPathOrigen = PolyUtilBusDgo.isLocationOnPath(pointOrigen,rutas.get(i).getRutaIda().getPoints() , true, 200);
               if (!isLocationOnPathOrigen){
                   isLocationOnPathOrigen = PolyUtilBusDgo.isLocationOnPath(pointOrigen,rutas.get(i).getRutaVuelta().getPoints() , true, 200);
               }
                //boolean isLocationOnPathOrigen = isLocationOnPathOrigenIda ||  isLocationOnPathOrigenVuelta;

                if (isLocationOnPathOrigen){

                    rutasOrigen+="- "+rutas.get(i).getNombre()+"\n";
                    //Toast.makeText(getActivity(), "La "+rutas.get(i).getNombre()+" pasa cerca ", Toast.LENGTH_LONG).show();
                    double distancia = SphericalUtil.computeDistanceBetween(pointOrigen, puntoDeRutaEncontrada);
                    rutasEncOpcion2.add(rutasEncontradasOpcion2, rutas.get(i).getNombre());
                    distanciaOrigenRuta.add(rutasEncontradasOpcion2, String.valueOf(distancia));
                    rutasEncontradasOpcion2++;
                }
                boolean isLocationOnPathDestino;
                isLocationOnPathDestino = PolyUtilBusDgo.isLocationOnPath(pointDestino,rutas.get(i).getRutaIda().getPoints() , true, 200);
               if (!isLocationOnPathDestino){
                   isLocationOnPathDestino = PolyUtilBusDgo.isLocationOnPath(pointDestino,rutas.get(i).getRutaVuelta().getPoints() , true, 200);
               }
                //boolean isLocationOnPathDestino = isLocationOnPathDestinoIda ||  isLocationOnPathDestinoVuelta;

                if (isLocationOnPathDestino){

                    rutasDestino+="- "+rutas.get(i).getNombre()+"\n";
                    //Toast.makeText(getActivity(), "La "+rutas.get(i).getNombre()+" pasa cerca ", Toast.LENGTH_LONG).show();
                }

                if (isLocationOnPathOrigen && isLocationOnPathDestino){
                    rutasOrigenDestino+="- "+rutas.get(i).getNombre()+"\n";
                    rutasEnc.add(rutasEncontradas,rutas.get(i).getNombre());
                    rutasEncontradas++;
                }


            }

            MSG=rutasOrigenDestino+"\n"+rutasOrigen+"\n"+rutasDestino;

            return MSG;

        }

        protected void onPostExecute(String result) {
            if (result != null){
                //Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                pDialog.dismiss();
                ProgressDialog pd = new ProgressDialog(getActivity());
                pd.setMessage(result);
                pd.setTitle("Rutas encontradas: "+rutasEncontradas);
                pd.setIndeterminate(false);
                pd.setCancelable(true);
                pd.show();
                pd.dismiss();

                /*Si se encontraron rutas que pasan por los dos puntos*/
                if (rutasEncontradas > 0){
                    iniciarDialogoRutasEncontradas();
                }
                /*Cuando no encontramos rutas que pasan por los dos lados*/
                else {
                    /*Si se encontraron rutas solo por el origen*/
                    if (rutasEncontradasOpcion2 > 0){
                        iniciarDialogoRutasCerca();
                    }
                    else {
                        /*Cuando no se encontro ninguna ruta de interes*/
                        iniciarDialogoNoRutasEncontradas();
                    }

                }


            }
        }
    }//Termina clase para buscar rutas cercanas


}//Termina clase FragmentoDondeVamos