package com.sooreck.busdgo;

/**
 * Created by SoOreck on 01/10/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

/**
 * Fragmento con un diálogo donde muestra que NO se encontraron rutas*/
public class DialogoNoRutas extends DialogFragment {

    public static final int REQUEST_CODE = 10;

    public DialogoNoRutas() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialogo();
    }

    public AlertDialog createDialogo() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_no_rutas_encontradas, null);
        builder.setView(v);

        Button cancelar = (Button) v.findViewById(R.id.boton_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return builder.create();
    }


}