package com.sooreck.busdgo;

/**
 * Created by SoOreck on 22/01/2017.
 */
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

/**
 * Fragmento que contiene los diferentes tipos de rutas
 */

public class FragmentoAcercaDe extends Fragment {

    public FragmentoAcercaDe() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_acerca_de, container, false);

        TextView version  = (TextView) view.findViewById(R.id.version);
        version.setText(getVersionName());

        return view;
    }

    private String getVersionName() {
        try {
            PackageManager manager = getActivity().getPackageManager();
            PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), 0);
            String version = info.versionName;
            return getString(R.string.v)+version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}