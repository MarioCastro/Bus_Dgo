package com.sooreck.busdgo;

/**
 * Created by SoOreck on 20/08/2016.
 */
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Fragmento con un diálogo donde muestra rutas encontradas y la opcion de dibujar la ruta seleccionada
 */
public class DialogoRutasEncontradas extends DialogFragment {

    private static final String TAG = DialogoRutasEncontradas.class.getSimpleName();

    public String rutaAEnviar = "";
    public static final int REQUEST_CODE = 10;

    public DialogoRutasEncontradas() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createLoginDialogo();
    }

    public AlertDialog createLoginDialogo() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialogo_rutas_encontradas, null);

        builder.setView(v);

        Bundle args = getArguments();
        if (args != null) {

            int numeroRutas = args.getInt("numeroRutas");
            final ArrayList<CharSequence> rutas = args.getCharSequenceArrayList("rutasEncontradas");
            final ArrayList<CharSequence> distancias = args.getCharSequenceArrayList("distancias");

            TextView title = (TextView) v.findViewById(R.id.title_dialog);
            title.setText(title.getText().toString()+" "+String.valueOf(numeroRutas));

            RadioGroup rGroup = new RadioGroup(getActivity());
            rGroup.setOrientation(LinearLayout.VERTICAL);

            assert rutas != null;
            for (int i = 0; i < rutas.size() ; i++) {

                RadioButton rutaButton = new RadioButton(getActivity());
                rutaButton.setId(i);
                rutaButton.setText(rutas.get(i));
                rGroup.addView(rutaButton);

            }

            ((ViewGroup) v.findViewById(R.id.group_rutas)).addView(rGroup);
            rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    rutaAEnviar = rutas.get(checkedId).toString();
                    Toast.makeText(getActivity(),"La distancia es: "+ String.valueOf(distancias.get(checkedId))+" mts", Toast.LENGTH_SHORT).show();
                }
            });


        }

        Button mostrarRuta = (Button) v.findViewById(R.id.boton_mostrar_ruta);
        Button cancelar = (Button) v.findViewById(R.id.boton_cancelar);

        mostrarRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!rutaAEnviar.equals("")){
                    Intent datos = new Intent();
                    datos.putExtra("marcarRuta",rutaAEnviar);
                    getTargetFragment().onActivityResult(getTargetRequestCode(),REQUEST_CODE,datos);
                    dismiss();
                } else Toast.makeText(getActivity(),R.string.msg_no_seleccion, Toast.LENGTH_SHORT).show();


            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return builder.create();
    }

    private void addRadioButtons(int n, ArrayList<CharSequence> list){




    }//Termina metodo agregar RadioButtons al group

}
