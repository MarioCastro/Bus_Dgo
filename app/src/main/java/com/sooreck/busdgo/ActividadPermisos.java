package com.sooreck.busdgo;

/**
 * Created by SoOreck on 19/01/2017.
 */

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPermisos extends AppCompatActivity {

    boolean permiso1;
    boolean permiso2;
    String user;
    Button permisoContinuarbutton;
    Button permiso1button;
    Button permiso2button;
    EditText ingresarUsuario;

    private static final int LOCATION_REQUEST_CODE = 1;
    private static final int WRITE_EXTERNAL_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_permisos);

        ingresarUsuario = (EditText) findViewById(R.id.username);

        Intent i = getIntent();
        permiso1 = i.getBooleanExtra("permiso1",false);
        permiso2 = i.getBooleanExtra("permiso2",false);
        user = i.getStringExtra("user");

        boolean tieneuser = !user.equals("");

        if (tieneuser) {
            ingresarUsuario.setText(user);
            ingresarUsuario.setEnabled(false);
        } else {
            ingresarUsuario.setEnabled(true);
        }

        permiso1button = (Button) findViewById(R.id.boton_activar1);
        permiso2button = (Button) findViewById(R.id.boton_activar2);
        permisoContinuarbutton = (Button) findViewById(R.id.boton_continuar_permisos);

        permiso1button.setEnabled(!permiso1);
        if (!permiso1button.isEnabled()) permiso1button.setText(R.string.permisos_activado);
        permiso2button.setEnabled(!permiso2);
        if (!permiso2button.isEnabled()) permiso2button.setText(R.string.permisos_activado);


        permiso1button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        ActividadPermisos.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }
        });

        permiso2button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ActividadPermisos.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_EXTERNAL_REQUEST_CODE);

                if (!permiso1button.isEnabled() && !permiso2button.isEnabled()){
                    permisoContinuarbutton.setEnabled(true);
                }
            }
        });

        permisoContinuarbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ingresarUsuario.getText().toString().equals("")){
                    if (!permiso1button.isEnabled() && !permiso2button.isEnabled()) {

                        SharedPreferences sp = getSharedPreferences("StarterData", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = sp.edit();
                        edit.putString("user", ingresarUsuario.getText().toString());
                        edit.apply();

                        Intent i = new Intent(ActividadPermisos.this, ActividadPrincipal.class);
                        finish();
                        startActivity(i);
                    }
                } else Toast.makeText(ActividadPermisos.this, "Ingrese un nombre de usuario", Toast.LENGTH_SHORT).show();
            }
        });

        /*todo evento keylistener al edittext del username*/
        ingresarUsuario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (ingresarUsuario.getText().toString().length()==0){
                    permisoContinuarbutton.setEnabled(false);
                }
                if (ingresarUsuario.getText().toString().length()>0){
                    if (!permiso1button.isEnabled() && !permiso2button.isEnabled()){
                        permisoContinuarbutton.setEnabled(true);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    /***********************************************************************************************
     ****************** EVENTO QUE SE EJECUTA AL RECIBIR UNA RESPUESTA DE PERMISOS ****************/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            // ¿Permisos asignados?
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permiso1button.setEnabled(false);
                permiso1button.setText(R.string.permisos_activado);
                if (!permiso2button.isEnabled()){
                    permisoContinuarbutton.setEnabled(true);
                }
            } else {
                Toast.makeText(ActividadPermisos.this, "No acepto permiso", Toast.LENGTH_SHORT).show();
            }

        }

        if (requestCode == WRITE_EXTERNAL_REQUEST_CODE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permiso2button.setEnabled(false);
                permiso2button.setText(R.string.permisos_activado);
                if (!permiso1button.isEnabled()){
                    permisoContinuarbutton.setEnabled(true);
                }

            } else {
                Toast.makeText(ActividadPermisos.this, "No aceptó permiso", Toast.LENGTH_SHORT).show();
            }
        }
    }

}

