package com.sooreck.busdgo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

/**
 * Created by SoOreck on 17/01/2017.
 */
public class SplashScreen extends Activity {

    private Thread timerThread;
    boolean permisoLocalizacion;
    boolean permisoDatos;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.spashscreen);

            timerThread = new Thread(){
                public void run(){
                    try{
                        sleep(1000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{

                        permisoLocalizacion = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED;
                        permisoDatos = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED;

                        SharedPreferences prefs =  getSharedPreferences("StarterData", Context.MODE_PRIVATE);
                        user = prefs.getString("user","");

                        if (permisoLocalizacion && permisoDatos && !user.equals(""))
                        iniciaActividadInicio();
                        else {
                            iniciaActividadPermisos();
                        }
                    }
                }
            };
            timerThread.start();


    }

    private void iniciaActividadInicio(){
        Intent i = new Intent(SplashScreen.this, ActividadPrincipal.class);
        finish();
        startActivity(i);
    }
    private void iniciaActividadPermisos(){

        Intent i = new Intent(SplashScreen.this, ActividadPermisos.class);
        i.putExtra("user", user);
        i.putExtra("permiso1",permisoLocalizacion);
        i.putExtra("permiso2",permisoDatos);
        finish();
        startActivity(i);

    }
}
