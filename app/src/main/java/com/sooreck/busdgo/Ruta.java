package com.sooreck.busdgo;

import android.graphics.Color;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by SoOreck on 17/06/2016.
 */
public class Ruta {

    /***********************************************************************************************
     *********************************** ATRIBUTOS DE LAS RUTAS ************************************
     **********************************************************************************************/

    private String nombre;
    private MarkerOptions inicio;
    private MarkerOptions fin;
    private MarkerOptions base;
    private PolylineOptions rutaIda;
    private PolylineOptions rutaVuelta;
    private List<LatLng> paradasOficiales;
    private String identificador;


    /***********************************************************************************************
     **************************************** CONSTRUCTORES ****************************************
     **********************************************************************************************/

    public Ruta(){ }

    public Ruta(String name,MarkerOptions start, MarkerOptions finish, MarkerOptions base,
                PolylineOptions rutaIda, PolylineOptions rutaVuelta, List<LatLng> paradas, String id){

        this.nombre = name;
        this.inicio = start;
        this.fin = finish;
        this.base = base;
        this.rutaIda = rutaIda;
        this.rutaVuelta = rutaVuelta;
        this.paradasOficiales = paradas;
        this.identificador = id;
    }

    /***********************************************************************************************
     *************************************** RUTAS ESTATICAS ***************************************
     **********************************************************************************************/
    public static final List<Ruta> RUTAS = new ArrayList<>();
    static {

        /*******************************************************************************************
        ********************************* RUTAS LIBERTAD/NAZAS ************************************/

        MarkerOptions baseLibertadNazas = new MarkerOptions()
                .position(new LatLng(23.97980618373215, -104.6737714111805))
                .title("Base")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.base));
        MarkerOptions inicioLibertadNazas = new MarkerOptions()
                .position(new LatLng(23.980230767537567, -104.67375498265028))
                .title("Inicio");
        MarkerOptions finLibertadNazas =  new MarkerOptions()
                .position(new LatLng(24.032005, -104.640674))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));


        List<LatLng> paradasLibertad = new ArrayList<>();
        List<LatLng> paradasNazas = new ArrayList<>();

        /*********************************** RUTA LIBERTAD ****************************************/

        PolylineOptions rutaInicioLibertad = new PolylineOptions()
                .add(new LatLng(23.980230767537567, -104.67375498265028))
                .add(new LatLng(23.980496055121147, -104.67272534966469))
                .add(new LatLng(23.980869172299055, -104.67123404145241))
                .add(new LatLng(23.981385040245915, -104.66918114572763))
                .add(new LatLng(23.98187609314762, -104.66722112149))
                .add(new LatLng(23.982339574299957, -104.66539654880762))
                .add(new LatLng(23.98249610985765, -104.66513872146606))
                .add(new LatLng(23.983048424600973, -104.66460697352886))
                .add(new LatLng(23.983026675146725, -104.6643940731883))
                .add(new LatLng(23.981975957700783, -104.66429449617863))
                .add(new LatLng(23.980354527633, -104.66431394219398))
                .add(new LatLng(23.980354527633, -104.66421972960234))
                .add(new LatLng(23.981957577727492, -104.66419324278831))
                .add(new LatLng(23.982959895109335, -104.66427840292454))
                .add(new LatLng(23.98562341149125, -104.66451309621334))
                .add(new LatLng(23.987409269139356, -104.6642180532217))
                .add(new LatLng(23.989309664037986, -104.66385897248983))
                .add(new LatLng(23.992712477084826, -104.66324105858803))
                .add(new LatLng(23.994729188716743, -104.6628799661994))
                .add(new LatLng(23.99701479787622, -104.66248400509357))
                .add(new LatLng(23.998451016905452, -104.66219365596771))
                .add(new LatLng(24.003739987299536, -104.66223254799843))
                .add(new LatLng(24.00854115993305, -104.66231502592564))
                .add(new LatLng(24.008739622513566, -104.66210212558508))//Arrieta vuelta politecnico
                .add(new LatLng(24.008814352171306, -104.66186609119177))
                .add(new LatLng(24.00876963688951, -104.66152310371399))
                .add(new LatLng(24.008631203178822, -104.66118983924389))
                .add(new LatLng(24.007113935443833, -104.6587647870183))
                .add(new LatLng(24.007154669752847, -104.65866722166538))
                .add(new LatLng(24.006177655327257, -104.65711623430252))
                .add(new LatLng(24.004596652630052, -104.6554770693183))
                .add(new LatLng(24.00423340517426, -104.65482395142317))
                .add(new LatLng(24.003453307888723, -104.65399146080017))
                .add(new LatLng(24.003396033186302, -104.65378023684025))
                .add(new LatLng(24.003434930981637, -104.65366087853909))//WalmartEden
                .add(new LatLng(24.00557429161553, -104.6525377035141))
                .add(new LatLng(24.007991098464494, -104.65132299810648))
                .add(new LatLng(24.00811391306093, -104.65198751538992))
                .add(new LatLng(24.008134126923416, -104.65210352092981))
                .add(new LatLng(24.008310538679407, -104.65325351804495))
                .add(new LatLng(24.00842967773192, -104.65401459485292))
                .add(new LatLng(24.00825755392394, -104.65466804802418))
                .add(new LatLng(24.008150359265237, -104.6550365164876))
                .add(new LatLng(24.00919626904757, -104.654411226511))
                .add(new LatLng(24.00937635455893, -104.6543562412262))
                .add(new LatLng(24.009636988075336, -104.65439546853304))
                .add(new LatLng(24.010375395792973, -104.65465899556875))
                .add(new LatLng(24.010953317084354, -104.654884301126))
                .add(new LatLng(24.01117780833247, -104.65498857200146))
                .add(new LatLng(24.012628876007, -104.6554009616375))
                .add(new LatLng(24.014902529536098, -104.65612649917603))
                .add(new LatLng(24.015530962240792, -104.65627737343311))
                .add(new LatLng(24.01772647130098, -104.65629950165749))
                .add(new LatLng(24.01845380771538, -104.65631525963545))
                .add(new LatLng(24.018244335249875, -104.65537682175636))
                .add(new LatLng(24.01768574034012, -104.65342551469803))
                .add(new LatLng(24.01746769166858, -104.65255279093981)) //hasta aqui dif rutas
                .add(new LatLng(24.017257605098774, -104.65175148099661))
                .add(new LatLng(24.018149092539385, -104.65174678713083))
                .add(new LatLng(24.01877414067958, -104.65175617486238))
                .add(new LatLng(24.019238407689144, -104.65170722454786))
                .add(new LatLng(24.0199338863326, -104.65173773467541))
                .add(new LatLng(24.020948770037457, -104.65193152427673))
                .add(new LatLng(24.024902268965718, -104.65254038572311))
                .add(new LatLng(24.027569204325932, -104.65295813977718))
                .add(new LatLng(24.02932508263998, -104.65324580669403))
                .add(new LatLng(24.031339382975673, -104.65356666594744))
                .add(new LatLng(24.033254745692034, -104.65387009084225))
                .add(new LatLng(24.034582774989786, -104.65407729148865))//paseo
                .add(new LatLng(24.034669, -104.654101))
                .add(new LatLng(24.034835, -104.653908))
                .add(new LatLng(24.034688, -104.652808))
                .add(new LatLng(24.034544, -104.651451))
                .add(new LatLng(24.034407, -104.650158))
                .add(new LatLng(24.034246, -104.648801))
                .add(new LatLng(24.033051, -104.648431))
                .add(new LatLng(24.031983, -104.648109))
                .add(new LatLng(24.031022, -104.647841))
                .add(new LatLng(24.030880, -104.647766))
                .add(new LatLng(24.030631, -104.647717))
                .add(new LatLng(24.031150, -104.645454))
                .add(new LatLng(24.031471, -104.643789))
                .add(new LatLng(24.032005, -104.640694))
                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo
            PolylineOptions rutaVueltaLibertad = new PolylineOptions()
                    .add(new LatLng(24.032005, -104.640664))
                    .add(new LatLng(24.032074, -104.640259))
                    .add(new LatLng(24.030482, -104.641987))
                    .add(new LatLng(24.029306, -104.643344))
                    .add(new LatLng(24.028375, -104.644385))
                    .add(new LatLng(24.027302, -104.645624))
                    .add(new LatLng(24.027047, -104.645801))
                    .add(new LatLng(24.025950, -104.645876)) //pinosuarez
                    .add(new LatLng(24.024934, -104.645806))
                    .add(new LatLng(24.023102, -104.645651))
                    .add(new LatLng(24.021195, -104.645490))
                    .add(new LatLng(24.019907, -104.645382))
                    .add(new LatLng(24.018692, -104.645275))
                    .add(new LatLng(24.016403, -104.645066))
                    .add(new LatLng(24.015864, -104.645039))
                    .add(new LatLng(24.015869, -104.645710))
                    .add(new LatLng(24.016374, -104.647737))
                    .add(new LatLng(24.016933, -104.649862))
                    .add(new LatLng(24.017491, -104.651959))
                    .add(new LatLng(24.018334, -104.655355))
                    .add(new LatLng(24.015928, -104.655355))
                    .add(new LatLng(24.015634, -104.655392))
                    .add(new LatLng(24.014870, -104.656160))//Uxmal
                    .add(new LatLng(24.013062, -104.655580))
                    .add(new LatLng(24.011150, -104.654985))
                    .add(new LatLng(24.011028, -104.654985))
                    .add(new LatLng(24.009391, -104.654448))
                    .add(new LatLng(24.008117, -104.655146))
                    .add(new LatLng(24.008416, -104.654030))
                    .add(new LatLng(24.008000, -104.651455))
                    .add(new LatLng(24.005677, -104.652624))
                    .add(new LatLng(24.003549, -104.653730))//waltmart esquina
                    .add(new LatLng(24.00352130242215, -104.65384997427464))
                    .add(new LatLng(24.003555812238433, -104.6539968252182))
                    .add(new LatLng(24.004633, -104.655178))
                    .add(new LatLng(24.005133, -104.655763))
                    .add(new LatLng(24.006427, -104.657206))
                    .add(new LatLng(24.007206, -104.658450))
                    .add(new LatLng(24.008445, -104.660430))//ctis 130
                    .add(new LatLng(24.008940, -104.661261))
                    .add(new LatLng(24.009068, -104.661626))
                    .add(new LatLng(24.008725, -104.662468))
                    .add(new LatLng(24.007617, -104.662441))//post soriana
                    .add(new LatLng(24.007039, -104.662506))
                    .add(new LatLng(24.004608, -104.662463))
                    .add(new LatLng(24.001585, -104.662415))
                    .add(new LatLng(23.999631, -104.662402))
                    .add(new LatLng(23.997832, -104.662483))//los naranjos
                    .add(new LatLng(23.995295, -104.662910))//Sep
                    .add(new LatLng(23.993690, -104.663186))
                    .add(new LatLng(23.991911, -104.663602))
                    .add(new LatLng(23.988699, -104.664133))
                    .add(new LatLng(23.987536, -104.664351))
                    .add(new LatLng(23.986286, -104.664584))
                    .add(new LatLng(23.985788, -104.664622))
                    .add(new LatLng(23.983615, -104.664463))
                    .add(new LatLng(23.982099, -104.664303))
                    .add(new LatLng(23.981006, -104.664292))
                    .add(new LatLng(23.98035851001034, -104.66432131826878))
                    .add(new LatLng(23.98035268961265, -104.66420497738275))
                    .add(new LatLng(23.981893, -104.664190))
                    .add(new LatLng(23.982966, -104.664270))
                    .add(new LatLng(23.983065, -104.664694))
                    .add(new LatLng(23.982682, -104.665032))
                    .add(new LatLng(23.982452, -104.665429))
                    .add(new LatLng(23.982021, -104.666990))//globalgas
                    .add(new LatLng(23.981467, -104.669243))
                    .add(new LatLng(23.980844, -104.671727))
                    .add(new LatLng(23.980330, -104.673755))
                    .width(30f)
                    .color(Color.argb(80, 37, 10, 205));

                RUTAS.add(new Ruta("Ruta Libertad", inicioLibertadNazas, finLibertadNazas, baseLibertadNazas,
                        rutaInicioLibertad, rutaVueltaLibertad, paradasLibertad, "0"));

        /************************************** RUTA NAZAS ****************************************/

        PolylineOptions rutaInicioNazas = new PolylineOptions()
                .add(new LatLng(23.980230767537567, -104.67375498265028))
                .add(new LatLng(23.980496055121147, -104.67272534966469))
                .add(new LatLng(23.980869172299055, -104.67123404145241))
                .add(new LatLng(23.981385040245915, -104.66918114572763))
                .add(new LatLng(23.98187609314762, -104.66722112149))
                .add(new LatLng(23.982339574299957, -104.66539654880762))
                .add(new LatLng(23.98249610985765, -104.66513872146606))
                .add(new LatLng(23.983048424600973, -104.66460697352886))
                .add(new LatLng(23.983026675146725, -104.6643940731883))
                .add(new LatLng(23.981975957700783, -104.66429449617863))
                .add(new LatLng(23.980354527633, -104.66431394219398))
                .add(new LatLng(23.980354527633, -104.66421972960234))
                .add(new LatLng(23.981957577727492, -104.66419324278831))
                .add(new LatLng(23.982959895109335, -104.66427840292454))
                .add(new LatLng(23.98562341149125, -104.66451309621334))
                .add(new LatLng(23.987409269139356, -104.6642180532217))
                .add(new LatLng(23.989309664037986, -104.66385897248983))
                .add(new LatLng(23.992712477084826, -104.66324105858803))
                .add(new LatLng(23.994729188716743, -104.6628799661994))
                .add(new LatLng(23.99701479787622, -104.66248400509357))
                .add(new LatLng(23.998451016905452, -104.66219365596771))
                .add(new LatLng(24.003739987299536, -104.66223254799843))
                .add(new LatLng(24.00854115993305, -104.66231502592564))
                .add(new LatLng(24.008739622513566, -104.66210212558508))//Arrieta vuelta politecnico
                .add(new LatLng(24.008814352171306, -104.66186609119177))
                .add(new LatLng(24.00876963688951, -104.66152310371399))
                .add(new LatLng(24.008631203178822, -104.66118983924389))
                .add(new LatLng(24.007113935443833, -104.6587647870183))
                .add(new LatLng(24.007154669752847, -104.65866722166538))
                .add(new LatLng(24.006177655327257, -104.65711623430252))
                .add(new LatLng(24.004596652630052, -104.6554770693183))
                .add(new LatLng(24.00423340517426, -104.65482395142317))
                .add(new LatLng(24.003453307888723, -104.65399146080017))
                .add(new LatLng(24.003396033186302, -104.65378023684025))
                .add(new LatLng(24.003434930981637, -104.65366087853909))//WalmartEden
                .add(new LatLng(24.00557429161553, -104.6525377035141))
                .add(new LatLng(24.007991098464494, -104.65132299810648))
                .add(new LatLng(24.00811391306093, -104.65198751538992))
                .add(new LatLng(24.008134126923416, -104.65210352092981))
                .add(new LatLng(24.008310538679407, -104.65325351804495))
                .add(new LatLng(24.00842967773192, -104.65401459485292))
                .add(new LatLng(24.00825755392394, -104.65466804802418))
                .add(new LatLng(24.008150359265237, -104.6550365164876))
                .add(new LatLng(24.00919626904757, -104.654411226511))
                .add(new LatLng(24.00937635455893, -104.6543562412262))
                .add(new LatLng(24.009636988075336, -104.65439546853304))
                .add(new LatLng(24.010375395792973, -104.65465899556875))
                .add(new LatLng(24.010953317084354, -104.654884301126))
                .add(new LatLng(24.01117780833247, -104.65498857200146))
                .add(new LatLng(24.012628876007, -104.6554009616375))
                .add(new LatLng(24.014902529536098, -104.65612649917603))
                .add(new LatLng(24.015530962240792, -104.65627737343311))
                .add(new LatLng(24.01772647130098, -104.65629950165749))
                .add(new LatLng(24.01845380771538, -104.65631525963545))
                .add(new LatLng(24.018244335249875, -104.65537682175636))
                .add(new LatLng(24.01768574034012, -104.65342551469803))
                .add(new LatLng(24.01746769166858, -104.65255279093981))
                .add(new LatLng(24.016730, -104.649801))
                .add(new LatLng(24.016240, -104.647934))
                .add(new LatLng(24.015691, -104.645767))
                .add(new LatLng(24.015657, -104.645011))
                .add(new LatLng(24.017249, -104.645140))//mondan
                .add(new LatLng(24.018699, -104.645284))
                .add(new LatLng(24.020018, -104.645391))
                .add(new LatLng(24.021532, -104.645519))
                .add(new LatLng(24.022218, -104.645525))
                .add(new LatLng(24.024021, -104.645734))
                .add(new LatLng(24.026025, -104.645863))//parqueinfantil
                .add(new LatLng(24.026990, -104.645793))
                .add(new LatLng(24.028269, -104.644355)) //conade
                .add(new LatLng(24.029533, -104.642907))
                .add(new LatLng(24.030503, -104.641700))
                .add(new LatLng(24.032091, -104.639999))
                .add(new LatLng(24.032421849665795, -104.63965404778719))
                .add(new LatLng(24.032644, -104.639205))
                .add(new LatLng(24.032865, -104.638406))
                .add(new LatLng(24.033126, -104.638366))
                .add(new LatLng(24.033248, -104.639975))//centralfrente
                .add(new LatLng(24.033364675315713, -104.64008823037148))
                .add(new LatLng(24.033601, -104.640163))
                .add(new LatLng(24.035428, -104.640174))
                .add(new LatLng(24.035605, -104.639970))
                .add(new LatLng(24.035279, -104.637336))
                .add(new LatLng(24.034952, -104.637092))
                .add(new LatLng(24.033531, -104.638685))
                .add(new LatLng(24.032105, -104.640209))
                .add(new LatLng(24.032005, -104.640664))
                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo
        PolylineOptions rutaVueltaNazas = new PolylineOptions()
                .add(new LatLng(24.032005, -104.640694))
                .add(new LatLng(24.031620, -104.642960))
                .add(new LatLng(24.031086, -104.645675))
                .add(new LatLng(24.030180, -104.645396))
                .add(new LatLng(24.029499, -104.648164))//sorianaTecnoFrente
                .add(new LatLng(24.029019, -104.650278))
                .add(new LatLng(24.028695, -104.651570))
                .add(new LatLng(24.029636, -104.651828))
                .add(new LatLng(24.029700, -104.651624))
                .add(new LatLng(24.030523, -104.651876))
                .add(new LatLng(24.029734, -104.654891))
                .add(new LatLng(24.029366, -104.656436))//Cuauhtemoc
                .add(new LatLng(24.027066, -104.656045))
                .add(new LatLng(24.024477, -104.655566))
                .add(new LatLng(24.022926, -104.655299))
                .add(new LatLng(24.021228, -104.654991))
                .add(new LatLng(24.020947, -104.655307))//telesec
                .add(new LatLng(24.018334, -104.655355))
                .add(new LatLng(24.015928, -104.655355))
                .add(new LatLng(24.015634, -104.655392))
                .add(new LatLng(24.014870, -104.656160))//Uxmal
                .add(new LatLng(24.013062, -104.655580))
                .add(new LatLng(24.011150, -104.654985))
                .add(new LatLng(24.011028, -104.654985))
                .add(new LatLng(24.009391, -104.654448))
                .add(new LatLng(24.008117, -104.655146))
                .add(new LatLng(24.008416, -104.654030))
                .add(new LatLng(24.008000, -104.651455))
                .add(new LatLng(24.005677, -104.652624))
                .add(new LatLng(24.003549, -104.653730))//waltmart esquina
                .add(new LatLng(24.00352130242215, -104.65384997427464))
                .add(new LatLng(24.003555812238433, -104.6539968252182))
                .add(new LatLng(24.004633, -104.655178))
                .add(new LatLng(24.005133, -104.655763))
                .add(new LatLng(24.006427, -104.657206))
                .add(new LatLng(24.007206, -104.658450))
                .add(new LatLng(24.008445, -104.660430))//ctis 130
                .add(new LatLng(24.008940, -104.661261))
                .add(new LatLng(24.009068, -104.661626))
                .add(new LatLng(24.008725, -104.662468))
                .add(new LatLng(24.007617, -104.662441))//post soriana
                .add(new LatLng(24.007039, -104.662506))
                .add(new LatLng(24.004608, -104.662463))
                .add(new LatLng(24.001585, -104.662415))
                .add(new LatLng(23.999631, -104.662402))
                .add(new LatLng(23.997832, -104.662483))//los naranjos
                .add(new LatLng(23.995295, -104.662910))//Sep
                .add(new LatLng(23.993690, -104.663186))
                .add(new LatLng(23.991911, -104.663602))
                .add(new LatLng(23.988699, -104.664133))
                .add(new LatLng(23.987536, -104.664351))
                .add(new LatLng(23.986286, -104.664584))
                .add(new LatLng(23.985788, -104.664622))
                .add(new LatLng(23.983615, -104.664463))
                .add(new LatLng(23.982099, -104.664303))
                .add(new LatLng(23.981006, -104.664292))
                .add(new LatLng(23.98035851001034, -104.66432131826878))
                .add(new LatLng(23.98035268961265, -104.66420497738275))
                .add(new LatLng(23.981893, -104.664190))
                .add(new LatLng(23.982966, -104.664270))
                .add(new LatLng(23.983065, -104.664694))
                .add(new LatLng(23.982682, -104.665032))
                .add(new LatLng(23.982452, -104.665429))
                .add(new LatLng(23.982021, -104.666990))//globalgas
                .add(new LatLng(23.981467, -104.669243))
                .add(new LatLng(23.980844, -104.671727))
                .add(new LatLng(23.980330, -104.673755))
                .width(30f)
                .color(Color.argb(80, 37, 10, 205));

        RUTAS.add(new Ruta("Ruta Nazas", inicioLibertadNazas,finLibertadNazas, baseLibertadNazas,
                rutaInicioNazas, rutaVueltaNazas, paradasNazas,"1"));


        /*******************************************************************************************
        ********************************* 20 DE NOVIEMBRE ******************************************/

        MarkerOptions base20Nov = new MarkerOptions()
                .position(new LatLng(24.029331, -104.628718))
                .title("Base")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.base));
        MarkerOptions inicio20Nov = new MarkerOptions()
                .position(new LatLng(24.029212699797135, -104.62885279208422))
                .title("Inicio");
        MarkerOptions fin20Nov =  new MarkerOptions()
                .position(new LatLng(24.028421, -104.681418))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        List<LatLng> paradas20NovTecno = new ArrayList<>();
        List<LatLng> paradas20NovCruzRoja = new ArrayList<>();

        /********************************* 20 DE NOV - TECNO ***************************************/

        PolylineOptions rutaInicio20Tecno = new PolylineOptions()
                .add(new LatLng(24.029204, -104.628820))
                .add(new LatLng(24.029032, -104.627173))
                .add(new LatLng(24.027273, -104.627425))
                .add(new LatLng(24.027072, -104.627586))
                .add(new LatLng(24.027043, -104.630140))
                .add(new LatLng(24.027023, -104.632248))
                .add(new LatLng(24.026989, -104.634522))//iniciaPant
                .add(new LatLng(24.026955, -104.636609))
                .add(new LatLng(24.026930, -104.637864))
                .add(new LatLng(24.026916, -104.639924))
                .add(new LatLng(24.026872, -104.642531))//terminaPant
                .add(new LatLng(24.026950, -104.644248))
                .add(new LatLng(24.027004, -104.645487))
                .add(new LatLng(24.0270578086, -104.6458583325))
                .add(new LatLng(24.026934399721878, -104.6468869567197))//patoni
                .add(new LatLng(24.02682722063227, -104.64776504784822))
                .add(new LatLng(24.026591, -104.649486))
                .add(new LatLng(24.026478, -104.650298))
                .add(new LatLng(24.026299, -104.651720))
                .add(new LatLng(24.026147, -104.652737)) //lazaroCardenas
                .add(new LatLng(24.026845, -104.652844))
                .add(new LatLng(24.027646, -104.652981))
                .add(new LatLng(24.027404, -104.654491))
                .add(new LatLng(24.027237, -104.655387))
                .add(new LatLng(24.027044, -104.656556))
                .add(new LatLng(24.026867, -104.657771))
                .add(new LatLng(24.026718, -104.658825))
                .add(new LatLng(24.026512, -104.659985))
                .add(new LatLng(24.026306, -104.661156))
                .add(new LatLng(24.026123, -104.662390))
                .add(new LatLng(24.025785, -104.664246))//terminaSorianaCentro
                .add(new LatLng(24.025642, -104.665048))
                .add(new LatLng(24.025368, -104.666655))
                .add(new LatLng(24.025047, -104.668605))//FcoIMadero
                .add(new LatLng(24.024756, -104.669962))
                .add(new LatLng(24.024464, -104.671351))
                .add(new LatLng(24.024346, -104.672097))
                .add(new LatLng(24.024268, -104.674162))//Independencia
                .add(new LatLng(24.024256, -104.674736))
                .add(new LatLng(24.024239, -104.675426))
                .add(new LatLng(24.024420, -104.676220))
                .add(new LatLng(24.024685, -104.676461))
                .add(new LatLng(24.024685, -104.676461))
                .add(new LatLng(24.025767, -104.676324))
                .add(new LatLng(24.026882, -104.676150))//Terminafenix
                .add(new LatLng(24.026899, -104.676756))
                .add(new LatLng(24.026897, -104.677719))
                .add(new LatLng(24.026848, -104.678170))
                .add(new LatLng(24.026693, -104.678998))
                .add(new LatLng(24.027281, -104.679293))//doloresUP
                .add(new LatLng(24.027945, -104.679642))
                .add(new LatLng(24.028354, -104.679897))
                .add(new LatLng(24.028624, -104.680010))
                .add(new LatLng(24.028734, -104.680474))//canoasFin
                .add(new LatLng(24.028690, -104.680817))
                .add(new LatLng(24.028617, -104.681066))
                .add(new LatLng(24.028403, -104.681388))
                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVuelta20Tecno = new PolylineOptions()
                .add(new LatLng(24.028403, -104.681388))
                .add(new LatLng(24.028274, -104.681541))
                .add(new LatLng(24.028706, -104.681815))
                .add(new LatLng(24.028895, -104.681975))
                .add(new LatLng(24.029393, -104.682550))
                .add(new LatLng(24.029522, -104.683035))
                .add(new LatLng(24.029794, -104.684468))
                .add(new LatLng(24.030025, -104.686012))
                .add(new LatLng(24.030149, -104.686136))
                .add(new LatLng(24.030331, -104.686157))
                .add(new LatLng(24.031037, -104.685630))
                .add(new LatLng(24.031398, -104.685485))
                .add(new LatLng(24.032206, -104.685055))
                .add(new LatLng(24.032948, -104.684708))
                .add(new LatLng(24.033608, -104.684385))
                .add(new LatLng(24.033952, -104.684129))//Dolores-artesanos
                .add(new LatLng(24.034304, -104.683896))
                .add(new LatLng(24.034476, -104.683750))
                .add(new LatLng(24.034781, -104.683436))//preretornodolores
                .add(new LatLng(24.034931, -104.683527))
                .add(new LatLng(24.035035, -104.683681))//retorno
                .add(new LatLng(24.034558, -104.684128))
                .add(new LatLng(24.034323, -104.684339))
                .add(new LatLng(24.034030, -104.684578))
                .add(new LatLng(24.033871, -104.684650))
                .add(new LatLng(24.033680, -104.684685))
                .add(new LatLng(24.033399, -104.684819))
                .add(new LatLng(24.032838, -104.685049))
                .add(new LatLng(24.032661, -104.685236))
                .add(new LatLng(24.032275, -104.685441))
                .add(new LatLng(24.031686, -104.685782))
                .add(new LatLng(24.031186, -104.686103))
                .add(new LatLng(24.030731, -104.686439))
                .add(new LatLng(24.030426, -104.686716))
                .add(new LatLng(24.029983, -104.687246))//terminasahuatoba
                .add(new LatLng(24.029705, -104.687160))
                .add(new LatLng(24.029442, -104.686975))
                .add(new LatLng(24.028847, -104.685930))
                .add(new LatLng(24.028070, -104.684502))
                .add(new LatLng(24.027342, -104.683622))//imms
                .add(new LatLng(24.026795, -104.683026))
                .add(new LatLng(24.026077, -104.682183))
                .add(new LatLng(24.025206, -104.681180))
                .add(new LatLng(24.024466, -104.680325))
                .add(new LatLng(24.024551, -104.680202))
                .add(new LatLng(24.024719, -104.678320))
                .add(new LatLng(24.024917, -104.676733))
                .add(new LatLng(24.025041, -104.676345))
                .add(new LatLng(24.025164, -104.675346))
                .add(new LatLng(24.025247, -104.674286))
                .add(new LatLng(24.024141, -104.674147))//independencia
                .add(new LatLng(24.024180, -104.673154))
                .add(new LatLng(24.024283987727348, -104.67204038053751))
                .add(new LatLng(24.024496, -104.670598))
                .add(new LatLng(24.024777, -104.669237))
                .add(new LatLng(24.024947, -104.668584))//FcoIMadero
                .add(new LatLng(24.025112, -104.667509))
                .add(new LatLng(24.025395, -104.665895))//progreso
                .add(new LatLng(24.025701, -104.664266))
                .add(new LatLng(24.025901, -104.663089))//sorianaCentro
                .add(new LatLng(24.026212, -104.661132))
                .add(new LatLng(24.026606, -104.658806))//regato
                .add(new LatLng(24.026865, -104.657105))
                .add(new LatLng(24.027151, -104.655386))
                .add(new LatLng(24.027305, -104.654420))
                .add(new LatLng(24.027569, -104.652963))
                .add(new LatLng(24.028317, -104.653083))
                .add(new LatLng(24.029309, -104.653234))//negrete
                .add(new LatLng(24.029694, -104.651629))
                .add(new LatLng(24.029978, -104.650470))
                .add(new LatLng(24.030280, -104.649112))
                .add(new LatLng(24.030624, -104.647718))
                .add(new LatLng(24.030940, -104.646351))//TecnoSur
                .add(new LatLng(24.031276, -104.644897))
                .add(new LatLng(24.031504, -104.643681))
                .add(new LatLng(24.031787, -104.641965))
                .add(new LatLng(24.031955, -104.641095))
                .add(new LatLng(24.032065, -104.640258))//TopeConHeroico
                .add(new LatLng(24.031130, -104.641271))
                .add(new LatLng(24.030451, -104.642039))
                .add(new LatLng(24.029643, -104.642975))
                .add(new LatLng(24.029544, -104.642738))
                .add(new LatLng(24.029736, -104.641286))
                .add(new LatLng(24.029948, -104.639784))
                .add(new LatLng(24.030161, -104.638161))
                .add(new LatLng(24.030263, -104.637174))
                .add(new LatLng(24.030464, -104.635747))
                .add(new LatLng(24.030629, -104.634506))//TerminaPanteon
                .add(new LatLng(24.030865, -104.633121))
                .add(new LatLng(24.031069, -104.631712))
                .add(new LatLng(24.029713, -104.631493))
                .add(new LatLng(24.028355, -104.631297))
                .add(new LatLng(24.028418, -104.630776))
                .add(new LatLng(24.028380, -104.629412))
                .add(new LatLng(24.029207, -104.628882))
                .width(30f)
                .color(Color.argb(80, 37, 10, 205));

        RUTAS.add(new Ruta("Ruta 20 de Noviembre - Tecno", inicio20Nov,fin20Nov, base20Nov,
                rutaInicio20Tecno, rutaVuelta20Tecno, paradas20NovTecno,"2"));


        /******************************* 20 DE NOV - CRUZ ROJA *************************************/

        PolylineOptions rutaInicio20Cruz = new PolylineOptions()
                .add(new LatLng(24.029210, -104.628902))
                .add(new LatLng(24.028374, -104.629411))
                .add(new LatLng(24.028434, -104.630790))
                .add(new LatLng(24.028355, -104.631308))
                .add(new LatLng(24.029050, -104.631402))
                .add(new LatLng(24.030381, -104.631594))
                .add(new LatLng(24.031148, -104.631724))//EGO
                .add(new LatLng(24.030978, -104.633048))
                .add(new LatLng(24.030763, -104.634530))//empiezapanteon
                .add(new LatLng(24.030548426301664, -104.63599484413862))
                .add(new LatLng(24.03034662898783, -104.637453630566))
                .add(new LatLng(24.030170553682744, -104.63888593018055))
                .add(new LatLng(24.029952219969385, -104.64050464332104))
                .add(new LatLng(24.02967815398366, -104.64247103780508))
                .add(new LatLng(24.0298196272247, -104.64249785989523))
                .add(new LatLng(24.03046605384005, -104.64176427572966))
                .add(new LatLng(24.031322234915123, -104.64084595441818))
                .add(new LatLng(24.032099, -104.640001))
                .add(new LatLng(24.032410, -104.639662))
                .add(new LatLng(24.032756, -104.638920))
                .add(new LatLng(24.032861, -104.638433))
                .add(new LatLng(24.032986810978528, -104.63836457580328))
                .add(new LatLng(24.033125, -104.638362))//pasandoPuente
                .add(new LatLng(24.033159, -104.638816))
                .add(new LatLng(24.033189, -104.639109))
                .add(new LatLng(24.032763, -104.639545))
                .add(new LatLng(24.032083, -104.640238))//iniciaPlazaGala
                .add(new LatLng(24.031895, -104.641456))
                .add(new LatLng(24.031625, -104.642969))
                .add(new LatLng(24.031490, -104.643772))
                .add(new LatLng(24.031270, -104.644902))//esquinaTecno
                .add(new LatLng(24.031033, -104.644918))
                .add(new LatLng(24.030154, -104.644849))
                .add(new LatLng(24.029109, -104.644765))//20denov
                .add(new LatLng(24.028895, -104.645745))
                .add(new LatLng(24.028799, -104.646470))
                .add(new LatLng(24.028521, -104.647894))
                .add(new LatLng(24.028304, -104.649312))
                .add(new LatLng(24.028067, -104.650652))//preLazaroCard6

                .add(new LatLng(24.027646, -104.652981))//lazaroCardenas
                .add(new LatLng(24.027404, -104.654491))
                .add(new LatLng(24.027237, -104.655387))
                .add(new LatLng(24.027044, -104.656556))
                .add(new LatLng(24.026867, -104.657771))
                .add(new LatLng(24.026718, -104.658825))
                .add(new LatLng(24.026512, -104.659985))
                .add(new LatLng(24.026306, -104.661156))
                .add(new LatLng(24.026123, -104.662390))
                .add(new LatLng(24.025785, -104.664246))//terminaSorianaCentro
                .add(new LatLng(24.025642, -104.665048))
                .add(new LatLng(24.025368, -104.666655))
                .add(new LatLng(24.025047, -104.668605))//FcoIMadero
                .add(new LatLng(24.024756, -104.669962))
                .add(new LatLng(24.024464, -104.671351))
                .add(new LatLng(24.024346, -104.672097))
                .add(new LatLng(24.024268, -104.674162))//Independencia
                .add(new LatLng(24.024256, -104.674736))
                .add(new LatLng(24.024239, -104.675426))
                .add(new LatLng(24.024420, -104.676220))
                .add(new LatLng(24.024685, -104.676461))
                .add(new LatLng(24.024685, -104.676461))
                .add(new LatLng(24.025767, -104.676324))
                .add(new LatLng(24.026882, -104.676150))//Terminafenix
                .add(new LatLng(24.026899, -104.676756))
                .add(new LatLng(24.026897, -104.677719))
                .add(new LatLng(24.026848, -104.678170))
                .add(new LatLng(24.026693, -104.678998))
                .add(new LatLng(24.027281, -104.679293))//doloresUP
                .add(new LatLng(24.027945, -104.679642))
                .add(new LatLng(24.028354, -104.679897))
                .add(new LatLng(24.028624, -104.680010))
                .add(new LatLng(24.028734, -104.680474))//canoasFin
                .add(new LatLng(24.028690, -104.680817))
                .add(new LatLng(24.028617, -104.681066))
                .add(new LatLng(24.028403, -104.681388))
                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVuelta20Cruz = new PolylineOptions()
                .add(new LatLng(24.028403, -104.681388))
                .add(new LatLng(24.028274, -104.681541))
                .add(new LatLng(24.028706, -104.681815))
                .add(new LatLng(24.028895, -104.681975))
                .add(new LatLng(24.029393, -104.682550))
                .add(new LatLng(24.029522, -104.683035))
                .add(new LatLng(24.029794, -104.684468))
                .add(new LatLng(24.030025, -104.686012))
                .add(new LatLng(24.030149, -104.686136))
                .add(new LatLng(24.030331, -104.686157))
                .add(new LatLng(24.031037, -104.685630))
                .add(new LatLng(24.031398, -104.685485))
                .add(new LatLng(24.032206, -104.685055))
                .add(new LatLng(24.032948, -104.684708))
                .add(new LatLng(24.033608, -104.684385))
                .add(new LatLng(24.033952, -104.684129))//Dolores-artesanos
                .add(new LatLng(24.034304, -104.683896))
                .add(new LatLng(24.034476, -104.683750))
                .add(new LatLng(24.034781, -104.683436))//preretornodolores
                .add(new LatLng(24.034931, -104.683527))
                .add(new LatLng(24.035035, -104.683681))//retorno
                .add(new LatLng(24.034558, -104.684128))
                .add(new LatLng(24.034323, -104.684339))
                .add(new LatLng(24.034030, -104.684578))
                .add(new LatLng(24.033871, -104.684650))
                .add(new LatLng(24.033680, -104.684685))
                .add(new LatLng(24.033399, -104.684819))
                .add(new LatLng(24.032838, -104.685049))
                .add(new LatLng(24.032661, -104.685236))
                .add(new LatLng(24.032275, -104.685441))
                .add(new LatLng(24.031686, -104.685782))
                .add(new LatLng(24.031186, -104.686103))
                .add(new LatLng(24.030731, -104.686439))
                .add(new LatLng(24.030426, -104.686716))
                .add(new LatLng(24.029983, -104.687246))//terminasahuatoba
                .add(new LatLng(24.029705, -104.687160))
                .add(new LatLng(24.029442, -104.686975))
                .add(new LatLng(24.028847, -104.685930))
                .add(new LatLng(24.028070, -104.684502))
                .add(new LatLng(24.027342, -104.683622))//imms
                .add(new LatLng(24.026795, -104.683026))
                .add(new LatLng(24.026077, -104.682183))
                .add(new LatLng(24.025206, -104.681180))
                .add(new LatLng(24.024466, -104.680325))
                .add(new LatLng(24.024551, -104.680202))
                .add(new LatLng(24.024719, -104.678320))
                .add(new LatLng(24.024917, -104.676733))
                .add(new LatLng(24.025041, -104.676345))
                .add(new LatLng(24.025164, -104.675346))
                .add(new LatLng(24.025247, -104.674286))
                .add(new LatLng(24.024141, -104.674147))//independencia
                .add(new LatLng(24.024180, -104.673154))
                .add(new LatLng(24.024283987727348, -104.67204038053751))
                .add(new LatLng(24.024496, -104.670598))
                .add(new LatLng(24.024777, -104.669237))
                .add(new LatLng(24.024947, -104.668584))//FcoIMadero
                .add(new LatLng(24.025112, -104.667509))
                .add(new LatLng(24.025395, -104.665895))//progreso
                .add(new LatLng(24.025701, -104.664266))
                .add(new LatLng(24.025901, -104.663089))//sorianaCentro
                .add(new LatLng(24.026212, -104.661132))
                .add(new LatLng(24.026606, -104.658806))//regato
                .add(new LatLng(24.026865, -104.657105))
                .add(new LatLng(24.027151, -104.655386))
                .add(new LatLng(24.027305, -104.654420))
                .add(new LatLng(24.027569, -104.652963))//LazaroCardenas
                .add(new LatLng(24.027703, -104.652161))
                .add(new LatLng(24.026174, -104.651908))
                .add(new LatLng(24.026226, -104.651691))
                .add(new LatLng(24.026405, -104.650285))
                .add(new LatLng(24.026518, -104.649478))//economia
                .add(new LatLng(24.026669, -104.648479))
                .add(new LatLng(24.026710, -104.647958))
                .add(new LatLng(24.026801, -104.647443))
                .add(new LatLng(24.026848, -104.646925))
                .add(new LatLng(24.026980, -104.645829))//parqueinfantil
                .add(new LatLng(24.02697910868712, -104.64569572359324))
                .add(new LatLng(24.027002381840955, -104.6454523132513))
                .add(new LatLng(24.02697084059199, -104.64475493878126))//5-telegrafos
                .add(new LatLng(24.026955, -104.644250))
                .add(new LatLng(24.026911, -104.643561))
                .add(new LatLng(24.026872, -104.642733))
                .add(new LatLng(24.026868, -104.642417))
                .add(new LatLng(24.026439, -104.641788))
                .add(new LatLng(24.026522, -104.640418))
                .add(new LatLng(24.026592, -104.639494))
                .add(new LatLng(24.026644, -104.639033))//justosierra
                .add(new LatLng(24.026691, -104.638174))
                .add(new LatLng(24.026743, -104.637165))
                .add(new LatLng(24.026800, -104.636184))
                .add(new LatLng(24.026842, -104.635204))
                .add(new LatLng(24.026840, -104.634182))
                .add(new LatLng(24.026859, -104.633209))
                .add(new LatLng(24.026913, -104.631227))
                .add(new LatLng(24.026962, -104.629401))
                .add(new LatLng(24.026963, -104.627452))//up
                .add(new LatLng(24.027092, -104.627434))
                .add(new LatLng(24.027252, -104.627437))
                .add(new LatLng(24.028136, -104.627315))
                .add(new LatLng(24.029040, -104.627169))
                .add(new LatLng(24.029159, -104.628165))
                .add(new LatLng(24.029227, -104.628813))
                .width(30f)
                .color(Color.argb(80, 37, 10, 205));


        RUTAS.add(new Ruta("Ruta 20 de Noviembre - Cruz Roja", inicio20Nov,fin20Nov, base20Nov,
                rutaInicio20Cruz, rutaVuelta20Cruz, paradas20NovCruzRoja,"3"));

        /*******************************************************************************************
         *********************************** RUTAS BLANCAS ****************************************/

        MarkerOptions baseBlancas = new MarkerOptions()
                .position(new LatLng(24.012697, -104.686624))
                .title("Base")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.base));
        MarkerOptions inicioBlancas = new MarkerOptions()
                .position(new LatLng(24.053366, -104.630706))
                .title("Inicio");
        MarkerOptions finBlancas =  new MarkerOptions()
                .position(new LatLng(24.012641, -104.686602))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        MarkerOptions inicioBlancas3 = new MarkerOptions()
                .position(new LatLng(24.012641, -104.686602))
                .title("Inicio");
        MarkerOptions finBlancas3 =  new MarkerOptions()
                .position(new LatLng(24.009840, -104.665658))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));


        List<LatLng> paradasRuta1 = new ArrayList<>();
        paradasRuta1.add(new LatLng(24.050486, -104.636678));
        paradasRuta1.add(new LatLng(24.048831, -104.638981));
        paradasRuta1.add(new LatLng(24.045243, -104.636904));
        paradasRuta1.add(new LatLng(24.043042, -104.634576));
        paradasRuta1.add(new LatLng(24.041631, -104.633267));
        paradasRuta1.add(new LatLng(24.039274, -104.633101));
        paradasRuta1.add(new LatLng(24.033946, -104.639015));
        paradasRuta1.add(new LatLng(24.033506, -104.641566));
        paradasRuta1.add(new LatLng(24.034304, -104.648034));
        paradasRuta1.add(new LatLng(24.034914, -104.653417));
        paradasRuta1.add(new LatLng(24.034905, -104.658370));//glorieta
        paradasRuta1.add(new LatLng(24.035430, -104.661550));//AMCCI
        paradasRuta1.add(new LatLng(24.034964, -104.668113));//patoni
        paradasRuta1.add(new LatLng(24.034402, -104.674708));//post estacion ferro
        paradasRuta1.add(new LatLng(24.035402, -104.676275));//subiendo felipe
        paradasRuta1.add(new LatLng(24.036000, -104.681736));//encino
        paradasRuta1.add(new LatLng(24.033651, -104.684754));//sabino
        paradasRuta1.add(new LatLng(24.031997, -104.685655));//arizona
        paradasRuta1.add(new LatLng(24.030356, -104.686846));//sahua
        paradasRuta1.add(new LatLng(24.028188, -104.684754));//delanormal
        paradasRuta1.add(new LatLng(24.022288, -104.677691));//famenn
        paradasRuta1.add(new LatLng(24.016118, -104.683097));//Diurna
        paradasRuta1.add(new LatLng(24.015659, -104.689266));//estadiobaeball
        paradasRuta1.add(new LatLng(24.012613, -104.686604));//atrasEstadioFut
        paradasRuta1.add(new LatLng(24.011913, -104.676401));//rioBravo
        paradasRuta1.add(new LatLng(24.016492, -104.673985));//Ocampo,BarrioAnalco
        paradasRuta1.add(new LatLng(24.018906, -104.672761));//Volantin
        paradasRuta1.add(new LatLng(24.021806, -104.671910));//Zaragoza
        paradasRuta1.add(new LatLng(24.022134, -104.669735));//Juarez
        paradasRuta1.add(new LatLng(24.022549, -104.667075));//Pasteur
        paradasRuta1.add(new LatLng(24.023119, -104.663851));//CarlosLeonPe..
        paradasRuta1.add(new LatLng(24.024066, -104.658238));//Regato
        paradasRuta1.add(new LatLng(24.024429, -104.655744));//Cuauhtemoc
        paradasRuta1.add(new LatLng(24.024887, -104.652678));//LazaroCardenas
        paradasRuta1.add(new LatLng(24.026303, -104.645829));//Nazas
        paradasRuta1.add(new LatLng(24.028223, -104.644338));//conade
        paradasRuta1.add(new LatLng(24.035816, -104.635736));//TrabajoSocial
        paradasRuta1.add(new LatLng(24.041800, -104.630311));//Blvd.Rosas
        paradasRuta1.add(new LatLng(24.045389, -104.636238));//DeLasCamelias
        paradasRuta1.add(new LatLng(24.048612, -104.639151));//MadreSelva
        paradasRuta1.add(new LatLng(24.050448, -104.636619));//Zapatilla
        List<LatLng> paradasRuta2 = new ArrayList<>();
        paradasRuta2.add(new LatLng(24.050521, -104.636662));//Zapatilla
        paradasRuta2.add(new LatLng(24.048880, -104.638947));//MadreSelva
        paradasRuta2.add(new LatLng(24.045191, -104.636858));//DeLasCamelias
        paradasRuta2.add(new LatLng(24.042996, -104.634551));//DeLasAzucenas
        paradasRuta2.add(new LatLng(24.041634, -104.633293));//Vuelta
        paradasRuta2.add(new LatLng(24.039295, -104.633089));//Asiano
        paradasRuta2.add(new LatLng(24.035503, -104.636632));//FcoVilla
        paradasRuta2.add(new LatLng(24.033930, -104.638732));//Camionera
        paradasRuta2.add(new LatLng(24.033541, -104.641674));//FelipePesc
        paradasRuta2.add(new LatLng(24.034382, -104.648342));//TEcno
        paradasRuta2.add(new LatLng(24.034944, -104.653397));//Vips
        paradasRuta2.add(new LatLng(24.034225, -104.657392));//Elorreaga
        paradasRuta2.add(new LatLng(24.032893, -104.657142));//Gomez
        paradasRuta2.add(new LatLng(24.031781, -104.656938));//Paloma
        paradasRuta2.add(new LatLng(24.029282, -104.656485));//AquilesSerdan
        paradasRuta2.add(new LatLng(24.027295, -104.656139));//lapulgga
        paradasRuta2.add(new LatLng(24.025720, -104.655828));//5
        paradasRuta2.add(new LatLng(24.024360, -104.663995));//CarlosLeon
        paradasRuta2.add(new LatLng(24.023990, -104.666237));//PAtoni
        paradasRuta2.add(new LatLng(24.023622, -104.668654));//Victoria
        paradasRuta2.add(new LatLng(24.023370, -104.670298));//PlazaArmas
        paradasRuta2.add(new LatLng(24.022782, -104.673729));//Independencia
        paradasRuta2.add(new LatLng(24.019297, -104.675236));//Volantin
        paradasRuta2.add(new LatLng(24.014546, -104.676823));//PrimoVerdad
        paradasRuta2.add(new LatLng(24.012295, -104.677778));//RioPanuco
        paradasRuta2.add(new LatLng(24.012567, -104.680039));//Huyuapan
        paradasRuta2.add(new LatLng(24.012836, -104.683314));//Durango
        paradasRuta2.add(new LatLng(24.012614, -104.686608));//Checador
        paradasRuta2.add(new LatLng(24.013062, -104.690179));//GonzalezVega
        paradasRuta2.add(new LatLng(24.015502, -104.689101));//FrenteEstadioBeis
        paradasRuta2.add(new LatLng(24.015723, -104.683544));//Diurna
        paradasRuta2.add(new LatLng(24.016181, -104.681666));//CerroCruz
        paradasRuta2.add(new LatLng(24.014542, -104.676830));//PrimoVerdad
        paradasRuta2.add(new LatLng(24.016081, -104.675500));//Allende
        paradasRuta2.add(new LatLng(24.020930, -104.676863));//FransicoSarabia
        paradasRuta2.add(new LatLng(24.023767, -104.679306));//FannyAnit
        paradasRuta2.add(new LatLng(24.027835, -104.683984));//parque
        paradasRuta2.add(new LatLng(24.033047, -104.684596));//licArmando
        paradasRuta2.add(new LatLng(24.036063, -104.676461));//FelipeBaja
        paradasRuta2.add(new LatLng(24.034625, -104.674511));//FelipePescador
        paradasRuta2.add(new LatLng(24.034836, -104.668548));//Pasteur
        paradasRuta2.add(new LatLng(24.035434, -104.662221));//REgato
        paradasRuta2.add(new LatLng(24.034666, -104.652586));//Pasarlien
        paradasRuta2.add(new LatLng(24.034177, -104.648235));//TecnoBioquimica
        paradasRuta2.add(new LatLng(24.033320, -104.641769));//Est1
        paradasRuta2.add(new LatLng(24.035721, -104.635766));//FcoVilla
        paradasRuta2.add(new LatLng(24.041823, -104.630364));//DeLasRosas
        paradasRuta2.add(new LatLng(24.045378, -104.636203));//Camelias
        paradasRuta2.add(new LatLng(24.048473, -104.639367));//Ferrocarril

        List<LatLng> paradasRuta3 = new ArrayList<>();

        /************************************** RUTA 1 ********************************************/

        PolylineOptions rutaInicio1 = new PolylineOptions()
                .add(new LatLng(24.053408, -104.630736))
                .add(new LatLng(24.054067, -104.631037))
                .add(new LatLng(24.054290, -104.631185))
                .add(new LatLng(24.053151, -104.632807))
                .add(new LatLng(24.051809, -104.634706))
                .add(new LatLng(24.051141, -104.635694))
                .add(new LatLng(24.050391, -104.636781))
                .add(new LatLng(24.049571, -104.637937))
                .add(new LatLng(24.048744, -104.639066))
                .add(new LatLng(24.048173, -104.639881))//blvd de las rosas
                .add(new LatLng(24.047882, -104.639610))
                .add(new LatLng(24.046477, -104.638140))
                .add(new LatLng(24.045146, -104.636756))
                .add(new LatLng(24.044032, -104.635593))
                .add(new LatLng(24.042955, -104.634458))
                .add(new LatLng(24.042053, -104.633513))
                .add(new LatLng(24.041885, -104.633381))
                .add(new LatLng(24.041576, -104.633201))
                .add(new LatLng(24.041310, -104.633123))
                .add(new LatLng(24.041005, -104.633074))
                .add(new LatLng(24.040565, -104.633086))
                .add(new LatLng(24.039712, -104.633090))
                .add(new LatLng(24.038704, -104.633082))//blvd Fco Villa
                .add(new LatLng(24.038197, -104.633658))
                .add(new LatLng(24.037783, -104.634110))
                .add(new LatLng(24.037392, -104.634309))
                .add(new LatLng(24.036802, -104.634955))
                .add(new LatLng(24.036465, -104.635352))
                .add(new LatLng(24.035853, -104.636185))//pasandopeatonaltopandonaranja
                .add(new LatLng(24.035433, -104.636651))
                .add(new LatLng(24.034967, -104.637098))
                .add(new LatLng(24.034811, -104.637309))
                .add(new LatLng(24.033844, -104.638389))
                .add(new LatLng(24.033883, -104.638604))
                .add(new LatLng(24.033934, -104.638923))
                .add(new LatLng(24.033924, -104.639099))
                .add(new LatLng(24.033890, -104.639272))
                .add(new LatLng(24.033799, -104.639477))
                .add(new LatLng(24.033682, -104.639617))
                .add(new LatLng(24.033578, -104.639683))
                .add(new LatLng(24.033333, -104.639786))//blvd felipe pescador
                .add(new LatLng(24.033412, -104.640429))
                .add(new LatLng(24.033421, -104.640713))
                .add(new LatLng(24.033390, -104.640949))
                .add(new LatLng(24.033611, -104.642511))
                .add(new LatLng(24.033786, -104.643864))
                .add(new LatLng(24.033936, -104.644940))
                .add(new LatLng(24.034120, -104.646631))
                .add(new LatLng(24.034272, -104.647917))
                .add(new LatLng(24.034377, -104.648853))
                .add(new LatLng(24.034417, -104.649123))
                .add(new LatLng(24.034562, -104.650447))
                .add(new LatLng(24.034687, -104.651670))
                .add(new LatLng(24.034815, -104.652698))
                .add(new LatLng(24.034983, -104.654191))
                .add(new LatLng(24.035151, -104.655949))
                .add(new LatLng(24.035241, -104.656568))//Inicia Glorieta
                .add(new LatLng(24.035057, -104.657259))
                .add(new LatLng(24.035057, -104.657757))
                .add(new LatLng(24.034831, -104.657673))
                .add(new LatLng(24.034627, -104.657840))
                .add(new LatLng(24.034600, -104.657994))
                .add(new LatLng(24.034616, -104.658051))
                .add(new LatLng(24.034792, -104.658260))
                .add(new LatLng(24.034948, -104.658376))
                .add(new LatLng(24.035271, -104.658445))//termina vuelta glorieta
                .add(new LatLng(24.035410, -104.658870))
                .add(new LatLng(24.035471, -104.659109))
                .add(new LatLng(24.035519, -104.659666))
                .add(new LatLng(24.035516, -104.659919))
                .add(new LatLng(24.035439, -104.661046))//regato
                .add(new LatLng(24.035326, -104.662537))
                .add(new LatLng(24.035264, -104.663456))
                .add(new LatLng(24.035183, -104.664500))
                .add(new LatLng(24.035100, -104.665510))
                .add(new LatLng(24.035023, -104.666687))
                .add(new LatLng(24.034934, -104.667802))//patoni
                .add(new LatLng(24.034833, -104.669267))
                .add(new LatLng(24.034785, -104.669865))
                .add(new LatLng(24.034740, -104.670569))
                .add(new LatLng(24.034710, -104.670964))
                .add(new LatLng(24.034673, -104.671499))//Constitucion
                .add(new LatLng(24.034602, -104.672364))
                .add(new LatLng(24.034503, -104.673404))//zaragosa
                .add(new LatLng(24.034422, -104.674552))
                .add(new LatLng(24.034371, -104.675497))//vueltaarribaFelipePesc
                .add(new LatLng(24.034444, -104.675683))
                .add(new LatLng(24.034667, -104.675846))
                .add(new LatLng(24.036083, -104.676573))
                .add(new LatLng(24.036188, -104.676919))
                .add(new LatLng(24.036108, -104.678121))
                .add(new LatLng(24.036120, -104.678534))
                .add(new LatLng(24.036230, -104.678716))//vueltaizqFelipePesc
                .add(new LatLng(24.036100, -104.680060))
                .add(new LatLng(24.036083, -104.680481))
                .add(new LatLng(24.035997, -104.681243))
                .add(new LatLng(24.035895, -104.681758))
                .add(new LatLng(24.035681, -104.682455))//felipeBaja
                .add(new LatLng(24.035311, -104.683123))
                .add(new LatLng(24.034731, -104.683885))
                .add(new LatLng(24.033964, -104.684494))//fresno
                .add(new LatLng(24.033408, -104.684813))//sabino
                .add(new LatLng(24.032835, -104.685051))
                .add(new LatLng(24.032661, -104.685239))
                .add(new LatLng(24.031478, -104.685899))
                .add(new LatLng(24.031000, -104.686234))
                .add(new LatLng(24.030260, -104.686900))
                .add(new LatLng(24.029981, -104.687251))
                .add(new LatLng(24.029714, -104.687168))
                .add(new LatLng(24.029440, -104.686969))//esquina de la normal
                .add(new LatLng(24.028756, -104.685752))
                .add(new LatLng(24.028161, -104.684660))
                .add(new LatLng(24.028026, -104.684451))//bajaXNormal
                .add(new LatLng(24.028008, -104.684426))
                .add(new LatLng(24.026800, -104.683044))
                .add(new LatLng(24.026075, -104.682186))//aquiles
                .add(new LatLng(24.025110, -104.681049))
                .add(new LatLng(24.024461, -104.680314))
                .add(new LatLng(24.024137, -104.679933))
                .add(new LatLng(24.023677, -104.679423))
                .add(new LatLng(24.022910, -104.678511))//derechoUje
                .add(new LatLng(24.022371, -104.677696))
                .add(new LatLng(24.0222622271267, -104.67762541025877))
                .add(new LatLng(24.022109, -104.677605))//plazaUniv-famenn
                .add(new LatLng(24.021731, -104.678058))
                .add(new LatLng(24.020837, -104.678659))//empiezaAvUnviersidad
                .add(new LatLng(24.019906, -104.679287))
                .add(new LatLng(24.018741, -104.680082))//ocampo
                .add(new LatLng(24.018165, -104.680514))
                .add(new LatLng(24.017462, -104.680972))
                .add(new LatLng(24.016911, -104.681348))
                .add(new LatLng(24.016788, -104.681490))
                .add(new LatLng(24.016602, -104.681957))
                .add(new LatLng(24.016274, -104.682726))
                .add(new LatLng(24.015563, -104.684164))//diurna
                .add(new LatLng(24.015561, -104.685264))
                .add(new LatLng(24.015593, -104.685731))
                .add(new LatLng(24.015578, -104.687713))
                .add(new LatLng(24.015593, -104.688804))//estadioBaseBall
                .add(new LatLng(24.015718, -104.690103))
                .add(new LatLng(24.015787, -104.690290))
                .add(new LatLng(24.014618, -104.690419))//bibliotecacentarl
                .add(new LatLng(24.013067, -104.690593))
                .add(new LatLng(24.013053, -104.690392))//gonzalesdelavega
                .add(new LatLng(24.012942, -104.689319))
                .add(new LatLng(24.012761, -104.687579))
                .add(new LatLng(24.01264082014323, -104.68662690371276))
                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVuelta1 = new PolylineOptions()
                .add(new LatLng(24.0126374512844, -104.6865762770176))
                .add(new LatLng(24.012514, -104.685301))
                .add(new LatLng(24.012376, -104.684169)) //Blvd.Dgo
                .add(new LatLng(24.012578, -104.683567))
                .add(new LatLng(24.012908, -104.682615))
                .add(new LatLng(24.012722, -104.681266))
                .add(new LatLng(24.012550, -104.680177))
                .add(new LatLng(24.012369, -104.679112))//RioBravo
                .add(new LatLng(24.012158, -104.677717))
                .add(new LatLng(24.011919, -104.676302))
                .add(new LatLng(24.012554, -104.675996))
                .add(new LatLng(24.013279, -104.675645))
                .add(new LatLng(24.013769, -104.675409))//topeBlvdDurango
                .add(new LatLng(24.014668, -104.674950))
                .add(new LatLng(24.015724, -104.674443))
                .add(new LatLng(24.016410, -104.674097))
                .add(new LatLng(24.017944, -104.673333))
                .add(new LatLng(24.017993, -104.673242))
                .add(new LatLng(24.018985, -104.672765))
                .add(new LatLng(24.019669, -104.672478))
                .add(new LatLng(24.020350, -104.672151))
                .add(new LatLng(24.020602412045155, -104.67193208634853))
                .add(new LatLng(24.020681, -104.672577))
                .add(new LatLng(24.020789, -104.673202))//dolores sube a Allende
                .add(new LatLng(24.021135, -104.673128))
                .add(new LatLng(24.021566, -104.673168))
                .add(new LatLng(24.021632, -104.673160))
                .add(new LatLng(24.021835, -104.671816))//zaragoza
                .add(new LatLng(24.022029, -104.670518))//consti
                .add(new LatLng(24.022178, -104.669638))
                .add(new LatLng(24.022389, -104.668268))
                .add(new LatLng(24.022600, -104.666927))//pasteur
                .add(new LatLng(24.022867, -104.665447))
                .add(new LatLng(24.022980, -104.664752))
                .add(new LatLng(24.023112, -104.663980))//carlosLeonPe..
                .add(new LatLng(24.023305, -104.662708))
                .add(new LatLng(24.023486, -104.661555))
                .add(new LatLng(24.023596, -104.661137))
                .add(new LatLng(24.023694, -104.660630))//MiguelCervantes
                .add(new LatLng(24.023809, -104.659785))
                .add(new LatLng(24.023919, -104.659136))
                .add(new LatLng(24.023973, -104.658774))
                .add(new LatLng(24.024029, -104.658635))
                .add(new LatLng(24.024107, -104.658112))//Regato
                .add(new LatLng(24.024203, -104.657484))
                .add(new LatLng(24.024301, -104.656618))
                .add(new LatLng(24.024463, -104.655572))//Cuauhtemoc
                .add(new LatLng(24.024581, -104.654937))
                .add(new LatLng(24.024728, -104.654052))
                .add(new LatLng(24.024777, -104.653510))
                .add(new LatLng(24.024926, -104.652555))//Lazaro
                .add(new LatLng(24.025068, -104.651501))
                .add(new LatLng(24.025175, -104.650827))
                .add(new LatLng(24.025261, -104.650323))
                .add(new LatLng(24.025334, -104.649813))
                .add(new LatLng(24.025501, -104.648815))
                .add(new LatLng(24.025641, -104.647788))//Educacion
                .add(new LatLng(24.025773, -104.647066))
                .add(new LatLng(24.025962, -104.645872))//Nazas
                .add(new LatLng(24.026871, -104.645837))
                .add(new LatLng(24.026993, -104.645706))
                .add(new LatLng(24.027223, -104.645545))
                .add(new LatLng(24.027516, -104.645224))
                .add(new LatLng(24.028275, -104.644347))
                .add(new LatLng(24.028921, -104.643625))
                .add(new LatLng(24.029337, -104.643156))
                .add(new LatLng(24.029709, -104.642692))
                .add(new LatLng(24.029846, -104.642480))
                .add(new LatLng(24.030476, -104.641748))
                .add(new LatLng(24.031309, -104.640849))
                .add(new LatLng(24.032093, -104.640007))//terminaHeroicoEmpiezaF.Villa
                .add(new LatLng(24.032387, -104.639677))
                .add(new LatLng(24.032654, -104.639462))
                .add(new LatLng(24.033161, -104.638896))
                .add(new LatLng(24.034038, -104.637936))
                .add(new LatLng(24.034486, -104.637421))
                .add(new LatLng(24.034640, -104.637206))
                .add(new LatLng(24.035213, -104.636578))//Av. Jesus Garcia
                .add(new LatLng(24.035407, -104.636404))
                .add(new LatLng(24.035880, -104.635878))
                .add(new LatLng(24.036331, -104.635309))
                .add(new LatLng(24.037068, -104.634488))
                .add(new LatLng(24.037651, -104.633842))
                .add(new LatLng(24.038368, -104.633048))
                .add(new LatLng(24.039284, -104.632031))
                .add(new LatLng(24.040051, -104.631167))//CalleUno
                .add(new LatLng(24.040722, -104.630443))
                .add(new LatLng(24.041349, -104.629727))//de las rosas
                .add(new LatLng(24.041584, -104.629963))
                .add(new LatLng(24.041751, -104.630266))
                .add(new LatLng(24.041837, -104.630558))//Magnolia
                .add(new LatLng(24.042376, -104.630751))
                .add(new LatLng(24.043194, -104.631038))
                .add(new LatLng(24.043816, -104.631253))
                .add(new LatLng(24.044274, -104.631422))
                .add(new LatLng(24.044830, -104.631607))//Azucenas
                .add(new LatLng(24.044404, -104.632455))
                .add(new LatLng(24.044049, -104.633029))
                .add(new LatLng(24.043593, -104.633692))
                .add(new LatLng(24.043287, -104.634097))//DeLasRosas
                .add(new LatLng(24.044115, -104.634953))
                .add(new LatLng(24.044642, -104.635484))
                .add(new LatLng(24.045445, -104.636345))//DeLasCamellas
                .add(new LatLng(24.046379, -104.637327))
                .add(new LatLng(24.047452, -104.638432))
                .add(new LatLng(24.048358, -104.639395))
                .add(new LatLng(24.048419, -104.639505))
                .add(new LatLng(24.048737, -104.639076))//madreselva
                .add(new LatLng(24.049518, -104.637976))
                .add(new LatLng(24.050365, -104.636788))
                .add(new LatLng(24.051109, -104.635718))
                .add(new LatLng(24.051785, -104.634704))
                .add(new LatLng(24.052517, -104.633671))
                .add(new LatLng(24.053411, -104.632408))
                .add(new LatLng(24.054271, -104.631182))
                .add(new LatLng(24.054073, -104.631051))
                .add(new LatLng(24.053348, -104.630724))
                .width(30f)
                .color(Color.argb(80, 37, 10, 205));


        RUTAS.add(new Ruta("Ruta 1", inicioBlancas,finBlancas, baseBlancas,
                rutaInicio1, rutaVuelta1, paradasRuta1,"4"));

        PolylineOptions ruta1Retorno = new PolylineOptions()
                .add(new LatLng(24.053408, -104.630736))
                .add(new LatLng(24.054067, -104.631037))
                .add(new LatLng(24.054290, -104.631185))
                .add(new LatLng(24.053151, -104.632807))
                .add(new LatLng(24.051809, -104.634706))
                .add(new LatLng(24.051141, -104.635694))
                .add(new LatLng(24.050391, -104.636781))//Zapatilla
                .add(new LatLng(24.048846, -104.636085))
                .add(new LatLng(24.047832, -104.635629))
                .add(new LatLng(24.047379, -104.635445))
                .add(new LatLng(24.045838, -104.634769))
                .add(new LatLng(24.045321, -104.634525))//orquidea
                .add(new LatLng(24.046095, -104.633405))
                .add(new LatLng(24.046715, -104.632442))//magnolia
                .add(new LatLng(24.046296, -104.632246))
                .add(new LatLng(24.045353, -104.631822))
                .add(new LatLng(24.044829, -104.631613))
                .add(new LatLng(24.043819, -104.631259))
                .add(new LatLng(24.043187, -104.631039))
                .add(new LatLng(24.042359, -104.630747))//Madreselva
                .add(new LatLng(24.042383, -104.631125))
                .add(new LatLng(24.042376, -104.631830))
                .add(new LatLng(24.042401, -104.632139))
                .add(new LatLng(24.042438, -104.632340))
                .add(new LatLng(24.042514, -104.632534))
                .add(new LatLng(24.042597, -104.632649))
                .add(new LatLng(24.043579, -104.633695))
                .add(new LatLng(24.043270, -104.634103))
                .add(new LatLng(24.042974, -104.634454))
                .width(35f)
                .color(Color.argb(100, 51, 255, 51));    // Verde
        RUTAS.add(new Ruta("Ruta 1 retorno", null,null, null,
                ruta1Retorno, ruta1Retorno, null,"4R"));

        /************************************** RUTA 2 ********************************************/

        PolylineOptions rutaInicio2 = new PolylineOptions()
                .add(new LatLng(24.053408, -104.630736))
                .add(new LatLng(24.054067, -104.631037))
                .add(new LatLng(24.054290, -104.631185))
                .add(new LatLng(24.053151, -104.632807))
                .add(new LatLng(24.051809, -104.634706))
                .add(new LatLng(24.051141, -104.635694))
                .add(new LatLng(24.050391, -104.636781))
                .add(new LatLng(24.049571, -104.637937))
                .add(new LatLng(24.048744, -104.639066))
                .add(new LatLng(24.048173, -104.639881))//blvd de las rosas
                .add(new LatLng(24.047882, -104.639610))
                .add(new LatLng(24.046477, -104.638140))
                .add(new LatLng(24.045146, -104.636756))
                .add(new LatLng(24.044032, -104.635593))
                .add(new LatLng(24.042955, -104.634458))
                .add(new LatLng(24.042053, -104.633513))
                .add(new LatLng(24.041885, -104.633381))
                .add(new LatLng(24.041576, -104.633201))
                .add(new LatLng(24.041310, -104.633123))
                .add(new LatLng(24.041005, -104.633074))
                .add(new LatLng(24.040565, -104.633086))
                .add(new LatLng(24.039712, -104.633090))
                .add(new LatLng(24.038704, -104.633082))//blvd Fco Villa
                .add(new LatLng(24.038197, -104.633658))
                .add(new LatLng(24.037783, -104.634110))
                .add(new LatLng(24.037392, -104.634309))
                .add(new LatLng(24.036802, -104.634955))
                .add(new LatLng(24.036465, -104.635352))
                .add(new LatLng(24.035853, -104.636185))//pasandopeatonaltopandonaranja
                .add(new LatLng(24.035433, -104.636651))
                .add(new LatLng(24.034967, -104.637098))
                .add(new LatLng(24.034811, -104.637309))
                .add(new LatLng(24.033844, -104.638389))
                .add(new LatLng(24.033883, -104.638604))
                .add(new LatLng(24.033934, -104.638923))
                .add(new LatLng(24.033924, -104.639099))
                .add(new LatLng(24.033890, -104.639272))
                .add(new LatLng(24.033799, -104.639477))
                .add(new LatLng(24.033682, -104.639617))
                .add(new LatLng(24.033578, -104.639683))
                .add(new LatLng(24.033333, -104.639786))//blvd felipe pescador
                .add(new LatLng(24.033412, -104.640429))
                .add(new LatLng(24.033421, -104.640713))
                .add(new LatLng(24.033390, -104.640949))
                .add(new LatLng(24.033611, -104.642511))
                .add(new LatLng(24.033786, -104.643864))
                .add(new LatLng(24.033936, -104.644940))
                .add(new LatLng(24.034120, -104.646631))
                .add(new LatLng(24.034272, -104.647917))
                .add(new LatLng(24.034377, -104.648853))
                .add(new LatLng(24.034417, -104.649123))
                .add(new LatLng(24.034562, -104.650447))
                .add(new LatLng(24.034687, -104.651670))
                .add(new LatLng(24.034815, -104.652698))
                .add(new LatLng(24.034983, -104.654191))
                .add(new LatLng(24.035151, -104.655949))
                .add(new LatLng(24.035241, -104.656568))//Inicia Glorieta
                .add(new LatLng(24.035083, -104.657100))
                .add(new LatLng(24.034958, -104.657229))
                .add(new LatLng(24.034561, -104.657430))
                .add(new LatLng(24.033951, -104.657274))//Elorreaga
                .add(new LatLng(24.032780, -104.657070))
                .add(new LatLng(24.031680, -104.656869))
                .add(new LatLng(24.030894, -104.656730))
                .add(new LatLng(24.030007, -104.656561))
                .add(new LatLng(24.029382, -104.656454))
                .add(new LatLng(24.028701, -104.656315))
                .add(new LatLng(24.027163, -104.656066))//20deNov
                .add(new LatLng(24.026364, -104.655916))
                .add(new LatLng(24.025656, -104.655782))//5deFeb
                .add(new LatLng(24.025497, -104.656815))
                .add(new LatLng(24.025352, -104.657751))
                .add(new LatLng(24.025256, -104.658466))//regato
                .add(new LatLng(24.025178, -104.658729))
                .add(new LatLng(24.024965, -104.659925))
                .add(new LatLng(24.024794, -104.660850))//MiguelCervantes
                .add(new LatLng(24.024659, -104.661673))
                .add(new LatLng(24.024507, -104.662743))
                .add(new LatLng(24.024289, -104.664132))//CarlosLeonDeLaPena
                .add(new LatLng(24.024167, -104.664878))
                .add(new LatLng(24.024030, -104.665659))
                .add(new LatLng(24.023925, -104.666399))//Patoni
                .add(new LatLng(24.023783, -104.667346))
                .add(new LatLng(24.023597, -104.668430))//FcoIMadero
                .add(new LatLng(24.023492, -104.669074))
                .add(new LatLng(24.023394, -104.669790))
                .add(new LatLng(24.023276, -104.670589))//Constitucion
                .add(new LatLng(24.023033, -104.671941))
                .add(new LatLng(24.022800, -104.673314))//Idalgo
                .add(new LatLng(24.022690, -104.673990))//Independencia
                .add(new LatLng(24.021526, -104.673824))
                .add(new LatLng(24.021171, -104.673773))
                .add(new LatLng(24.021068, -104.673926))
                .add(new LatLng(24.021110, -104.674267))
                .add(new LatLng(24.021083, -104.674369))
                .add(new LatLng(24.020975, -104.674444))
                .add(new LatLng(24.020659, -104.674583))//Morelos
                .add(new LatLng(24.019498, -104.675082))
                .add(new LatLng(24.018403, -104.675576))//Morelos-JuanEGarcia
                .add(new LatLng(24.017178, -104.676134))
                .add(new LatLng(24.016396, -104.676491))
                .add(new LatLng(24.015891, -104.676698))//MorelosLerdo
                .add(new LatLng(24.015759, -104.676754))
                .add(new LatLng(24.015538, -104.676730))
                .add(new LatLng(24.015423, -104.676666))
                .add(new LatLng(24.015337, -104.676561))//FilomenoMata
                .add(new LatLng(24.014907, -104.676730))
                .add(new LatLng(24.014483, -104.676896))
                .add(new LatLng(24.014203, -104.677011))
                .add(new LatLng(24.013791, -104.677134))//PrimodeVerdadCalleLaLlave
                .add(new LatLng(24.012848, -104.677493))
                .add(new LatLng(24.012482, -104.677648))
                .add(new LatLng(24.012286, -104.677734))
                .add(new LatLng(24.012171, -104.677841))//RioPanuco
                .add(new LatLng(24.012367, -104.679110))
                .add(new LatLng(24.012539, -104.680172))
                .add(new LatLng(24.012715, -104.681272))
                .add(new LatLng(24.012865308445942, -104.68239940702915))
                .add(new LatLng(24.012943, -104.682847))//blvd Durango
                .add(new LatLng(24.012696, -104.683582))
                .add(new LatLng(24.012414, -104.684427))//GonzalesDeLaVega
                .add(new LatLng(24.012524, -104.685419))
                .add(new LatLng(24.0126374512844, -104.6865762770176))//Checador

                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVuelta2 = new PolylineOptions()
                .add(new LatLng(24.01264082014323, -104.68662690371276))
                .add(new LatLng(24.012770, -104.687581))
                .add(new LatLng(24.012946, -104.689316))
                .add(new LatLng(24.013061, -104.690410))//Sahuatoba
                .add(new LatLng(24.014271, -104.690279))//lenguas
                .add(new LatLng(24.015623, -104.690131))
                .add(new LatLng(24.015498, -104.688704))//EStadioBeis
                .add(new LatLng(24.015478, -104.687712))
                .add(new LatLng(24.015480, -104.686095))
                .add(new LatLng(24.015490, -104.685741))
                .add(new LatLng(24.015461, -104.685288))//Frontones
                .add(new LatLng(24.015485, -104.684161))
                .add(new LatLng(24.015892, -104.683289))
                .add(new LatLng(24.016206, -104.682645))
                .add(new LatLng(24.016191, -104.682007))//RioPapaloapan
                .add(new LatLng(24.016225, -104.681790))
                .add(new LatLng(24.016095, -104.680599))//Glorieta
                .add(new LatLng(24.015983, -104.680561))
                .add(new LatLng(24.015929, -104.680413))
                .add(new LatLng(24.015944, -104.680357))//pasandoGlorieta
                .add(new LatLng(24.015469, -104.679220))
                .add(new LatLng(24.015052, -104.678246))
                .add(new LatLng(24.014858, -104.678039))
                .add(new LatLng(24.014635, -104.677881))
                .add(new LatLng(24.014488, -104.677825))
                .add(new LatLng(24.014010, -104.677704))//BlvdDurango
                .add(new LatLng(24.014034, -104.677489))
                .add(new LatLng(24.014122, -104.677041))//PrimoDeVerdad
                .add(new LatLng(24.014908, -104.676733))
                .add(new LatLng(24.015344, -104.676567))
                .add(new LatLng(24.015516, -104.676513))
                .add(new LatLng(24.015621, -104.676368))
                .add(new LatLng(24.015660, -104.676186))//CalleLerdo
                .add(new LatLng(24.015508, -104.675829))//CalleAllende
                .add(new LatLng(24.016155, -104.675488))
                .add(new LatLng(24.016763, -104.675169))
                .add(new LatLng(24.016863, -104.675142))
                .add(new LatLng(24.017992, -104.674571))//JuanEGarcia
                .add(new LatLng(24.018406, -104.675571))
                .add(new LatLng(24.018818, -104.676604))
                .add(new LatLng(24.019279, -104.677747))//Fray
                .add(new LatLng(24.020286, -104.677248))
                .add(new LatLng(24.021036, -104.676859))//Sarabia
                .add(new LatLng(24.021712, -104.677944))
                .add(new LatLng(24.021933, -104.677689))
                .add(new LatLng(24.022043, -104.677517))//Glorieta
                .add(new LatLng(24.022067, -104.677410))
                .add(new LatLng(24.022158, -104.677356))
                .add(new LatLng(24.022234, -104.677364))
                .add(new LatLng(24.022337, -104.677520))//FinGlorieta
                .add(new LatLng(24.022896, -104.678325))
                .add(new LatLng(24.023268, -104.678837))
                .add(new LatLng(24.023802, -104.679422))//feca
                .add(new LatLng(24.024201, -104.679838))
                .add(new LatLng(24.024640, -104.680329))
                .add(new LatLng(24.025142, -104.680927))
                .add(new LatLng(24.025630, -104.681498))
                .add(new LatLng(24.026118, -104.682064))//AquilesSerdan
                .add(new LatLng(24.026865, -104.682960))
                .add(new LatLng(24.027899, -104.684132))
                .add(new LatLng(24.028213, -104.684558))
                .add(new LatLng(24.028482, -104.685052))
                .add(new LatLng(24.028989, -104.685967))//Normal
                .add(new LatLng(24.029496, -104.686866))
                .add(new LatLng(24.029577, -104.686944))
                .add(new LatLng(24.029982, -104.686482))
                .add(new LatLng(24.030359, -104.686131))
                .add(new LatLng(24.030658, -104.685898))
                .add(new LatLng(24.031185, -104.685541))
                .add(new LatLng(24.031366, -104.685546))//LicArmando
                .add(new LatLng(24.032194, -104.685071))
                .add(new LatLng(24.032919, -104.684717))
                .add(new LatLng(24.033625, -104.684384))
                .add(new LatLng(24.033921, -104.684245))
                .add(new LatLng(24.034303, -104.684020))
                .add(new LatLng(24.034450, -104.683886))
                .add(new LatLng(24.034952, -104.683382))
                .add(new LatLng(24.035248, -104.682956))//ElPino
                .add(new LatLng(24.035483, -104.682495))
                .add(new LatLng(24.035615, -104.682200))
                .add(new LatLng(24.035767, -104.681645))
                .add(new LatLng(24.035850, -104.681358))
                .add(new LatLng(24.035921, -104.680846))
                .add(new LatLng(24.035923, -104.680460))
                .add(new LatLng(24.035945, -104.680321))
                .add(new LatLng(24.035947, -104.680010))
                .add(new LatLng(24.036028, -104.679069))//Costa
                .add(new LatLng(24.036051, -104.678804))
                .add(new LatLng(24.036208, -104.678313))
                .add(new LatLng(24.036279, -104.677943))
                .add(new LatLng(24.036323, -104.677235))
                .add(new LatLng(24.036340, -104.676795))
                .add(new LatLng(24.036413, -104.676623))//Hidalgo
                .add(new LatLng(24.036144, -104.676443))
                .add(new LatLng(24.035422, -104.676071))
                .add(new LatLng(24.034797, -104.675749))//Plaza450
                .add(new LatLng(24.034594, -104.675551))
                .add(new LatLng(24.034521, -104.675264))//FelipePescador
                .add(new LatLng(24.034612, -104.673580))
                .add(new LatLng(24.034651, -104.673373))
                .add(new LatLng(24.034722, -104.672300))
                .add(new LatLng(24.034827, -104.670945))
                .add(new LatLng(24.034925, -104.669832))
                .add(new LatLng(24.035072, -104.667871))
                .add(new LatLng(24.035065, -104.667656))
                .add(new LatLng(24.035131, -104.666757))
                .add(new LatLng(24.035209, -104.665521))
                .add(new LatLng(24.035285, -104.664494))//Ramirez
                .add(new LatLng(24.035444, -104.662544))
                .add(new LatLng(24.035547, -104.661055))//REgato
                .add(new LatLng(24.035628, -104.659926))
                .add(new LatLng(24.035621, -104.659526))
                .add(new LatLng(24.035621, -104.659526))
                .add(new LatLng(24.035535, -104.658874))
                .add(new LatLng(24.035498, -104.658539))//EmpezaraGlorietaFelipePEscador
                .add(new LatLng(24.035231, -104.658338))
                .add(new LatLng(24.034969, -104.658228))
                .add(new LatLng(24.034798, -104.658102))
                .add(new LatLng(24.034634, -104.657839))
                .add(new LatLng(24.034541, -104.657421))
                .add(new LatLng(24.034590, -104.657145))
                .add(new LatLng(24.034735, -104.656917))
                .add(new LatLng(24.034902, -104.656745))
                .add(new LatLng(24.034977, -104.656733))
                .add(new LatLng(24.035046, -104.656687))
                .add(new LatLng(24.035077, -104.656655))
                .add(new LatLng(24.035116, -104.656576))
                .add(new LatLng(24.035122, -104.656494))//TerminaGlorietaCalleChica
                .add(new LatLng(24.035116, -104.656404))
                .add(new LatLng(24.035040, -104.655932))
                .add(new LatLng(24.034944, -104.654997))//Galileo
                .add(new LatLng(24.034864, -104.654157))
                .add(new LatLng(24.034831, -104.653914))
                .add(new LatLng(24.034771, -104.653426))
                .add(new LatLng(24.034691, -104.652804))
                .add(new LatLng(24.034621, -104.652128))
                .add(new LatLng(24.034545, -104.651455))
                .add(new LatLng(24.034465, -104.650823))
                .add(new LatLng(24.034404, -104.650152))//ValleSuchil
                .add(new LatLng(24.034328, -104.649455))
                .add(new LatLng(24.034253, -104.648746))//EsquinaITD
                .add(new LatLng(24.034124, -104.647795))
                .add(new LatLng(24.034004, -104.646903))
                .add(new LatLng(24.033875, -104.645877))
                .add(new LatLng(24.033743, -104.644808))//SalidaEsteTecnoNorte
                .add(new LatLng(24.033672, -104.644205))
                .add(new LatLng(24.033458, -104.642554))
                .add(new LatLng(24.033440, -104.642448))
                .add(new LatLng(24.033376, -104.642207))
                .add(new LatLng(24.033332, -104.641919))
                .add(new LatLng(24.033309, -104.641542))
                .add(new LatLng(24.033356, -104.641462))//OfficeDe
                .add(new LatLng(24.033221, -104.639957))
                .add(new LatLng(24.033151, -104.638912))//FcoVilla
                .add(new LatLng(24.034038, -104.637936))
                .add(new LatLng(24.034486, -104.637421))
                .add(new LatLng(24.034640, -104.637206))
                .add(new LatLng(24.035213, -104.636578))//Av. Jesus Garcia
                .add(new LatLng(24.035407, -104.636404))
                .add(new LatLng(24.035880, -104.635878))
                .add(new LatLng(24.036331, -104.635309))
                .add(new LatLng(24.037068, -104.634488))
                .add(new LatLng(24.037651, -104.633842))
                .add(new LatLng(24.038368, -104.633048))
                .add(new LatLng(24.039284, -104.632031))
                .add(new LatLng(24.040051, -104.631167))//CalleUno
                .add(new LatLng(24.040722, -104.630443))
                .add(new LatLng(24.041349, -104.629727))//de las rosas
                .add(new LatLng(24.041584, -104.629963))
                .add(new LatLng(24.041751, -104.630266))
                .add(new LatLng(24.041837, -104.630558))//Magnolia
                .add(new LatLng(24.042376, -104.630751))
                .add(new LatLng(24.043194, -104.631038))
                .add(new LatLng(24.043816, -104.631253))
                .add(new LatLng(24.044274, -104.631422))
                .add(new LatLng(24.044830, -104.631607))//Azucenas
                .add(new LatLng(24.044404, -104.632455))
                .add(new LatLng(24.044049, -104.633029))
                .add(new LatLng(24.043593, -104.633692))
                .add(new LatLng(24.043287, -104.634097))//DeLasRosas
                .add(new LatLng(24.044115, -104.634953))
                .add(new LatLng(24.044642, -104.635484))
                .add(new LatLng(24.045445, -104.636345))//DeLasCamellas
                .add(new LatLng(24.046379, -104.637327))
                .add(new LatLng(24.047452, -104.638432))
                .add(new LatLng(24.048358, -104.639395))
                .add(new LatLng(24.048419, -104.639505))
                .add(new LatLng(24.048737, -104.639076))//madreselva
                .add(new LatLng(24.049518, -104.637976))
                .add(new LatLng(24.050365, -104.636788))
                .add(new LatLng(24.051109, -104.635718))
                .add(new LatLng(24.051785, -104.634704))
                .add(new LatLng(24.052517, -104.633671))
                .add(new LatLng(24.053411, -104.632408))
                .add(new LatLng(24.054271, -104.631182))
                .add(new LatLng(24.054073, -104.631051))
                .add(new LatLng(24.053348, -104.630724))

                .width(30f)
                .color(Color.argb(80, 37, 10, 205));


        RUTAS.add(new Ruta("Ruta 2", inicioBlancas,finBlancas, baseBlancas,
                rutaInicio2, rutaVuelta2, paradasRuta2,"5"));

        PolylineOptions ruta2Retorno = new PolylineOptions()
                .add(new LatLng(24.053408, -104.630736))
                .add(new LatLng(24.054067, -104.631037))
                .add(new LatLng(24.054290, -104.631185))
                .add(new LatLng(24.053151, -104.632807))
                .add(new LatLng(24.051809, -104.634706))
                .add(new LatLng(24.051141, -104.635694))
                .add(new LatLng(24.050391, -104.636781))//Zapatilla
                .add(new LatLng(24.048846, -104.636085))
                .add(new LatLng(24.047832, -104.635629))
                .add(new LatLng(24.047379, -104.635445))
                .add(new LatLng(24.045838, -104.634769))
                .add(new LatLng(24.045321, -104.634525))//orquidea
                .add(new LatLng(24.046095, -104.633405))
                .add(new LatLng(24.046715, -104.632442))//magnolia
                .add(new LatLng(24.046296, -104.632246))
                .add(new LatLng(24.045353, -104.631822))
                .add(new LatLng(24.044829, -104.631613))
                .add(new LatLng(24.043819, -104.631259))
                .add(new LatLng(24.043187, -104.631039))
                .add(new LatLng(24.042359, -104.630747))//Madreselva
                .add(new LatLng(24.042383, -104.631125))
                .add(new LatLng(24.042376, -104.631830))
                .add(new LatLng(24.042401, -104.632139))
                .add(new LatLng(24.042438, -104.632340))
                .add(new LatLng(24.042514, -104.632534))
                .add(new LatLng(24.042597, -104.632649))
                .add(new LatLng(24.043579, -104.633695))
                .add(new LatLng(24.043270, -104.634103))
                .add(new LatLng(24.042964, -104.634462))
                .add(new LatLng(24.042052, -104.633523))
                .add(new LatLng(24.041834, -104.633346))
                .add(new LatLng(24.041555, -104.633196))
                .add(new LatLng(24.041151, -104.633097))
                .add(new LatLng(24.040703, -104.633086))
                .add(new LatLng(24.040541, -104.633089))
                .add(new LatLng(24.039679, -104.633097))
                .add(new LatLng(24.038797, -104.633113))
                .width(35f)
                .color(Color.argb(100, 51, 255, 51));    // Verde
        RUTAS.add(new Ruta("Ruta 2 retorno", null,null, null,
                ruta2Retorno, ruta2Retorno, null,"5R"));

        /************************************** RUTA 3 ********************************************/

        PolylineOptions rutaInicio3 = new PolylineOptions()
                .add(new LatLng(24.0126374512844, -104.6865762770176))
                .add(new LatLng(24.012514, -104.685301))
                .add(new LatLng(24.012376, -104.684169)) //Blvd.Dgo
                .add(new LatLng(24.012581, -104.683568))
                .add(new LatLng(24.012919, -104.682610))
                .add(new LatLng(24.013282, -104.681534))
                .add(new LatLng(24.013417, -104.681150))
                .add(new LatLng(24.013566, -104.680335))
                .add(new LatLng(24.013691, -104.679571))//RioBravo
                .add(new LatLng(24.013848, -104.678664))
                .add(new LatLng(24.014012, -104.677701))
                .add(new LatLng(24.014044, -104.677484))
                .add(new LatLng(24.014131, -104.677031))//PrimoDeVerdad
                .add(new LatLng(24.014135, -104.676953))
                .add(new LatLng(24.014118, -104.676764))
                .add(new LatLng(24.014081, -104.676590))
                .add(new LatLng(24.014019, -104.676387))
                .add(new LatLng(24.013689, -104.675442))//Belisario
                .add(new LatLng(24.013333, -104.674419))
                .add(new LatLng(24.013088, -104.673753))
                .add(new LatLng(24.012814, -104.673030))//CalleLuna
                .add(new LatLng(24.012625, -104.672595))
                .add(new LatLng(24.012460, -104.672169))
                .add(new LatLng(24.012241, -104.671629))//Urrea
                .add(new LatLng(24.012074, -104.671188))
                .add(new LatLng(24.011993, -104.671009))
                .add(new LatLng(24.011912, -104.670779))
                .add(new LatLng(24.011622, -104.670088))//CalleBravo
                .add(new LatLng(24.011216, -104.669029))
                .add(new LatLng(24.011070, -104.668644))
                .add(new LatLng(24.010872, -104.668179))//Lerdo
                .add(new LatLng(24.010460, -104.667189))
                .add(new LatLng(24.010283, -104.666738))
                .add(new LatLng(24.009851, -104.665646))//DeOcampo
                .add(new LatLng(24.009363, -104.664463))//RaulMAdero
                .add(new LatLng(24.009019, -104.664627))
                .add(new LatLng(24.008703, -104.664749))
                .add(new LatLng(24.008317, -104.664915))//Encino
                .add(new LatLng(24.008775, -104.666135))//ocampo
                .add(new LatLng(24.009449, -104.665857))
                .add(new LatLng(24.009663, -104.665758))
                .add(new LatLng(24.009830, -104.665665))//Fin

                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVuelta3 = new PolylineOptions()
                .add(new LatLng(24.009840, -104.665658))
                .add(new LatLng(24.009872, -104.665641))
                .add(new LatLng(24.009934, -104.665616))//blvdDurango
                .add(new LatLng(24.010348, -104.666688))
                .add(new LatLng(24.010546, -104.667152))
                .add(new LatLng(24.010948, -104.668145))
                .add(new LatLng(24.011149, -104.668602))
                .add(new LatLng(24.011455, -104.669411))
                .add(new LatLng(24.011716, -104.670053))
                .add(new LatLng(24.012087, -104.670965))//lerdo
                .add(new LatLng(24.012326, -104.671593))//urrea
                .add(new LatLng(24.012897, -104.672988))//luna
                .add(new LatLng(24.013164, -104.673689))//milagro
                .add(new LatLng(24.013430, -104.674396))
                .add(new LatLng(24.013756, -104.675416))//belisario
                .add(new LatLng(24.014220, -104.676700))//durangovuelta
                .add(new LatLng(24.014240, -104.676900))
                .add(new LatLng(24.014233, -104.676999))//primo de vdd
                .add(new LatLng(24.014157, -104.677495))
                .add(new LatLng(24.014126, -104.677731))//cerrodelacruz
                .add(new LatLng(24.013996, -104.678447))
                .add(new LatLng(24.013795, -104.679591))//RioBravo
                .add(new LatLng(24.013733, -104.679983))
                .add(new LatLng(24.013517, -104.681182))
                .add(new LatLng(24.013471, -104.681355))
                .add(new LatLng(24.013132, -104.682344))//riopapaloapa
                .add(new LatLng(24.012958, -104.682811))
                .add(new LatLng(24.012682, -104.683600))
                .add(new LatLng(24.012414, -104.684356))//gonzalevega
                .add(new LatLng(24.012500, -104.685149))
                .add(new LatLng(24.012618, -104.686324))
                .add(new LatLng(24.0126374512844, -104.6865762770176))//checador

                .width(30f)
                .color(Color.argb(80, 37, 10, 205));


        RUTAS.add(new Ruta("Ruta 3", inicioBlancas3,finBlancas3, baseBlancas,
                rutaInicio3, rutaVuelta3, paradasRuta3,"6"));

        /*******************************************************************************************
         ******************************** RUTAS AZUL/AMARILLO *************************************/


        /********************************* RUTA AZTECA - CENTRO ***********************************/
        List<LatLng> paradasRutaAztecaCentro = new ArrayList<>();
        paradasRutaAztecaCentro.add(new LatLng(23.986923, -104.664155));//bomberos
        paradasRutaAztecaCentro.add(new LatLng(23.988770, -104.663783));//hilarioPerez
        paradasRutaAztecaCentro.add(new LatLng(23.994963, -104.662696));//sep
        paradasRutaAztecaCentro.add(new LatLng(23.998878, -104.662123));//Acuario
        paradasRutaAztecaCentro.add(new LatLng(24.002909, -104.662185));//alsuper
        paradasRutaAztecaCentro.add(new LatLng(24.007946, -104.662306));//pollositalia
        paradasRutaAztecaCentro.add(new LatLng(24.011449, -104.662621));//chedraui
        paradasRutaAztecaCentro.add(new LatLng(24.015004, -104.663232));//NicolasLuna
        paradasRutaAztecaCentro.add(new LatLng(24.019996, -104.664027));//iv centenario Calle
        paradasRutaAztecaCentro.add(new LatLng(24.023020, -104.663945));//PinoSuarez
        paradasRutaAztecaCentro.add(new LatLng(24.024237, -104.664536));//5Febrero
        paradasRutaAztecaCentro.add(new LatLng(24.023974, -104.666311));//Patoni
        paradasRutaAztecaCentro.add(new LatLng(24.023550, -104.668909));//VictoriaSur
        paradasRutaAztecaCentro.add(new LatLng(24.021402, -104.668848));//Plazuela
        paradasRutaAztecaCentro.add(new LatLng(24.019675, -104.665528));//Base
        paradasRutaAztecaCentro.add(new LatLng(24.014914, -104.663460));//PostCantaranas
        paradasRutaAztecaCentro.add(new LatLng(24.011455, -104.662874));//PolloFeliz
        paradasRutaAztecaCentro.add(new LatLng(24.006889, -104.662562));//Soriana
        paradasRutaAztecaCentro.add(new LatLng(24.002966, -104.662485));//GralBalderas
        paradasRutaAztecaCentro.add(new LatLng(23.998352, -104.662509));//SEDESOE
        paradasRutaAztecaCentro.add(new LatLng(23.994987, -104.663028));//SEP
        paradasRutaAztecaCentro.add(new LatLng(23.991972, -104.663648));//PAtriaLibre

        MarkerOptions baseAzteca = new MarkerOptions()
                .position(new LatLng(24.019396, -104.665224))
                .title("Base")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.base));
        MarkerOptions inicioAzteca = new MarkerOptions()
                .position(new LatLng(23.979829, -104.641976))
                .title("Inicio");
        MarkerOptions finAzteca =  new MarkerOptions()
                .position(new LatLng(24.024232, -104.664589))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        PolylineOptions rutaInicioAzteca = new PolylineOptions()
                .add(new LatLng(23.979829, -104.641976))
                .add(new LatLng(23.979952, -104.642391))
                .add(new LatLng(23.980245, -104.643418))
                .add(new LatLng(23.980631, -104.644798))//CanalDeLaFerreria
                .add(new LatLng(23.980682, -104.645002))
                .add(new LatLng(23.981061, -104.646344))//palma
                .add(new LatLng(23.981299, -104.647056))
                .add(new LatLng(23.981373, -104.647337))
                .add(new LatLng(23.981439, -104.647648))//Nogales
                .add(new LatLng(23.981653, -104.648407))
                .add(new LatLng(23.981991, -104.649582))
                .add(new LatLng(23.982070, -104.649834))
                .add(new LatLng(23.982506, -104.651453))//lailusion
                .add(new LatLng(23.982830, -104.652571))
                .add(new LatLng(23.982939, -104.652893))
                .add(new LatLng(23.983209, -104.653922))//leyesDeReforma
                .add(new LatLng(23.983730, -104.655782))
                .add(new LatLng(23.984047, -104.656853))//sct
                .add(new LatLng(23.984440, -104.658287))
                .add(new LatLng(23.985003, -104.660121))//DerechosHumanos
                .add(new LatLng(23.985260, -104.661028))//tioCactus
                .add(new LatLng(23.985523, -104.662004))
                .add(new LatLng(23.985879, -104.663200))
                .add(new LatLng(23.986063, -104.663849))//SantoDomingo
                .add(new LatLng(23.986201, -104.664076))
                .add(new LatLng(23.986340, -104.664309))//ManuelEstebane
                .add(new LatLng(23.986699, -104.664229))
                .add(new LatLng(23.986997, -104.664179))
                .add(new LatLng(23.987401, -104.664115))//SimonAlvarez
                .add(new LatLng(23.988150, -104.663964))
                .add(new LatLng(23.988683, -104.663852))//HilarioPerezDeLeon
                .add(new LatLng(23.989667, -104.663669))
                .add(new LatLng(23.990940, -104.663437))//IsabelAlmanza
                .add(new LatLng(23.991781, -104.663288))
                .add(new LatLng(23.992610, -104.663139))
                .add(new LatLng(23.993458, -104.662990))//otilia
                .add(new LatLng(23.994341, -104.662835))
                .add(new LatLng(23.995198, -104.662658))
                .add(new LatLng(23.995698, -104.662567))//hilariomoreno
                .add(new LatLng(23.996870, -104.662375))
                .add(new LatLng(23.996966, -104.662394))
                .add(new LatLng(23.997039, -104.662475))//DomingoArrieta
                .add(new LatLng(23.997855, -104.662321))//los naranjos
                .add(new LatLng(23.998459, -104.662201))
                .add(new LatLng(23.998998, -104.662171))
                .add(new LatLng(23.999504, -104.662186))//escorpion
                .add(new LatLng(24.000472, -104.662191))
                .add(new LatLng(24.001464, -104.662212))
                .add(new LatLng(24.002142, -104.662225))
                .add(new LatLng(24.002813, -104.662234))//alsuper
                .add(new LatLng(24.003738, -104.662243))
                .add(new LatLng(24.004633, -104.662268))
                .add(new LatLng(24.005797, -104.662310))
                .add(new LatLng(24.006552, -104.662325))
                .add(new LatLng(24.006987, -104.662340))
                .add(new LatLng(24.007576, -104.662321))//banorte
                .add(new LatLng(24.008157, -104.662321))//calleentreSantander
                .add(new LatLng(24.008559, -104.662316))
                .add(new LatLng(24.008931, -104.662396))
                .add(new LatLng(24.009057, -104.662417))//blvdDurango
                .add(new LatLng(24.009750, -104.662495))
                .add(new LatLng(24.010383, -104.662557))//homedepot
                .add(new LatLng(24.011114, -104.662616))
                .add(new LatLng(24.011935, -104.662789))//cedrauiCinepolis
                .add(new LatLng(24.012725, -104.662943))
                .add(new LatLng(24.013286, -104.663050))
                .add(new LatLng(24.013767, -104.663110))
                .add(new LatLng(24.014781, -104.663252))
                .add(new LatLng(24.015167, -104.663309))
                .add(new LatLng(24.015367, -104.663351))//sanMartin
                .add(new LatLng(24.015961, -104.663494))
                .add(new LatLng(24.016700, -104.663619))
                .add(new LatLng(24.016912, -104.663627))
                .add(new LatLng(24.017458, -104.663716))
                .add(new LatLng(24.017932, -104.663729))//villasalpinas
                .add(new LatLng(24.018413, -104.663803))
                .add(new LatLng(24.018812, -104.663926))//6Julio
                .add(new LatLng(24.019195, -104.664031))
                .add(new LatLng(24.019516, -104.664068))//doloresdelrio
                .add(new LatLng(24.019768, -104.664065))//carlosleonpena
                .add(new LatLng(24.020172, -104.664057))
                .add(new LatLng(24.020906, -104.664031))
                .add(new LatLng(24.021451, -104.663997))//stamaria
                .add(new LatLng(24.022019, -104.664012))
                .add(new LatLng(24.022074, -104.664008))
                .add(new LatLng(24.022634, -104.663993))
                .add(new LatLng(24.023118, -104.663979))//pinoSuarez
                .add(new LatLng(24.023762, -104.664062))
                .add(new LatLng(24.024291, -104.664132))//5FEBRERO
                .add(new LatLng(24.024217, -104.664574))//Fin

                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVueltaAzteca = new PolylineOptions()
                .add(new LatLng(24.024217, -104.664574))
                .add(new LatLng(24.024160, -104.664878))//Francisco Zarco
                .add(new LatLng(24.024035, -104.665663))
                .add(new LatLng(24.023927, -104.666400))
                .add(new LatLng(24.023792, -104.667356))//pasteur
                .add(new LatLng(24.023609, -104.668428))
                .add(new LatLng(24.023498, -104.669078))//VictoriaSur
                .add(new LatLng(24.022296, -104.668923))//PinoSurez
                .add(new LatLng(24.022296, -104.668923))
                .add(new LatLng(24.021352, -104.668817))//plazuela
                .add(new LatLng(24.021424, -104.668153))
                .add(new LatLng(24.020955, -104.668065))
                .add(new LatLng(24.020166, -104.667958))//plazuelaSalida
                .add(new LatLng(24.019943, -104.667868))//sarabia
                .add(new LatLng(24.019947, -104.667338))
                .add(new LatLng(24.019881, -104.666971))
                .add(new LatLng(24.019796, -104.666255))
                .add(new LatLng(24.019651, -104.665203))//base
                .add(new LatLng(24.019486, -104.664915))
                .add(new LatLng(24.019436, -104.664587))
                .add(new LatLng(24.019393, -104.664513))
                .add(new LatLng(24.019284, -104.664457))//barrazaDomArrieta
                .add(new LatLng(24.018546, -104.664153))
                .add(new LatLng(24.018208, -104.664025))
                .add(new LatLng(24.017703, -104.664059))
                .add(new LatLng(24.017604, -104.664048))
                .add(new LatLng(24.017205, -104.664070))
                .add(new LatLng(24.017061, -104.664072))
                .add(new LatLng(24.016934, -104.664063))
                .add(new LatLng(24.016796, -104.664040))
                .add(new LatLng(24.016484, -104.663949))
                .add(new LatLng(24.015912, -104.663737))
                .add(new LatLng(24.015139, -104.663439))
                .add(new LatLng(24.014594, -104.663366))
                .add(new LatLng(24.013946, -104.663274))
                .add(new LatLng(24.013284, -104.663162))//DomingoArrieta
                .add(new LatLng(24.012730, -104.663055))
                .add(new LatLng(24.012455, -104.663025))
                .add(new LatLng(24.011911, -104.662938))//JuanEscutia
                .add(new LatLng(24.011401, -104.662841))
                .add(new LatLng(24.011033, -104.662775))
                .add(new LatLng(24.010732, -104.662723))
                .add(new LatLng(24.010037, -104.662661))//lerdo
                .add(new LatLng(24.009027, -104.662544))
                .add(new LatLng(24.009027, -104.662544))//BlvdDurango
                .add(new LatLng(24.008503, -104.662459))
                .add(new LatLng(24.008156, -104.662431))
                .add(new LatLng(24.007734, -104.662451))
                .add(new LatLng(24.007642, -104.662445))
                .add(new LatLng(24.007283, -104.662489))
                .add(new LatLng(24.007020, -104.662510))
                .add(new LatLng(24.006441, -104.662514))//gralValencia
                .add(new LatLng(24.005551, -104.662503))
                .add(new LatLng(24.004619, -104.662471))
                .add(new LatLng(24.003730, -104.662460))//GralTornel
                .add(new LatLng(24.002800, -104.662458))
                .add(new LatLng(24.001951, -104.662438))
                .add(new LatLng(24.001599, -104.662426))
                .add(new LatLng(24.000471, -104.662411))
                .add(new LatLng(23.999703, -104.662402))
                .add(new LatLng(23.999062, -104.662429))
                .add(new LatLng(23.998476, -104.662441))//IsmaelLares
                .add(new LatLng(23.997823, -104.662494))
                .add(new LatLng(23.997075, -104.662624))
                .add(new LatLng(23.996065, -104.662793))
                .add(new LatLng(23.995713, -104.662860))
                .add(new LatLng(23.994965, -104.662984))//sep
                .add(new LatLng(23.994442, -104.663070))
                .add(new LatLng(23.993691, -104.663200))//MarianoArrieta
                .add(new LatLng(23.993496, -104.663234))
                .add(new LatLng(23.992610, -104.663443))
                .add(new LatLng(23.991863, -104.663622))//patriaLibre
                .add(new LatLng(23.991248, -104.663715))
                .add(new LatLng(23.991193, -104.663791))
                .add(new LatLng(23.991099, -104.663814))//costadoDArrieta
                .add(new LatLng(23.990576, -104.663914))
                .add(new LatLng(23.989882, -104.664031))
                .add(new LatLng(23.989392, -104.664123))
                .add(new LatLng(23.988717, -104.664244))//HilarioPerezLeon
                .add(new LatLng(23.987662, -104.664421))
                .add(new LatLng(23.986937, -104.664544))
                .add(new LatLng(23.986403, -104.664633))//pastor
                .add(new LatLng(23.986353, -104.664418))
                .add(new LatLng(23.986326, -104.664314))
                .add(new LatLng(23.986192, -104.664070))
                .add(new LatLng(23.986052, -104.663859))
                .add(new LatLng(23.985875, -104.663232))
                .add(new LatLng(23.985812, -104.662992))
                .add(new LatLng(23.985514, -104.662009))//solidaridad
                .add(new LatLng(23.985226, -104.660983))
                .add(new LatLng(23.984992, -104.660127))//derechoisHumanos
                .add(new LatLng(23.984657, -104.659077))
                .add(new LatLng(23.984427, -104.658291))
                .add(new LatLng(23.983992, -104.656710))//sct
                .add(new LatLng(23.983703, -104.655739))
                .add(new LatLng(23.983605, -104.655349))
                .add(new LatLng(23.983409, -104.654686))
                .add(new LatLng(23.983191, -104.653916))//leyesdeReforma
                .add(new LatLng(23.983040, -104.653325))
                .add(new LatLng(23.982922, -104.652896))
                .add(new LatLng(23.982793, -104.652467))//chatarraLasFlores
                .add(new LatLng(23.982496, -104.651448))
                .add(new LatLng(23.982133, -104.650130))
                .add(new LatLng(23.982057, -104.649836))
                .add(new LatLng(23.981982, -104.649593))
                .add(new LatLng(23.981757, -104.648843))
                .add(new LatLng(23.981538, -104.648054))
                .add(new LatLng(23.981428, -104.647648))
                .add(new LatLng(23.981354, -104.647333))
                .add(new LatLng(23.981290, -104.647075))
                .add(new LatLng(23.981048, -104.646354))//Palmas
                .add(new LatLng(23.980825, -104.645539))//Sauces
                .add(new LatLng(23.980657, -104.644970))
                .add(new LatLng(23.980628, -104.644835))
                .add(new LatLng(23.980281, -104.643605))
                .add(new LatLng(23.979942, -104.642406))
                .add(new LatLng(23.979811, -104.641960))//cbtaFin

                .width(30f)
                .color(Color.argb(80, 37, 10, 205));


        RUTAS.add(new Ruta("Ruta 5 de Febrero - Azteca", inicioAzteca,finAzteca, baseAzteca,
                rutaInicioAzteca, rutaVueltaAzteca, paradasRutaAztecaCentro,"7"));

        /******************************* RUTA AZTECA - PLAZUELA ***********************************/
        List<LatLng> paradasPlazuelaAzteca = new ArrayList<>();
        paradasPlazuelaAzteca.add(new LatLng(23.986875352372792,-104.66417279094458));
        paradasPlazuelaAzteca.add(new LatLng(23.988727969738797,-104.66380130499601));
        paradasPlazuelaAzteca.add(new LatLng(23.99486212394457,-104.66267678886652));
        paradasPlazuelaAzteca.add(new LatLng(23.998890853863056,-104.66212291270496));
        paradasPlazuelaAzteca.add(new LatLng(24.003447794816875,-104.66217320412399));
        paradasPlazuelaAzteca.add(new LatLng(24.011536442241955,-104.66263521462679));
        paradasPlazuelaAzteca.add(new LatLng(24.01516866442385,-104.66327391564846));
        paradasPlazuelaAzteca.add(new LatLng(24.020267384004097,-104.6686064824462));
        paradasPlazuelaAzteca.add(new LatLng(24.020447760253834,-104.67026978731157));
        paradasPlazuelaAzteca.add(new LatLng(24.02431062992228,-104.67374861240387));
        paradasPlazuelaAzteca.add(new LatLng(24.025001181438657,-104.67824265360834));
        paradasPlazuelaAzteca.add(new LatLng(24.028320370412544,-104.67957437038422));
        paradasPlazuelaAzteca.add(new LatLng(24.02840060069306,-104.68142542988062));
        paradasPlazuelaAzteca.add(new LatLng(24.030149730848105,-104.68175500631332));
        paradasPlazuelaAzteca.add(new LatLng(24.030407260080523,-104.68053460121155));
        paradasPlazuelaAzteca.add(new LatLng(24.028475625146694,-104.68020971864462));
        paradasPlazuelaAzteca.add(new LatLng(24.024951572096427,-104.67862151563166));
        paradasPlazuelaAzteca.add(new LatLng(24.022351648291878,-104.67739306390285));
        paradasPlazuelaAzteca.add(new LatLng(24.021253478578178,-104.6693105623126));
        paradasPlazuelaAzteca.add(new LatLng(24.019647855500953,-104.66556821018457));
        paradasPlazuelaAzteca.add(new LatLng(24.01495336777354,-104.66344725340605));
        paradasPlazuelaAzteca.add(new LatLng(24.01137718563359,-104.6628725901246));
        paradasPlazuelaAzteca.add(new LatLng(24.006830020014498,-104.66255810111761));
        paradasPlazuelaAzteca.add(new LatLng(24.002924970762813,-104.66253530234098));
        paradasPlazuelaAzteca.add(new LatLng(23.998228034920988,-104.66251384466887));
        paradasPlazuelaAzteca.add(new LatLng(23.995059688718495,-104.66309320181608));


        MarkerOptions inicioPlazuelaAzteca = new MarkerOptions()
                .position(new LatLng(23.982506, -104.651453))
                .title("Inicio");
        MarkerOptions finPlazuelaAzteca =  new MarkerOptions()
                .position(new LatLng(24.028384064685145,-104.68141537159681))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        PolylineOptions rutaInicioPlazuelaAzteca = new PolylineOptions()
                .add(new LatLng(23.982506, -104.651453))//lailusion
                .add(new LatLng(23.982830, -104.652571))
                .add(new LatLng(23.982939, -104.652893))
                .add(new LatLng(23.983209, -104.653922))//leyesDeReforma
                .add(new LatLng(23.983730, -104.655782))
                .add(new LatLng(23.984047, -104.656853))//sct
                .add(new LatLng(23.984440, -104.658287))
                .add(new LatLng(23.985003, -104.660121))//DerechosHumanos
                .add(new LatLng(23.985260, -104.661028))//tioCactus
                .add(new LatLng(23.985523, -104.662004))
                .add(new LatLng(23.985879, -104.663200))
                .add(new LatLng(23.986063, -104.663849))//SantoDomingo
                .add(new LatLng(23.986201, -104.664076))
                .add(new LatLng(23.986340, -104.664309))//ManuelEstebane
                .add(new LatLng(23.986699, -104.664229))
                .add(new LatLng(23.986997, -104.664179))
                .add(new LatLng(23.987401, -104.664115))//SimonAlvarez
                .add(new LatLng(23.988150, -104.663964))
                .add(new LatLng(23.988683, -104.663852))//HilarioPerezDeLeon
                .add(new LatLng(23.989667, -104.663669))
                .add(new LatLng(23.990940, -104.663437))//IsabelAlmanza
                .add(new LatLng(23.991781, -104.663288))
                .add(new LatLng(23.992610, -104.663139))
                .add(new LatLng(23.993458, -104.662990))//otilia
                .add(new LatLng(23.994341, -104.662835))
                .add(new LatLng(23.995198, -104.662658))
                .add(new LatLng(23.995698, -104.662567))//hilariomoreno
                .add(new LatLng(23.996870, -104.662375))
                .add(new LatLng(23.996966, -104.662394))
                .add(new LatLng(23.997039, -104.662475))//DomingoArrieta
                .add(new LatLng(23.997855, -104.662321))//los naranjos
                .add(new LatLng(23.998459, -104.662201))
                .add(new LatLng(23.998998, -104.662171))
                .add(new LatLng(23.999504, -104.662186))//escorpion
                .add(new LatLng(24.000472, -104.662191))
                .add(new LatLng(24.001464, -104.662212))
                .add(new LatLng(24.002142, -104.662225))
                .add(new LatLng(24.002813, -104.662234))//alsuper
                .add(new LatLng(24.003738, -104.662243))
                .add(new LatLng(24.004633, -104.662268))
                .add(new LatLng(24.005797, -104.662310))
                .add(new LatLng(24.006552, -104.662325))
                .add(new LatLng(24.006987, -104.662340))
                .add(new LatLng(24.007576, -104.662321))//banorte
                .add(new LatLng(24.008157, -104.662321))//calleentreSantander
                .add(new LatLng(24.008559, -104.662316))
                .add(new LatLng(24.008931, -104.662396))
                .add(new LatLng(24.009057, -104.662417))//blvdDurango
                .add(new LatLng(24.009750, -104.662495))
                .add(new LatLng(24.010383, -104.662557))//homedepot
                .add(new LatLng(24.011114, -104.662616))
                .add(new LatLng(24.011935, -104.662789))//cedrauiCinepolis
                .add(new LatLng(24.012725, -104.662943))
                .add(new LatLng(24.013286, -104.663050))
                .add(new LatLng(24.013767, -104.663110))
                .add(new LatLng(24.014781, -104.663252))
                .add(new LatLng(24.015167, -104.663309))
                .add(new LatLng(24.015367, -104.663351))//sanMartin
                .add(new LatLng(24.015961, -104.663494))
                .add(new LatLng(24.016700, -104.663619))
                .add(new LatLng(24.016912, -104.663627))
                .add(new LatLng(24.017458, -104.663716))

                .add(new LatLng(24.017612547077277,-104.6637312322855))
                .add(new LatLng(24.01770993391993,-104.66377682983875))
                .add(new LatLng(24.017990456614193,-104.66385494917631))
                .add(new LatLng(24.018674610924343,-104.66409098356962))
                .add(new LatLng(24.01937009261753,-104.6643964201212))
                .add(new LatLng(24.019584769386054,-104.66459926217794))
                .add(new LatLng(24.019658574012546,-104.66467168182135))
                .add(new LatLng(24.019710329098807,-104.66476321220398))
                .add(new LatLng(24.019758409249025,-104.66516889631748))
                .add(new LatLng(24.019851507068392,-104.66573383659126))
                .add(new LatLng(24.01992990517979,-104.6659517660737))
                .add(new LatLng(24.01999819724567,-104.66649156063794))
                .add(new LatLng(24.020058833219952,-104.66696597635746))
                .add(new LatLng(24.02011028190298,-104.66733913868666))
                .add(new LatLng(24.0201687741307,-104.66796208173037))
                .add(new LatLng(24.020195110936594,-104.66818772256374))
                .add(new LatLng(24.020290352131457,-104.66913320124148))
                .add(new LatLng(24.020309032872007,-104.66940946877003))
                .add(new LatLng(24.02041621739651,-104.67023491859435))
                .add(new LatLng(24.020497983816515,-104.67091351747514))
                .add(new LatLng(24.02057209415978,-104.67160284519196))
                .add(new LatLng(24.020607924351747,-104.67192605137825))
                .add(new LatLng(24.02068509703896,-104.67258788645269))
                .add(new LatLng(24.02078462523782,-104.67319842427968))
                .add(new LatLng(24.020953057398817,-104.67315852642058))
                .add(new LatLng(24.02115272578382,-104.67312265187502))
                .add(new LatLng(24.021569823464883,-104.67317126691341))
                .add(new LatLng(24.02163382732114,-104.67316154390574))
                .add(new LatLng(24.022262533363673,-104.67323563992977))
                .add(new LatLng(24.022810390140613,-104.6733083948493))
                .add(new LatLng(24.02363997988756,-104.67339858412743))
                .add(new LatLng(24.024157820016683,-104.67346664518118))
                .add(new LatLng(24.024259489152442,-104.6734693273902))//20deNov
                .add(new LatLng(24.02425979538466,-104.6737378835678))
                .add(new LatLng(24.02425979538466,-104.67417072504757))
                .add(new LatLng(24.024244790005252,-104.67446107417345))
                .add(new LatLng(24.024223047513633,-104.67489391565323))
                .add(new LatLng(24.02423100955325,-104.67544108629227))
                .add(new LatLng(24.024310323690177,-104.67580620199442))
                .add(new LatLng(24.02440984908233,-104.67622730880976))
                .add(new LatLng(24.024488550645568,-104.67630442231894))
                .add(new LatLng(24.0246738206723,-104.67647105455399))
                .add(new LatLng(24.024924930034352,-104.67671513557434))
                .add(new LatLng(24.024990769602983,-104.67677816748619))
                .add(new LatLng(24.025009143430083,-104.67681873589754))
                .add(new LatLng(24.024944528793196,-104.67742290347815))
                .add(new LatLng(24.024898287966757,-104.67793822288513))
                .add(new LatLng(24.02486215273984,-104.67826712876557))
                .add(new LatLng(24.025276176087292,-104.67836268246174))
                .add(new LatLng(24.02553585868975,-104.67842672020197))
                .add(new LatLng(24.026205886377614,-104.67874858528376))
                .add(new LatLng(24.02670319900275,-104.67898361384869))
                .add(new LatLng(24.02706607676675,-104.6792059019208))
                .add(new LatLng(24.02727767843303,-104.67929877340794))
                .add(new LatLng(24.02756399851222,-104.67944394797087))
                .add(new LatLng(24.027901763518184,-104.67960253357887))
                .add(new LatLng(24.02794218501881,-104.67958711087702))
                .add(new LatLng(24.02820614935424,-104.67961493879557))
                .add(new LatLng(24.02830904017805,-104.67962097376585))
                .add(new LatLng(24.02844071581454,-104.67959649860859))
                .add(new LatLng(24.02854666867059,-104.67982515692711))
                .add(new LatLng(24.02863914768166,-104.68000888824464))
                .add(new LatLng(24.028692736482114,-104.68015976250173))
                .add(new LatLng(24.028708353785476,-104.6802506223321))
                .add(new LatLng(24.028736526171038,-104.6804953739047))
                .add(new LatLng(24.02867742539854,-104.68087188899517))
                .add(new LatLng(24.028621386817154,-104.68107271939516))
                .add(new LatLng(24.028570247762513,-104.68116626143456))
                .add(new LatLng(24.028387739353754,-104.68140531331301))//finCanoas

                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVueltaPlazuelaAzteca = new PolylineOptions()
                .add(new LatLng(24.028378552682028,-104.68142207711935))
                .add(new LatLng(24.02827841791766,-104.68154042959215))
                .add(new LatLng(24.028573922425803,-104.68173187226057))
                .add(new LatLng(24.028788583823786,-104.68189481645823))
                .add(new LatLng(24.02884676588131,-104.68194644898176))
                .add(new LatLng(24.02920075720044,-104.68230150640011))
                .add(new LatLng(24.029396125694287,-104.6825559809804))
                .add(new LatLng(24.02972133089842,-104.68221768736838))
                .add(new LatLng(24.029993559480758,-104.68195784837009))
                .add(new LatLng(24.030255988513083,-104.68170404434206))
                .add(new LatLng(24.0303294807948,-104.68163564801216))
                .add(new LatLng(24.0303653082669,-104.68140497803688))
                .add(new LatLng(24.030417671477394,-104.68066267669201))
                .add(new LatLng(24.0304678911446,-104.68028884381056))
                .add(new LatLng(24.030087874760593,-104.68027342110872))
                .add(new LatLng(24.029710613225898,-104.68025598675011))
                .add(new LatLng(24.02926812568014,-104.68025028705597))
                .add(new LatLng(24.028710497336768,-104.68024358153343))
                .add(new LatLng(24.028579434420536,-104.6802331879735))
                .add(new LatLng(24.028378858904432,-104.68015540391205))
                .add(new LatLng(24.02806375566755,-104.67995624989273))
                .add(new LatLng(24.027914931129626,-104.67988450080158))
                .add(new LatLng(24.027546237499127,-104.67971250414848))
                .add(new LatLng(24.027130384111725,-104.67951435595751))
                .add(new LatLng(24.02690990165212,-104.6793946623802))
                .add(new LatLng(24.026644709748275,-104.67924378812312))
                .add(new LatLng(24.026062265563105,-104.67894874513148))
                .add(new LatLng(24.025746237960607,-104.67881932854652))
                .add(new LatLng(24.02547124431765,-104.67871405184269))
                .add(new LatLng(24.02519043171654,-104.67866208404303))
                .add(new LatLng(24.024948203560136,-104.67860072851182))
                .add(new LatLng(24.0248309171965,-104.67852462083103))
                .add(new LatLng(24.024702300173395,-104.67845287173986))
                .add(new LatLng(24.024375244877806,-104.67829663306476))
                .add(new LatLng(24.024084936671073,-104.67814911156893))
                .add(new LatLng(24.023753286303442,-104.6779865026474))
                .add(new LatLng(24.023612112618622,-104.67791274189949))
                .add(new LatLng(24.023462670457945,-104.67784065753222))
                .add(new LatLng(24.023128568605607,-104.67768542468549))
                .add(new LatLng(24.022730156370635,-104.67750605195761))
                .add(new LatLng(24.022577650822473,-104.67744402587414))
                .add(new LatLng(24.022440150685203,-104.67739306390285))
                .add(new LatLng(24.0223929902479,-104.67737797647715))
                .add(new LatLng(24.02234674850362,-104.6773699298501))
                .add(new LatLng(24.022288563503732,-104.67737060040236))
                .add(new LatLng(24.02224140301082,-104.67738769948483))
                .add(new LatLng(24.022278763922206,-104.67745710164309))
                .add(new LatLng(24.022274782842004,-104.67752549797298))
                .add(new LatLng(24.022244159144,-104.6775707602501))
                .add(new LatLng(24.02220863564516,-104.67759389430285))
                .add(new LatLng(24.022158106513537,-104.67759791761637))
                .add(new LatLng(24.022118295668573,-104.6775908768177))
                .add(new LatLng(24.022080322235723,-104.6775583550334))
                .add(new LatLng(24.022062560464896,-104.67750236392021))
                .add(new LatLng(24.022068991451174,-104.67743329703808))
                .add(new LatLng(24.022099615190914,-104.67738434672354))
                .add(new LatLng(24.02216361878341,-104.67735953629017))
                .add(new LatLng(24.02219485497487,-104.67712517827749))
                .add(new LatLng(24.021898110849513,-104.67690356075764))
                .add(new LatLng(24.0216154530116,-104.67665042728186))
                .add(new LatLng(24.02133861309665,-104.67637550085783))
                .add(new LatLng(24.02106605994315,-104.67610158026218))
                .add(new LatLng(24.02094172651491,-104.67594131827356))
                .add(new LatLng(24.020857204214252,-104.6756761148572))
                .add(new LatLng(24.02082933634207,-104.6754078939557))
                .add(new LatLng(24.020818005447257,-104.67505350708963))
                .add(new LatLng(24.02079075004754,-104.6744865551591))
                .add(new LatLng(24.020742057802195,-104.67409394681452))
                .add(new LatLng(24.020689078168402,-104.67370066791773))
                .add(new LatLng(24.020629361213942,-104.67324033379555))
                .add(new LatLng(24.020563825652523,-104.6728041395545))
                .add(new LatLng(24.02045143514933,-104.67206854373218))
                .add(new LatLng(24.020395086740166,-104.6716809645295))
                .add(new LatLng(24.020327713609845,-104.67115726321936))
                .add(new LatLng(24.020264015345077,-104.6707385033369))
                .add(new LatLng(24.020201542016277,-104.67031035572289))
                .add(new LatLng(24.02013569999474,-104.66992881149054))
                .add(new LatLng(24.020056689524385,-104.66938734054565))
                .add(new LatLng(24.020316382670828,-104.66940946877003))
                .add(new LatLng(24.02058311883529,-104.66944232583046))
                .add(new LatLng(24.02068203463163,-104.66944769024848))
                .add(new LatLng(24.0209065088964,-104.66949865221979))
                .add(new LatLng(24.02124888498745,-104.66954525560142))
                .add(new LatLng(24.021304620543876,-104.66916672885418))
                .add(new LatLng(24.0213514751413,-104.66881904751062))
                .add(new LatLng(24.021424053797723,-104.66816056519747))
                .add(new LatLng(24.020948770037457,-104.66806668788195))
                .add(new LatLng(24.02017551145365,-104.66796308755876))
                .add(new LatLng(24.019940623667864,-104.66787490993738))
                .add(new LatLng(24.019953179609903,-104.66734617948534))
                .add(new LatLng(24.019890706130063,-104.66696631163359))
                .add(new LatLng(24.01978198995307,-104.6661214157939))
                .add(new LatLng(24.019697466889962,-104.66559670865536))
                .add(new LatLng(24.019629787150826,-104.66513972729443))
                .add(new LatLng(24.01948799645061,-104.66494627296925))
                .add(new LatLng(24.0194233790386,-104.66456439346075))
                .add(new LatLng(24.019378361201568,-104.66450471431017))
                .add(new LatLng(24.019258313559064,-104.66445039957762))//Barraza

                .add(new LatLng(24.018546, -104.664153))
                .add(new LatLng(24.018208, -104.664025))
                .add(new LatLng(24.017703, -104.664059))
                .add(new LatLng(24.017604, -104.664048))
                .add(new LatLng(24.017205, -104.664070))
                .add(new LatLng(24.017061, -104.664072))
                .add(new LatLng(24.016934, -104.664063))
                .add(new LatLng(24.016796, -104.664040))
                .add(new LatLng(24.016484, -104.663949))
                .add(new LatLng(24.015912, -104.663737))
                .add(new LatLng(24.015139, -104.663439))
                .add(new LatLng(24.014594, -104.663366))
                .add(new LatLng(24.013946, -104.663274))
                .add(new LatLng(24.013284, -104.663162))//DomingoArrieta
                .add(new LatLng(24.012730, -104.663055))
                .add(new LatLng(24.012455, -104.663025))
                .add(new LatLng(24.011911, -104.662938))//JuanEscutia
                .add(new LatLng(24.011401, -104.662841))
                .add(new LatLng(24.011033, -104.662775))
                .add(new LatLng(24.010732, -104.662723))
                .add(new LatLng(24.010037, -104.662661))//lerdo
                .add(new LatLng(24.009027, -104.662544))
                .add(new LatLng(24.009027, -104.662544))//BlvdDurango
                .add(new LatLng(24.008503, -104.662459))
                .add(new LatLng(24.008156, -104.662431))
                .add(new LatLng(24.007734, -104.662451))
                .add(new LatLng(24.007642, -104.662445))
                .add(new LatLng(24.007283, -104.662489))
                .add(new LatLng(24.007020, -104.662510))
                .add(new LatLng(24.006441, -104.662514))//gralValencia
                .add(new LatLng(24.005551, -104.662503))
                .add(new LatLng(24.004619, -104.662471))
                .add(new LatLng(24.003730, -104.662460))//GralTornel
                .add(new LatLng(24.002800, -104.662458))
                .add(new LatLng(24.001951, -104.662438))
                .add(new LatLng(24.001599, -104.662426))
                .add(new LatLng(24.000471, -104.662411))
                .add(new LatLng(23.999703, -104.662402))
                .add(new LatLng(23.999062, -104.662429))
                .add(new LatLng(23.998476, -104.662441))//IsmaelLares
                .add(new LatLng(23.997823, -104.662494))
                .add(new LatLng(23.997075, -104.662624))
                .add(new LatLng(23.996065, -104.662793))
                .add(new LatLng(23.995713, -104.662860))
                .add(new LatLng(23.994965, -104.662984))//sep
                .add(new LatLng(23.994442, -104.663070))
                .add(new LatLng(23.993691, -104.663200))//MarianoArrieta
                .add(new LatLng(23.993496, -104.663234))
                .add(new LatLng(23.992610, -104.663443))
                .add(new LatLng(23.991863, -104.663622))//patriaLibre
                .add(new LatLng(23.991248, -104.663715))
                .add(new LatLng(23.991193, -104.663791))
                .add(new LatLng(23.991099, -104.663814))//costadoDArrieta
                .add(new LatLng(23.990576, -104.663914))
                .add(new LatLng(23.989882, -104.664031))
                .add(new LatLng(23.989392, -104.664123))
                .add(new LatLng(23.988717, -104.664244))//HilarioPerezLeon
                .add(new LatLng(23.987662, -104.664421))
                .add(new LatLng(23.986937, -104.664544))
                .add(new LatLng(23.986403, -104.664633))//pastor
                .add(new LatLng(23.986326729912413,-104.66431595385075))
                .add(new LatLng(23.986061760662036,-104.66386534273626))
                .add(new LatLng(23.98646763819342,-104.66346736997365))
                .add(new LatLng(23.986844413926605,-104.66307308524848))
                .add(new LatLng(23.987198520846498,-104.6627277508378))
                .add(new LatLng(23.987588772500498,-104.66232609003782))
                .add(new LatLng(23.988114415715994,-104.66177523136137))
                .add(new LatLng(23.9884559601284,-104.66143224388361))
                .add(new LatLng(23.98877452988477,-104.66107349842787))
                .add(new LatLng(23.98885478483372,-104.66096721589565))
                .add(new LatLng(23.989240436734157,-104.6605984121561))
                .add(new LatLng(23.989600663359443,-104.66023195534945))
                .add(new LatLng(23.989959357407518,-104.6598906442523))
                .add(new LatLng(23.990339798679024,-104.65948093682528))
                .add(new LatLng(23.99069450861028,-104.65909369289875))
                .add(new LatLng(23.99105228067705,-104.65874634683134))
                .add(new LatLng(23.991377276530013,-104.65839564800264))
                .add(new LatLng(23.992200942835378,-104.65757522732018))
                .add(new LatLng(23.993048189475527,-104.65666830539703))
                .add(new LatLng(23.992922603738425,-104.65664450079203))
                .add(new LatLng(23.992407700935434,-104.65642623603343))
                .add(new LatLng(23.99203032886421,-104.65628609061241))
                .add(new LatLng(23.991658469263896,-104.65615298599003))
                .add(new LatLng(23.991403925540954,-104.65604905039072))
                .add(new LatLng(23.991298554706905,-104.6560215577483))
                .add(new LatLng(23.990950585293458,-104.65587940067051))
                .add(new LatLng(23.990602614939494,-104.65579055249691))
                .add(new LatLng(23.990104243789183,-104.65560782700776))
                .add(new LatLng(23.989520715190334,-104.65538185089827))
                .add(new LatLng(23.98913506412942,-104.65528797358273))
                .add(new LatLng(23.98898129348754,-104.65521086007357))
                .add(new LatLng(23.988216113339796,-104.65491849929094))
                .add(new LatLng(23.987853432289814,-104.6547408029437))
                .add(new LatLng(23.987494732374927,-104.65459764003752))
                .add(new LatLng(23.98713970730786,-104.65446151793005))
                .add(new LatLng(23.98677855483324,-104.65429991483688))
                .add(new LatLng(23.986425978369873,-104.65414769947529))
                .add(new LatLng(23.985995288521778,-104.65377889573574))
                .add(new LatLng(23.98594015031488,-104.65370144695044))
                .add(new LatLng(23.98590094090894,-104.6536048874259))
                .add(new LatLng(23.985886543702215,-104.65341813862322))
                .add(new LatLng(23.98485913036755,-104.65295545756817))
                .add(new LatLng(23.98378269627434,-104.6524964645505))
                .add(new LatLng(23.983829870865335,-104.65231072157621))
                .add(new LatLng(23.98378759753134,-104.65191107243298))
                .add(new LatLng(23.983277559473223,-104.651686437428))
                .add(new LatLng(23.98270073922345,-104.65146414935589))
                .add(new LatLng(23.982606695528354,-104.65144000947475))
                .add(new LatLng(23.982502236490085,-104.65144604444505))//Lillusion

                .width(30f)
                .color(Color.argb(80, 37, 10, 205));

        RUTAS.add(new Ruta("Ruta Plazuela - Azteca", inicioPlazuelaAzteca,finPlazuelaAzteca, baseAzteca,
                rutaInicioPlazuelaAzteca, rutaVueltaPlazuelaAzteca, paradasRutaAztecaCentro,"8"));

        /*******************************************************************************************
         ********************************** RUTAS AMARILLAS ***************************************/


        /************************************ RUTA HIPODROMO **************************************/
        MarkerOptions baseHipodromo = new MarkerOptions()
                .position(new LatLng(24.02702473631371,-104.68344613909721))
                .title("Base")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.base));
        MarkerOptions inicioHipodromo = new MarkerOptions()
                .position(new LatLng(24.024501718606707,-104.61986437439919))
                .title("Inicio");
        MarkerOptions finHipodromo =  new MarkerOptions()
                .position(new LatLng(24.041778146267298,-104.7027375921607))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        List<LatLng> paradasHipodromo = new ArrayList<>();
        paradasHipodromo.add(new LatLng(24.024535404082528,-104.61986571550369));
        paradasHipodromo.add(new LatLng(24.027066382992288,-104.63108405470848));
        paradasHipodromo.add(new LatLng(24.026302654255936,-104.64582681655884));
        paradasHipodromo.add(new LatLng(24.028252695213304,-104.6443485841155));
        paradasHipodromo.add(new LatLng(24.032677537071688,-104.63969662785529));
        paradasHipodromo.add(new LatLng(24.032081034404342,-104.64034102857113));
        paradasHipodromo.add(new LatLng(24.028764698550436,-104.6466301381588));
        paradasHipodromo.add(new LatLng(24.028435816258384,-104.6485586464405));
        paradasHipodromo.add(new LatLng(24.027664746281427,-104.65310230851175));
        paradasHipodromo.add(new LatLng(24.026982170944454,-104.65707197785376));
        paradasHipodromo.add(new LatLng(24.02655498534284,-104.65980146080256));
        paradasHipodromo.add(new LatLng(24.026034705047305,-104.66291718184948));
        paradasHipodromo.add(new LatLng(24.025530346564487,-104.66579418629408));
        paradasHipodromo.add(new LatLng(24.02501128704306,-104.668862298131));
        paradasHipodromo.add(new LatLng(24.02428796874533,-104.67390451580286));
        paradasHipodromo.add(new LatLng(24.027783561178257,-104.68392692506315));
        paradasHipodromo.add(new LatLng(24.02945583857594,-104.68264248222113));
        paradasHipodromo.add(new LatLng(24.0300211191476,-104.68578569591047));
        paradasHipodromo.add(new LatLng(24.027167743602135,-104.68346323817968));
        paradasHipodromo.add(new LatLng(24.02239666508779,-104.67775147408247));
        paradasHipodromo.add(new LatLng(24.02413852737039,-104.67345423996449));
        paradasHipodromo.add(new LatLng(24.02485602812409,-104.66893907636404));
        paradasHipodromo.add(new LatLng(24.02525382131043,-104.66668736189604));
        paradasHipodromo.add(new LatLng(24.026597550853573,-104.6588784456253));
        paradasHipodromo.add(new LatLng(24.027084144071686,-104.655716791749));
        paradasHipodromo.add(new LatLng(24.027751713897096,-104.65183194726706));
        paradasHipodromo.add(new LatLng(24.030746854914288,-104.647005982697));
        paradasHipodromo.add(new LatLng(24.032062049161052,-104.64033130556345));
        paradasHipodromo.add(new LatLng(24.028378858904432,-104.64443173259497));
        paradasHipodromo.add(new LatLng(24.026448112163845,-104.64590292423964));
        paradasHipodromo.add(new LatLng(24.026919088428834,-104.62941572070122));
        paradasHipodromo.add(new LatLng(24.030663563929018,-104.61654111742973));



        PolylineOptions rutaInicioHipodromo = new PolylineOptions()
                .add(new LatLng(24.02449314412054,-104.61985029280186))
                .add(new LatLng(24.02448732571889,-104.61986269801855))
                .add(new LatLng(24.024431897774175,-104.61996495723724))
                .add(new LatLng(24.025016186729758,-104.62043602019547))
                .add(new LatLng(24.02541030466681,-104.62073978036642))
                .add(new LatLng(24.025998570139866,-104.62123062461613))
                .add(new LatLng(24.02639268506466,-104.6215558424592))
                .add(new LatLng(24.02648485939873,-104.62161585688591))
                .add(new LatLng(24.02719285407319,-104.62217375636101))
                .add(new LatLng(24.027952596615393,-104.62279736995697))
                .add(new LatLng(24.0281984937837,-104.62299652397633))
                .add(new LatLng(24.02870529156929,-104.62341293692589))
                .add(new LatLng(24.02879225848082,-104.62348502129316))//ProlPinoSuarez
                .add(new LatLng(24.02866119564798,-104.62371300905944))
                .add(new LatLng(24.02841070603018,-104.62408013641836))
                .add(new LatLng(24.02785858599211,-104.62488748133184))
                .add(new LatLng(24.027429259730884,-104.62553188204764))
                .add(new LatLng(24.027126403181782,-104.6259456127882))
                .add(new LatLng(24.02710925455907,-104.62600260972975))
                .add(new LatLng(24.027089349904838,-104.62666109204291))
                .add(new LatLng(24.027090574806724,-104.62743893265724))
                .add(new LatLng(24.027085675199107,-104.62754789739847))
                .add(new LatLng(24.02705995225603,-104.62844509631397))
                .add(new LatLng(24.0270706701496,-104.62885446846485))
                .add(new LatLng(24.027056277549455,-104.62941706180573))
                .add(new LatLng(24.027048009459275,-104.6301757916808))
                .add(new LatLng(24.027039128917387,-104.63122051209211))
                .add(new LatLng(24.026904083359874,-104.63122252374887))
                .add(new LatLng(24.02575909956424,-104.63117826730014))
                .add(new LatLng(24.024859702893572,-104.63114272803068))
                .add(new LatLng(24.02398204246575,-104.63110886514185))
                .add(new LatLng(24.023072833839944,-104.63107097893953))
                .add(new LatLng(24.02214922563381,-104.63103476911785))
                .add(new LatLng(24.02126480943461,-104.63100124150515))
                .add(new LatLng(24.021217648566125,-104.6319343149662))
                .add(new LatLng(24.021170181440784,-104.63292337954044))
                .add(new LatLng(24.02206439788958,-104.6329652890563))
                .add(new LatLng(24.0229717762362,-104.63300887495281))
                .add(new LatLng(24.023873329748614,-104.63305547833444))
                .add(new LatLng(24.024787126176882,-104.6331275627017))
                .add(new LatLng(24.02475252207392,-104.63410053402187))
                .add(new LatLng(24.024702606404556,-104.63509026914835))
                .add(new LatLng(24.025606291380637,-104.63513687252998))
                .add(new LatLng(24.025563725541637,-104.63612090796232))
                .add(new LatLng(24.02552146591773,-104.63710360229015))
                .add(new LatLng(24.025450114492237,-104.63881552219391))
                .add(new LatLng(24.025390399749465,-104.63880311697721))
                .add(new LatLng(24.02533252235692,-104.6397713944316))
                .add(new LatLng(24.02523789735741,-104.6412107348442))
                .add(new LatLng(24.025181551046302,-104.64212436228989))
                .add(new LatLng(24.025122448639078,-104.64303497225046))
                .add(new LatLng(24.025091825619462,-104.64354693889618))
                .add(new LatLng(24.02500730604748,-104.64488737285136))
                .add(new LatLng(24.024946978637843,-104.64578121900557))
                .add(new LatLng(24.025965497503798,-104.64586235582829))
                .add(new LatLng(24.026684825417828,-104.6458449214697))
                .add(new LatLng(24.026897346389582,-104.64582614600658))
                .add(new LatLng(24.02697665888121,-104.64571785181761))
                .add(new LatLng(24.027060564707114,-104.64567594230175))
                .add(new LatLng(24.027363421411344,-104.64539129287003))
                .add(new LatLng(24.027935141879464,-104.64474856853485))
                .add(new LatLng(24.028512065579818,-104.64409779757263))
                .add(new LatLng(24.02894567531863,-104.64361734688282))
                .add(new LatLng(24.029216068221647,-104.64328810572624))
                .add(new LatLng(24.029574958036445,-104.64287638664246))
                .add(new LatLng(24.029658249727298,-104.64275065809488))
                .add(new LatLng(24.029851167774513,-104.64247539639473))
                .add(new LatLng(24.03047646523217,-104.64175220578907))
                .add(new LatLng(24.031320091407395,-104.64085936546326))
                .add(new LatLng(24.03185872315367,-104.64024882763623))
                .add(new LatLng(24.032106143915367,-104.64000608772038))
                .add(new LatLng(24.032406232813727,-104.63967382907867))
                .add(new LatLng(24.032761439175438,-104.63891509920359))
                .add(new LatLng(24.032822681552343,-104.63868107646704))
                .add(new LatLng(24.032863101505114,-104.63843666017054))
                .add(new LatLng(24.03293444881496,-104.6383960917592))
                .add(new LatLng(24.033013451369335,-104.638358540833))
                .add(new LatLng(24.033127974365794,-104.63835787028074))
                .add(new LatLng(24.033173293598246,-104.6388805657625))
                .add(new LatLng(24.033194728364773,-104.63906899094582))
                .add(new LatLng(24.03277521871279,-104.63953837752341))//empieza libramiento
                .add(new LatLng(24.032081034404342,-104.64021798223257))
                .add(new LatLng(24.031950893567366,-104.64112255722283))
                .add(new LatLng(24.03179380574621,-104.64207138866186))
                .add(new LatLng(24.031632430728028,-104.64298132807018))
                .add(new LatLng(24.031285182847924,-104.64490111917257))
                .add(new LatLng(24.031032554832258,-104.64491285383701))
                .add(new LatLng(24.030248639282593,-104.6448528394103))
                .add(new LatLng(24.02912236474323,-104.64477572590114))
                .add(new LatLng(24.029038154042563,-104.6450623869896))
                .add(new LatLng(24.028897292383565,-104.64575842022894))
                .add(new LatLng(24.028772047865793,-104.64647423475981))
                .add(new LatLng(24.028521558464053,-104.64789882302284))
                .add(new LatLng(24.028399375803655,-104.64860793203114))
                .add(new LatLng(24.02828852326439,-104.64929893612863))
                .add(new LatLng(24.028150416790286,-104.65007845312357))
                .add(new LatLng(24.028083047724664,-104.65053610503675))
                .add(new LatLng(24.028041095152332,-104.65065378695728))
                .add(new LatLng(24.02793391698562,-104.6513682603836))
                .add(new LatLng(24.027801015934784,-104.65216588228941))
                .add(new LatLng(24.02766199026446,-104.65297222137451))
                .add(new LatLng(24.027405067980187,-104.65449035167696))
                .add(new LatLng(24.02724736215201,-104.65538386255503))
                .add(new LatLng(24.027118441321523,-104.65604234486818))
                .add(new LatLng(24.02694634252918,-104.65712428092957))
                .add(new LatLng(24.02681068442191,-104.65807244181634))
                .add(new LatLng(24.026705648813873,-104.65883016586302))
                .add(new LatLng(24.026574277626427,-104.65964958071709))
                .add(new LatLng(24.02642851363427,-104.6604673191905))
                .add(new LatLng(24.026315515803915,-104.66115195304157))
                .add(new LatLng(24.026105137464846,-104.66239180415869))
                .add(new LatLng(24.025769205109068,-104.66425258666277))
                .add(new LatLng(24.025632627072525,-104.66502506285907))
                .add(new LatLng(24.025475531528183,-104.66589979827404))
                .add(new LatLng(24.02536651384459,-104.66664109379052))
                .add(new LatLng(24.02519931238616,-104.66753829270601))
                .add(new LatLng(24.025095500382204,-104.66817833483219))
                .add(new LatLng(24.025029660867226,-104.66867387294769))
                .add(new LatLng(24.02489308204487,-104.66923981904984))
                .add(new LatLng(24.024759259156642,-104.66994993388654))
                .add(new LatLng(24.02459879399944,-104.67063054442406))
                .add(new LatLng(24.02446619573174,-104.6713376417756))
                .add(new LatLng(24.02435595226414,-104.67206183820961))
                .add(new LatLng(24.0243008304949,-104.67268209904432))
                .add(new LatLng(24.024290418602494,-104.67316254973412))
                .add(new LatLng(24.024267144957605,-104.67346228659153))
                .add(new LatLng(24.024261939010145,-104.67415865510702))//Independencia
                .add(new LatLng(24.02422365997824,-104.67487949877976))
                .add(new LatLng(24.024229784624115,-104.67544075101614))
                .add(new LatLng(24.024412298937175,-104.67624306678772))
                .add(new LatLng(24.02468423253365,-104.67648144811392))
                .add(new LatLng(24.024923705111828,-104.67671949416399))
                .add(new LatLng(24.024993219446753,-104.67678118497135))
                .add(new LatLng(24.025009755890935,-104.67683214694262))
                .add(new LatLng(24.024942691409684,-104.6774373203516))
                .add(new LatLng(24.024870114739862,-104.67826008796692))
                .add(new LatLng(24.02483796050596,-104.67852361500263))
                .add(new LatLng(24.024745784990845,-104.67942550778389))
                .add(new LatLng(24.024637072919642,-104.68032974749804))
                .add(new LatLng(24.025145722129096,-104.68093726783991))
                .add(new LatLng(24.025635383132997,-104.681495167315))
                .add(new LatLng(24.026119223943713,-104.68206144869326))
                .add(new LatLng(24.02647169164077,-104.68249697238207))
                .add(new LatLng(24.02686458021079,-104.6829616650939))
                .add(new LatLng(24.02755818024957,-104.68375392258167))
                .add(new LatLng(24.02789870128277,-104.6841311082244))
                .add(new LatLng(24.028610056609196,-104.68338947743177))
                .add(new LatLng(24.02939826923411,-104.68255899846554))
                .add(new LatLng(24.029434403185718,-104.6826223656535))
                .add(new LatLng(24.029541273881428,-104.68311153352262))
                .add(new LatLng(24.02968979031672,-104.68390010297298))
                .add(new LatLng(24.029807990903397,-104.68448381870985))
                .add(new LatLng(24.02997212418023,-104.68562945723535))
                .add(new LatLng(24.030030611920374,-104.68602910637854))
                .add(new LatLng(24.030025712424838,-104.68621384352446))
                .add(new LatLng(24.02999233460654,-104.68632146716118))
                .add(new LatLng(24.029932621974176,-104.68640193343161))
                .add(new LatLng(24.029523206921915,-104.68689545989037))
                .add(new LatLng(24.029655799972456,-104.68700006604195))//termionadif
                .add(new LatLng(24.029752871472358,-104.68708019703627))
                .add(new LatLng(24.029852392650092,-104.68715496361256))
                .add(new LatLng(24.029885158067113,-104.68721363693477))
                .add(new LatLng(24.029984679142398,-104.68725588172673))
                .add(new LatLng(24.030064602165506,-104.68727800995113))
                .add(new LatLng(24.030604157825653,-104.68728605657816))
                .add(new LatLng(24.031478404667734,-104.68727633357048))
                .add(new LatLng(24.03193711394161,-104.68727096915245))
                .add(new LatLng(24.032315593791775,-104.6872441470623))
                .add(new LatLng(24.032495953134,-104.68724381178619))
                .add(new LatLng(24.03258873551533,-104.68718279153109))
                .add(new LatLng(24.03289096676818,-104.68752712011337))
                .add(new LatLng(24.03320422090309,-104.68784764409065))
                .add(new LatLng(24.033853386376045,-104.6885222196579))
                .add(new LatLng(24.034537150046773,-104.68914717435835))
                .add(new LatLng(24.033954435423524,-104.68989416956902))
                .add(new LatLng(24.033422549090936,-104.69056505709887))
                .add(new LatLng(24.034129893126266,-104.69113167375328))
                .add(new LatLng(24.034810287047662,-104.69168119132519))
                .add(new LatLng(24.03425115171275,-104.69238895922898))
                .add(new LatLng(24.034155920862805,-104.69249557703733))
                .add(new LatLng(24.03384328146694,-104.69258107244968))
                .add(new LatLng(24.033538908983406,-104.69295792281629))
                .add(new LatLng(24.033062445177098,-104.69370692968369))
                .add(new LatLng(24.032993853840996,-104.6938155591488))
                .add(new LatLng(24.032872287856243,-104.6939379349351))
                .add(new LatLng(24.032534842127067,-104.69442710280418))
                .add(new LatLng(24.03319717805215,-104.69500377774239))
                .add(new LatLng(24.033917384115345,-104.6955543011427))
                .add(new LatLng(24.034596860539384,-104.69611119478941))
                .add(new LatLng(24.035675319121353,-104.69699129462244))
                .add(new LatLng(24.03598734165062,-104.69724845141172))
                .add(new LatLng(24.036673543585376,-104.69780735671522))
                .add(new LatLng(24.036346824959764,-104.69823449850081))
                .add(new LatLng(24.036011225581397,-104.69866666942836))
                .add(new LatLng(24.035712063717654,-104.69905123114586))
                .add(new LatLng(24.035399734314144,-104.69948273152112))
                .add(new LatLng(24.036095125508186,-104.70004834234715))
                .add(new LatLng(24.036783470283833,-104.70060724765062))
                .add(new LatLng(24.037331265158837,-104.70106523483992))
                .add(new LatLng(24.037762089336255,-104.70142230391502))
                .add(new LatLng(24.037997250751012,-104.70161575824022))
                .add(new LatLng(24.03825017127131,-104.70138072967531))
                .add(new LatLng(24.03833131394774,-104.7014420852065))
                .add(new LatLng(24.038584540008372,-104.70159195363522))
                .add(new LatLng(24.03872508484958,-104.70172706991436))
                .add(new LatLng(24.03878173141966,-104.70172170549633))
                .add(new LatLng(24.039426888160826,-104.70164157450199))
                .add(new LatLng(24.040071735467095,-104.70154903829098))
                .add(new LatLng(24.040190538891167,-104.70157116651535))
                .add(new LatLng(24.040214115846283,-104.7014668956399))
                .add(new LatLng(24.040210441515896,-104.7013582661748))
                .add(new LatLng(24.040733420149724,-104.7012948989868))
                .add(new LatLng(24.040799864004036,-104.70129624009132))
                .add(new LatLng(24.04110942454954,-104.70165934413671))
                .add(new LatLng(24.04144807252125,-104.70211330801247))
                .add(new LatLng(24.041625663324915,-104.70232587307692))
                .add(new LatLng(24.04185408148031,-104.70266919583082))
                .add(new LatLng(24.041781820552828,-104.70273490995169))

                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo

        PolylineOptions rutaVueltaHipodromo = new PolylineOptions()
                .add(new LatLng(24.041768960552993,-104.70273055136204))
                .add(new LatLng(24.041530437941745,-104.7024029865861))
                .add(new LatLng(24.041290383929407,-104.70207206904888))
                .add(new LatLng(24.0410273650513,-104.701734110713))
                .add(new LatLng(24.040823134654563,-104.70146622508764))
                .add(new LatLng(24.040702188464397,-104.70140486955643))
                .add(new LatLng(24.04021105390431,-104.70146723091601))
                .add(new LatLng(24.04019390702788,-104.70157686620952))
                .add(new LatLng(24.04007663457955,-104.70155239105223))
                .add(new LatLng(24.039429950121505,-104.7016392275691))
                .add(new LatLng(24.038781119024442,-104.70172505825758))
                .add(new LatLng(24.03872294146536,-104.70172505825758))
                .add(new LatLng(24.038589439177528,-104.70159932971))
                .add(new LatLng(24.038326720967415,-104.70144644379616))
                .add(new LatLng(24.038058184432792,-104.70167946070433))
                .add(new LatLng(24.037994494955683,-104.70161274075508))
                .add(new LatLng(24.03775198473457,-104.70141626894475))
                .add(new LatLng(24.037070993989083,-104.70085099339484))
                .add(new LatLng(24.03678530749805,-104.70060724765062))
                .add(new LatLng(24.036093900692123,-104.70004834234715))
                .add(new LatLng(24.035399121902802,-104.6994834020734))
                .add(new LatLng(24.03600877594767,-104.69866666942836))
                .add(new LatLng(24.03634560014611,-104.69823315739632))
                .add(new LatLng(24.035685117681393,-104.69766855239867))
                .add(new LatLng(24.034978700828336,-104.69708684831859))
                .add(new LatLng(24.0343087188804,-104.69653464853764))
                .add(new LatLng(24.033624035364305,-104.69596803188325))
                .add(new LatLng(24.03294302273794,-104.69540409743784))
                .add(new LatLng(24.032226791985284,-104.69487838447094))
                .add(new LatLng(24.03253606697705,-104.69442307949065))
                .add(new LatLng(24.03287626860819,-104.69392854720354))
                .add(new LatLng(24.032992935206778,-104.69381421804428))
                .add(new LatLng(24.03353860277331,-104.69295859336852))
                .add(new LatLng(24.033840525582487,-104.6925814077258))
                .add(new LatLng(24.03415377740235,-104.6924962475896))
                .add(new LatLng(24.03481396153251,-104.69167750328779))
                .add(new LatLng(24.033423467722084,-104.69056405127048))
                .add(new LatLng(24.034541130747144,-104.68914818018675))
                .add(new LatLng(24.033850630491823,-104.68851786106825))
                .add(new LatLng(24.033202077426758,-104.68784764409065))
                .add(new LatLng(24.032583529904944,-104.68718279153109))
                .add(new LatLng(24.032491666157657,-104.68723643571138))
                .add(new LatLng(24.03193650151377,-104.68726761639118))
                .add(new LatLng(24.03092629780941,-104.68727767467499))
                .add(new LatLng(24.030598645917763,-104.68729510903357))
                .add(new LatLng(24.02997947342652,-104.68725655227901))
                .add(new LatLng(24.029874134189413,-104.68721028417349))
                .add(new LatLng(24.029638651687243,-104.68712914735079))
                .add(new LatLng(24.029467474929127,-104.68699671328068))
                .add(new LatLng(24.0292344414447,-104.68661684542894))
                .add(new LatLng(24.028117650930962,-104.68458976596595))
                .add(new LatLng(24.028019659526127,-104.68445230275391))
                .add(new LatLng(24.02781755201772,-104.6842085570097))
                .add(new LatLng(24.026805478577472,-104.68303207308054))
                .add(new LatLng(24.02577502345269,-104.68181602656841))
                .add(new LatLng(24.025240040966587,-104.68119375407696))
                .add(new LatLng(24.024575826641843,-104.68043770641088))
                .add(new LatLng(24.02446803312206,-104.68030460178852))
                .add(new LatLng(24.024127809232322,-104.67992641031742))
                .add(new LatLng(24.02367397182281,-104.67942718416452))
                .add(new LatLng(24.02319563403333,-104.67884950339794))
                .add(new LatLng(24.02289736104083,-104.67847432941198))
                .add(new LatLng(24.022409833263204,-104.67774476855993))
                .add(new LatLng(24.022313062454252,-104.67764887958765))
                .add(new LatLng(24.022210166830657,-104.67759892344476))
                .add(new LatLng(24.022125339126646,-104.67759590595962))
                .add(new LatLng(24.022087365695892,-104.67755801975727))
                .add(new LatLng(24.022059191852907,-104.67746816575527))
                .add(new LatLng(24.022098084004092,-104.67738870531322))
                .add(new LatLng(24.022163312546198,-104.67735886573792))
                .add(new LatLng(24.022189955180632,-104.67712383717297))
                .add(new LatLng(24.02191066660037,-104.67690289020538))
                .add(new LatLng(24.021455290168884,-104.67646837234497))
                .add(new LatLng(24.02102747371966,-104.67603821307422))
                .add(new LatLng(24.020927333228506,-104.67591550201176))
                .add(new LatLng(24.020847098283287,-104.6756734326482))
                .add(new LatLng(24.020829030101687,-104.67512827366592))
                .add(new LatLng(24.02079687485697,-104.67449191957714))
                .add(new LatLng(24.020624767600925,-104.67324368655682))
                .add(new LatLng(24.02068448455751,-104.67323094606398))
                .add(new LatLng(24.0207879938832,-104.67320445924997))
                .add(new LatLng(24.02114231363645,-104.67311896383762))
                .add(new LatLng(24.021558186396685,-104.67316959053277))
                .add(new LatLng(24.021633214844204,-104.67315919697283))
                .add(new LatLng(24.022812840025946,-104.67331644147635))
                .add(new LatLng(24.024165782060333,-104.67346664518118))
                .add(new LatLng(24.024204979806523,-104.67271495610476))
                .add(new LatLng(24.024253058275757,-104.6720702201128))
                .add(new LatLng(24.024362076903714,-104.67134300619364))
                .add(new LatLng(24.024497737595343,-104.67060372233391))//Constitucion
                .add(new LatLng(24.024641053926686,-104.66994121670724))
                .add(new LatLng(24.024794781950632,-104.66922942548992))
                .add(new LatLng(24.024930442185568,-104.66866783797741))
                .add(new LatLng(24.0250027125909,-104.66818604618312))
                .add(new LatLng(24.02510805582077,-104.66752119362354))
                .add(new LatLng(24.025276788546872,-104.66661460697651))
                .add(new LatLng(24.025397136798762,-104.6658894047141))
                .add(new LatLng(24.025553926209792,-104.66501131653786))
                .add(new LatLng(24.02569203547397,-104.66426398605108))
                .add(new LatLng(24.025875466395846,-104.66315422207117))
                .add(new LatLng(24.026005307157284,-104.6623807400465))
                .add(new LatLng(24.026087376250146,-104.66183725744487))
                .add(new LatLng(24.026213848294397,-104.6611315011978))
                .add(new LatLng(24.026236815359297,-104.66103427112103))
                .add(new LatLng(24.02633572680525,-104.66044116765259))
                .add(new LatLng(24.026480878448805,-104.65963449329139))
                .add(new LatLng(24.026535693056356,-104.65926904231308))
                .add(new LatLng(24.0266119435051,-104.65881641954184))
                .add(new LatLng(24.026715141831538,-104.65805198997259))
                .add(new LatLng(24.026867642470847,-104.65711891651154))
                .add(new LatLng(24.02703851646619,-104.65604837983847))
                .add(new LatLng(24.027163150222748,-104.65539962053299))
                .add(new LatLng(24.0273104445065,-104.65449303388596))
                .add(new LatLng(24.027455288824637,-104.6536736190319))
                .add(new LatLng(24.02758022840135,-104.65297322720289))
                .add(new LatLng(24.027709454992603,-104.65215817093849))
                .add(new LatLng(24.02784572459856,-104.6513632312417))
                .add(new LatLng(24.027984137623438,-104.65052001178265))//DespuesLazaroCardenas JesusArritola
                .add(new LatLng(24.028134186973165,-104.64967545121908))
                .add(new LatLng(24.02843030425749,-104.64798901230097))
                .add(new LatLng(24.028580353086305,-104.64715484529734))
                .add(new LatLng(24.02870529156929,-104.64646719396116))
                .add(new LatLng(24.028784909166642,-104.64647993445396))
                .add(new LatLng(24.02981442150214,-104.64678570628166))
                .add(new LatLng(24.029863416529654,-104.64679442346096))
                .add(new LatLng(24.030305595807853,-104.6469295397401))
                .add(new LatLng(24.030596808615083,-104.64699190109967))
                .add(new LatLng(24.030781763620013,-104.6470472216606))
                .add(new LatLng(24.03093977132726,-104.64635856449604))
                .add(new LatLng(24.031097778840227,-104.64568365365267))
                .add(new LatLng(24.031277527460816,-104.64490447193386))
                .add(new LatLng(24.03149432784753,-104.64376788586377))
                .add(new LatLng(24.031627531293545,-104.64297730475664))
                .add(new LatLng(24.031700104147415,-104.64256625622511))
                .add(new LatLng(24.031778495032118,-104.64207541197538))
                .add(new LatLng(24.031949362497908,-104.64112658053637))
                .add(new LatLng(24.032079809550012,-104.64025050401688))
                .add(new LatLng(24.031708984367228,-104.64065819978714))
                .add(new LatLng(24.031165146325442,-104.64124795049429))
                .add(new LatLng(24.030613650555363,-104.6418658643961))
                .add(new LatLng(24.029726842843928,-104.64288711547852))
                .add(new LatLng(24.02964324497815,-104.64298535138369))
                .add(new LatLng(24.029194020350527,-104.64351508766413))
                .add(new LatLng(24.02873407639863,-104.64401632547379))
                .add(new LatLng(24.028338743763644,-104.64444547891617))
                .add(new LatLng(24.028260963225982,-104.6445306390524))
                .add(new LatLng(24.02815501013438,-104.64463893324137))
                .add(new LatLng(24.027612688173452,-104.6453071385622))
                .add(new LatLng(24.027336167399447,-104.64560955762865))
                .add(new LatLng(24.027251343078206,-104.64570041745901))
                .add(new LatLng(24.027063933188035,-104.64581742882727))
                .add(new LatLng(24.026974515301003,-104.64584324508907))
                .add(new LatLng(24.026883566221947,-104.64583754539488))
                .add(new LatLng(24.026690643720002,-104.64585028588772))
                .add(new LatLng(24.025962128994095,-104.64587409049273))
                .add(new LatLng(24.024942385179084,-104.64581977576019))
                .add(new LatLng(24.02402338389743,-104.64573260396719))
                .add(new LatLng(24.02408554913633,-104.64480858296156))
                .add(new LatLng(24.024149245507576,-104.64396670460701))
                .add(new LatLng(24.024193036744485,-104.64295651763678))
                .add(new LatLng(24.024314304707335,-104.64113663882019))
                .add(new LatLng(24.02438565676333,-104.64021764695646))
                .add(new LatLng(24.024422710819707,-104.63959638029337))
                .add(new LatLng(24.024488550645568,-104.63863145560026))
                .add(new LatLng(24.024558065215825,-104.6386395022273))
                .add(new LatLng(24.024605531090227,-104.63706403970718))
                .add(new LatLng(24.02470811856531,-104.63508993387222))
                .add(new LatLng(24.02479386325781,-104.63312555104494))
                .add(new LatLng(24.02387700454619,-104.63304474949837))
                .add(new LatLng(24.022065622839353,-104.63296730071306))
                .add(new LatLng(24.02117630623214,-104.63291566818953))
                .add(new LatLng(24.02126909678542,-104.63099654763937))
                .add(new LatLng(24.02215902522519,-104.63103510439396))
                .add(new LatLng(24.023074977485226,-104.63107265532018))
                .add(new LatLng(24.023983573630115,-104.63110785931349))
                .add(new LatLng(24.024860009124353,-104.63113836944103))
                .add(new LatLng(24.02575909956424,-104.63117692619564))
                .add(new LatLng(24.026919088428834,-104.63122084736824))
                .add(new LatLng(24.026965634754028,-104.62940867990255))
                .add(new LatLng(24.026976965106947,-104.6275706961751))
                .add(new LatLng(24.026974209075252,-104.62744429707527))
                .add(new LatLng(24.02698033359006,-104.6261266618967))
                .add(new LatLng(24.02699993203549,-104.62596137076616))
                .add(new LatLng(24.027082306718757,-104.62582021951675))
                .add(new LatLng(24.027482236586753,-104.62524019181728))
                .add(new LatLng(24.02778294873062,-104.62479662150145))
                .add(new LatLng(24.028322820192837,-104.62400268763304))
                .add(new LatLng(24.028718765319994,-104.6234303712845))
                .add(new LatLng(24.02914073797968,-104.62281715124846))
                .add(new LatLng(24.02970785725281,-104.62203059345484))
                .add(new LatLng(24.030194438694796,-104.62130337953568))
                .add(new LatLng(24.030722663788097,-104.62054967880249))
                .add(new LatLng(24.03105797076419,-104.6200705692172))
                .add(new LatLng(24.03095048889727,-104.6190345659852))
                .add(new LatLng(24.03069112345218,-104.61645260453224))
                .add(new LatLng(24.030491469884016,-104.61450565606356))
                .add(new LatLng(24.030384906196144,-104.61346965283155))
                .add(new LatLng(24.029719799802397,-104.61370065808296))
                .add(new LatLng(24.029565159010435,-104.61375396698713))
                .add(new LatLng(24.028569941540567,-104.61404666304588))
                .add(new LatLng(24.02766933964287,-104.61441580206156))
                .add(new LatLng(24.02754011301128,-104.61450230330227))
                .add(new LatLng(24.027013712190648,-104.61529891937971))
                .add(new LatLng(24.02672585975386,-104.61572304368019))
                .add(new LatLng(24.026144028391897,-104.61677111685276))
                .add(new LatLng(24.025647632289914,-104.61765289306639))
                .add(new LatLng(24.02520941797499,-104.6184253692627))
                .add(new LatLng(24.02452591090386,-104.61978357285261))




                .width(30f)
                .color(Color.argb(80, 37, 10, 205));

        RUTAS.add(new Ruta("Ruta Hipodromo", inicioHipodromo,finHipodromo, baseHipodromo,
                rutaInicioHipodromo, rutaVueltaHipodromo, paradasHipodromo,"19"));


        /****************************** RUTA SANTA FE LIBRAMIENTO *********************************/

        MarkerOptions inicioLibramiento = new MarkerOptions()
                .position(new LatLng(24.021448859151906,-104.5196332409978))
                .title("Inicio");
        MarkerOptions finLibramiento =  new MarkerOptions()
                .position(new LatLng(24.03042869530846,-104.7112214192748))
                .title("Fin")
                .snippet("Ruta de retorno")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        PolylineOptions rutaInicioSantaFeLibramiento = new PolylineOptions()
                .add(new LatLng(24.021448859151906,-104.5196332409978))
                .add(new LatLng(24.02123877908727,-104.51976299285889))
                .add(new LatLng(24.021076472096706,-104.51990481466055))
                .add(new LatLng(24.020984600124443,-104.5200214907527))
                .add(new LatLng(24.02086608518324,-104.52026389539242))
                .add(new LatLng(24.02081279936012,-104.5204761251807))
                .add(new LatLng(24.020793506211817,-104.52085934579371))
                .add(new LatLng(24.020800243502034,-104.52130895107985))
                .add(new LatLng(24.02079442493323,-104.52158354222775))
                .add(new LatLng(24.020785543959292,-104.52171061187983))
                .add(new LatLng(24.020887522002397,-104.5226302742958))
                .add(new LatLng(24.02100940556342,-104.52375043183564))
                .add(new LatLng(24.021096071442294,-104.52447965741158))
                .add(new LatLng(24.021236635411363,-104.52593743801118))
                .add(new LatLng(24.0213563749675,-104.52702607959509))
                .add(new LatLng(24.021488363965375,-104.52833030372858))
                .add(new LatLng(24.02159615998375,-104.52941793948412))
                .add(new LatLng(24.021731211117874,-104.53061856329441))
                .add(new LatLng(24.021912810265032,-104.53233886510134))
                .add(new LatLng(24.02203163047881,-104.53342851251364))
                .add(new LatLng(24.02217433708525,-104.53473307192326))
                .add(new LatLng(24.022313368691112,-104.53605238348246))
                .add(new LatLng(24.02246771197172,-104.53747026622297))
                .add(new LatLng(24.02257397598775,-104.5384881645441))
                .add(new LatLng(24.02267289025229,-104.53943565487863))
                .add(new LatLng(24.022793240942075,-104.54069729894398))
                .add(new LatLng(24.02296932635391,-104.54237300902605))
                .add(new LatLng(24.02310529475036,-104.54352837055922))
                .add(new LatLng(24.02322595127001,-104.54476855695248))
                .add(new LatLng(24.023417347801118,-104.54661626368761))
                .add(new LatLng(24.02364120482232,-104.54868055880068))
                .add(new LatLng(24.023785134575323,-104.55007195472717))
                .add(new LatLng(24.023925389371104,-104.55133058130741))
                .add(new LatLng(24.023970099383003,-104.55190055072308))
                .add(new LatLng(24.024023996362985,-104.55233305692673))
                .add(new LatLng(24.024043901491915,-104.55257277935743))
                .add(new LatLng(24.02410851658152,-104.55316588282587))
                .add(new LatLng(24.02420804212994,-104.5540875568986))
                .add(new LatLng(24.024290112370355,-104.55488316714764))
                .add(new LatLng(24.024369120238866,-104.55558624118567))
                .add(new LatLng(24.024412298937175,-104.55601640045643))
                .add(new LatLng(24.024531729303767,-104.55708593130112))
                .add(new LatLng(24.02464779101526,-104.55814808607103))
                .add(new LatLng(24.024761096542786,-104.55925416201354))
                .add(new LatLng(24.024889407276337,-104.5605305582285))
                .add(new LatLng(24.025026598563375,-104.5618810504675))
                .add(new LatLng(24.025119998797773,-104.56272192299366))
                .add(new LatLng(24.025260558366874,-104.56398356705904))
                .add(new LatLng(24.02537171974731,-104.56498906016348))
                .add(new LatLng(24.02546175120811,-104.56589262932539))
                .add(new LatLng(24.025619459227265,-104.56737656146288))
                .add(new LatLng(24.025781148024635,-104.56894766539335))
                .add(new LatLng(24.025911907566186,-104.57026295363903))
                .add(new LatLng(24.026085232655102,-104.57182802259923))
                .add(new LatLng(24.026245083501653,-104.57338705658913))
                .add(new LatLng(24.026417489460062,-104.57491725683212))
                .add(new LatLng(24.02648485939873,-104.57558277994394))
                .add(new LatLng(24.02660888123895,-104.5768591761589))
                .add(new LatLng(24.0267154480579,-104.57773558795452))
                .add(new LatLng(24.026824158371245,-104.57880076020956))
                .add(new LatLng(24.026949711013092,-104.57994271069765))
                .add(new LatLng(24.027046784556983,-104.5808717608452))
                .add(new LatLng(24.027173561882453,-104.58217497915028))
                .add(new LatLng(24.027324837078186,-104.58343762904406))
                .add(new LatLng(24.02743569044866,-104.58429761230947))
                .add(new LatLng(24.0275710416719,-104.5857148244977))
                .add(new LatLng(24.027653415989125,-104.58649165928362))
                .add(new LatLng(24.027766412643235,-104.5875333622098))
                .add(new LatLng(24.027855523755665,-104.58845570683478))
                .add(new LatLng(24.027949534381186,-104.58932742476463))
                .add(new LatLng(24.028038645366653,-104.59016930311917))
                .add(new LatLng(24.028086416178795,-104.59060817956924))
                .add(new LatLng(24.028157459917836,-104.59129080176353))
                .add(new LatLng(24.028275049468572,-104.59231708198786))
                .add(new LatLng(24.028358648224373,-104.5930590480566))
                .add(new LatLng(24.028444696703772,-104.59402095526457))
                .add(new LatLng(24.028573309981937,-104.59534998983145))
                .add(new LatLng(24.02867650673348,-104.59633603692055))
                .add(new LatLng(24.028765004771923,-104.59719032049179))
                .add(new LatLng(24.028850440536683,-104.5979654788971))
                .add(new LatLng(24.02893587624464,-104.59867894649506))
                .add(new LatLng(24.029033254509194,-104.59947757422924))
                .add(new LatLng(24.02916829782943,-104.60074726492167))
                .add(new LatLng(24.02941235535205,-104.60306134074925))
                .add(new LatLng(24.029615991449703,-104.60505958646537))
                .add(new LatLng(24.029836775485766,-104.60717920213938))
                .add(new LatLng(24.029985597798113,-104.60854444652796))
                .add(new LatLng(24.030099204838262,-104.60958547890186))
                .add(new LatLng(24.030101348366358,-104.60971221327782))
                .add(new LatLng(24.030172084773398,-104.61028285324574))
                .add(new LatLng(24.030214036650094,-104.61066775023939))
                .add(new LatLng(24.03029579683757,-104.61141910403967))
                .add(new LatLng(24.03033162431906,-104.61177919059993))
                .add(new LatLng(24.03052209161707,-104.61337577551603))
                .add(new LatLng(24.03038215023752,-104.61347065865993))
                .add(new LatLng(24.030063377291963,-104.61357995867729))
                .add(new LatLng(24.02970326396421,-104.61369764059782))
                .add(new LatLng(24.028579434420536,-104.61402554064989))
                .add(new LatLng(24.02782826984811,-104.61435075849295))
                .add(new LatLng(24.027508571894263,-104.61452443152668))
                .add(new LatLng(24.027074651081275,-104.61517285555601))
                .add(new LatLng(24.02673382163844,-104.61573243141174))
                .add(new LatLng(24.026335420577983,-104.61644355207682))
                .add(new LatLng(24.02614341593644,-104.61678754538298))
                .add(new LatLng(24.0266033691597,-104.61718250066042))
                .add(new LatLng(24.027051684166093,-104.61754728108644))
                .add(new LatLng(24.027663521385012,-104.61803812533617))
                .add(new LatLng(24.028016597293522,-104.61831606924532))
                .add(new LatLng(24.02879501447355,-104.61894571781157))
                .add(new LatLng(24.02918758972074,-104.61926724761724))
                .add(new LatLng(24.02950697727816,-104.61952507495882))
                .add(new LatLng(24.030184333497935,-104.62007962167263))
                .add(new LatLng(24.030738893278322,-104.6205473318696))
                .add(new LatLng(24.031068075892353,-104.62004508823156))
                .add(new LatLng(24.031211691112777,-104.61983688175678))
                .add(new LatLng(24.031230064050504,-104.62001893669367))
                .add(new LatLng(24.03133846432963,-104.6209617331624))
                .add(new LatLng(24.031433697267747,-104.62197963148355))
                .add(new LatLng(24.03152770527571,-104.62292242795229))
                .add(new LatLng(24.03164039230861,-104.62391082197428))
                .add(new LatLng(24.031741749310868,-104.62486200034618))
                .add(new LatLng(24.03184065651999,-104.62581720203161))
                .add(new LatLng(24.031951505995142,-104.6268578991294))
                .add(new LatLng(24.032097876150086,-104.62801057845354))
                .add(new LatLng(24.032158506416483,-104.62851718068123))
                .add(new LatLng(24.032200151431365,-104.62915018200876))
                .add(new LatLng(24.032297833435393,-104.63007185608149))
                .add(new LatLng(24.032394596726654,-104.6310143172741))
                .add(new LatLng(24.032500546322765,-104.63198393583298))
                .add(new LatLng(24.032600984009505,-104.63294215500355))
                .add(new LatLng(24.03271611979774,-104.633926525712))
                .add(new LatLng(24.032811657926654,-104.63487602770327))
                .add(new LatLng(24.032852996518123,-104.63544767349958))
                .add(new LatLng(24.032909339465796,-104.63592041283846))
                .add(new LatLng(24.033033355105992,-104.6369956433773))
                .add(new LatLng(24.03309980294297,-104.63761758059263))
                .add(new LatLng(24.033097353253737,-104.63793642818926))
                .add(new LatLng(24.033124299832725,-104.63811982423066))
                .add(new LatLng(24.03311970666626,-104.6382063254714))
                .add(new LatLng(24.03312828057689,-104.638358540833))
                .add(new LatLng(24.03317176254335,-104.63890738785267))
                .add(new LatLng(24.033192584888276,-104.6390810608864))
                .add(new LatLng(24.03277521871279,-104.63953837752341))//Esquina felipe pescador - heroico
                .add(new LatLng(24.032081034404342,-104.64021798223257))
                .add(new LatLng(24.031950893567366,-104.64112255722283))
                .add(new LatLng(24.03179380574621,-104.64207138866186))
                .add(new LatLng(24.031632430728028,-104.64298132807018))
                .add(new LatLng(24.031285182847924,-104.64490111917257))
                .add(new LatLng(24.031032554832258,-104.64491285383701))
                .add(new LatLng(24.030248639282593,-104.6448528394103))
                .add(new LatLng(24.02912236474323,-104.64477572590114))
                .add(new LatLng(24.029038154042563,-104.6450623869896))
                .add(new LatLng(24.028897292383565,-104.64575842022894))
                .add(new LatLng(24.028772047865793,-104.64647423475981))
                .add(new LatLng(24.028521558464053,-104.64789882302284))
                .add(new LatLng(24.028399375803655,-104.64860793203114))
                .add(new LatLng(24.02828852326439,-104.64929893612863))
                .add(new LatLng(24.028150416790286,-104.65007845312357))
                .add(new LatLng(24.028083047724664,-104.65053610503675))
                .add(new LatLng(24.028041095152332,-104.65065378695728))
                .add(new LatLng(24.02793391698562,-104.6513682603836))
                .add(new LatLng(24.027801015934784,-104.65216588228941))
                .add(new LatLng(24.02766199026446,-104.65297222137451))
                .add(new LatLng(24.027405067980187,-104.65449035167696))
                .add(new LatLng(24.02724736215201,-104.65538386255503))
                .add(new LatLng(24.027118441321523,-104.65604234486818))
                .add(new LatLng(24.02694634252918,-104.65712428092957))
                .add(new LatLng(24.02681068442191,-104.65807244181634))
                .add(new LatLng(24.026705648813873,-104.65883016586302))
                .add(new LatLng(24.026574277626427,-104.65964958071709))
                .add(new LatLng(24.02642851363427,-104.6604673191905))
                .add(new LatLng(24.026315515803915,-104.66115195304157))
                .add(new LatLng(24.026105137464846,-104.66239180415869))
                .add(new LatLng(24.025769205109068,-104.66425258666277))
                .add(new LatLng(24.025632627072525,-104.66502506285907))
                .add(new LatLng(24.025475531528183,-104.66589979827404))
                .add(new LatLng(24.02536651384459,-104.66664109379052))
                .add(new LatLng(24.02519931238616,-104.66753829270601))
                .add(new LatLng(24.025095500382204,-104.66817833483219))
                .add(new LatLng(24.025029660867226,-104.66867387294769))
                .add(new LatLng(24.02489308204487,-104.66923981904984))
                .add(new LatLng(24.024759259156642,-104.66994993388654))
                .add(new LatLng(24.02459879399944,-104.67063054442406))
                .add(new LatLng(24.02446619573174,-104.6713376417756))
                .add(new LatLng(24.02435595226414,-104.67206183820961))
                .add(new LatLng(24.0243008304949,-104.67268209904432))
                .add(new LatLng(24.024290418602494,-104.67316254973412))
                .add(new LatLng(24.024267144957605,-104.67346228659153))
                .add(new LatLng(24.024261939010145,-104.67415865510702))//Independencia
                .add(new LatLng(24.02422365997824,-104.67487949877976))
                .add(new LatLng(24.024229784624115,-104.67544075101614))
                .add(new LatLng(24.024412298937175,-104.67624306678772))
                .add(new LatLng(24.02468423253365,-104.67648144811392))
                .add(new LatLng(24.024923705111828,-104.67671949416399))
                .add(new LatLng(24.024993219446753,-104.67678118497135))
                .add(new LatLng(24.025009755890935,-104.67683214694262))
                .add(new LatLng(24.024942691409684,-104.6774373203516))
                .add(new LatLng(24.024870114739862,-104.67826008796692))
                .add(new LatLng(24.02483796050596,-104.67852361500263))
                .add(new LatLng(24.024745784990845,-104.67942550778389))
                .add(new LatLng(24.024637072919642,-104.68032974749804))
                .add(new LatLng(24.025145722129096,-104.68093726783991))
                .add(new LatLng(24.025635383132997,-104.681495167315))
                .add(new LatLng(24.026119223943713,-104.68206144869326))
                .add(new LatLng(24.02647169164077,-104.68249697238207))
                .add(new LatLng(24.02686458021079,-104.6829616650939))
                .add(new LatLng(24.02755818024957,-104.68375392258167))
                .add(new LatLng(24.02789870128277,-104.6841311082244))
                .add(new LatLng(24.028610056609196,-104.68338947743177))
                .add(new LatLng(24.02939826923411,-104.68255899846554))
                .add(new LatLng(24.029434403185718,-104.6826223656535))
                .add(new LatLng(24.029541273881428,-104.68311153352262))
                .add(new LatLng(24.02968979031672,-104.68390010297298))
                .add(new LatLng(24.029807990903397,-104.68448381870985))
                .add(new LatLng(24.02997212418023,-104.68562945723535))
                .add(new LatLng(24.030030611920374,-104.68602910637854))
                .add(new LatLng(24.030025712424838,-104.68621384352446))
                .add(new LatLng(24.02999233460654,-104.68632146716118))
                .add(new LatLng(24.029932621974176,-104.68640193343161))
                .add(new LatLng(24.029523206921915,-104.68689545989037))
                .add(new LatLng(24.029655799972456,-104.68700006604195))//sahuatoba
                .add(new LatLng(24.02988117722251,-104.68718379735947))
                .add(new LatLng(24.029984066705246,-104.6872652694583))
                .add(new LatLng(24.0297605269503,-104.68742586672306))
                .add(new LatLng(24.0292540395464,-104.68802131712437))
                .add(new LatLng(24.028765923436353,-104.68862783163785))
                .add(new LatLng(24.028447146481703,-104.68900937587023))
                .add(new LatLng(24.02802486532139,-104.68951363116503))
                .add(new LatLng(24.027697818480238,-104.68991830945015))
                .add(new LatLng(24.027049234361566,-104.69071157276629))
                .add(new LatLng(24.027145082928918,-104.69138983637094))
                .add(new LatLng(24.027197753676724,-104.69188906252384))
                .add(new LatLng(24.027209084009186,-104.69208654016256))
                .add(new LatLng(24.027178767711973,-104.69223339110614))
                .add(new LatLng(24.02712579073102,-104.6923939883709))
                .add(new LatLng(24.026813440457133,-104.69340082257986))
                .add(new LatLng(24.026695237116275,-104.69383098185061))
                .add(new LatLng(24.02632286525931,-104.69491694122553))
                .add(new LatLng(24.02619823068792,-104.69528507441284))
                .add(new LatLng(24.026093194579367,-104.69545807689428))
                .add(new LatLng(24.02589506500976,-104.69578597694634))
                .add(new LatLng(24.02592844389218,-104.69600960612297))
                .add(new LatLng(24.02594498021604,-104.69655074179173))
                .add(new LatLng(24.02596396636303,-104.69710294157267))
                .add(new LatLng(24.02692245691346,-104.69703823328018))
                .add(new LatLng(24.02780744663396,-104.69696547836064))
                .add(new LatLng(24.027811121319054,-104.69690579921007))
                .add(new LatLng(24.02877878473782,-104.69685584306717))
                .add(new LatLng(24.029748890623644,-104.69680018723011))
                .add(new LatLng(24.029758689635642,-104.69730645418167))
                .add(new LatLng(24.0298009478663,-104.69830960035324))
                .add(new LatLng(24.029843206083058,-104.69883095473051))
                .add(new LatLng(24.02987260309522,-104.69973754137754))
                .add(new LatLng(24.029921291881816,-104.70078092068434))
                .add(new LatLng(24.029962018965872,-104.70183536410332))
                .add(new LatLng(24.029999683851678,-104.70283046364784))
                .add(new LatLng(24.030058171579277,-104.70384735614061))
                .add(new LatLng(24.030113290879367,-104.70488335937262))
                .add(new LatLng(24.030155242775248,-104.70585498958826))
                .add(new LatLng(24.030191682732088,-104.70686517655848))
                .add(new LatLng(24.030240065179832,-104.70791090279818))
                .add(new LatLng(24.03028446677724,-104.70889829099178))
                .add(new LatLng(24.03033315540779,-104.70991417765617))
                .add(new LatLng(24.03035918391341,-104.71038524061441))
                .add(new LatLng(24.030375719667187,-104.71092838793993))
                .add(new LatLng(24.03035489686579,-104.71101522445679))
                .add(new LatLng(24.030430838831073,-104.71121571958065))


                .width(20f)
                .color(Color.argb(150, 241, 57, 43));    // Rojo


        PolylineOptions rutaVueltaLibramiento = new PolylineOptions()
                .add(new LatLng(24.030430838831073,-104.71121571958065))
                .add(new LatLng(24.03042869530846,-104.7112214192748))
                .add(new LatLng(24.03042624556831,-104.71121605485679))
                .add(new LatLng(24.030351834688826,-104.7110179066658))
                .add(new LatLng(24.03037173883776,-104.71093039959668))
                .add(new LatLng(24.030356427954246,-104.71038457006216))
                .add(new LatLng(24.03030375850099,-104.70940858125687))
                .add(new LatLng(24.030258438256478,-104.70839470624925))
                .add(new LatLng(24.030209137161542,-104.70739625394344))
                .add(new LatLng(24.030165960410685,-104.70634683966637))
                .add(new LatLng(24.03013411371992,-104.70537118613719))
                .add(new LatLng(24.030078688210416,-104.70436267554759))
                .add(new LatLng(24.030026324861783,-104.70333471894266))
                .add(new LatLng(24.029980392082287,-104.70232184976338))
                .add(new LatLng(24.029939665004044,-104.70131803303957))
                .add(new LatLng(24.029917923475804,-104.70077924430369))
                .add(new LatLng(24.02989495706872,-104.70028202980757))
                .add(new LatLng(24.029890670005614,-104.70020927488804))
                .add(new LatLng(24.029871378219852,-104.69981499016284))
                .add(new LatLng(24.029851780212304,-104.69931274652481))
                .add(new LatLng(24.029830038669193,-104.69883665442467))
                .add(new LatLng(24.029797885676142,-104.69831362366675))
                .add(new LatLng(24.02975134037672,-104.69730477780104))
                .add(new LatLng(24.029738785391746,-104.69680152833462))
                .add(new LatLng(24.02877143542285,-104.69685215502977))
                .add(new LatLng(24.02874050705111,-104.69636332243681))
                .add(new LatLng(24.028732851512405,-104.69582855701447))
                .add(new LatLng(24.028705597790903,-104.69532463699579))
                .add(new LatLng(24.028664564086938,-104.69433020800352))
                .add(new LatLng(24.028639453903445,-104.69383165240288))
                .add(new LatLng(24.028633329467688,-104.69333946704865))
                .add(new LatLng(24.028610975274734,-104.6928335353732))
                .add(new LatLng(24.028564735767386,-104.69182401895523))
                .add(new LatLng(24.02852982645943,-104.69082958996296))
                .add(new LatLng(24.028510534469383,-104.69033438712358))
                .add(new LatLng(24.02847593136887,-104.68984220176935))
                .add(new LatLng(24.028450821148517,-104.68899294734001))
                .add(new LatLng(24.029242709394225,-104.6880216524005))
                .add(new LatLng(24.029743072459915,-104.6874339133501))
                .add(new LatLng(24.029974573929035,-104.68725219368933))
                .add(new LatLng(24.029856679714467,-104.68720592558384))
                .add(new LatLng(24.029726536624743,-104.68716971576214))
                .add(new LatLng(24.02951340789197,-104.68703627586363))
                .add(new LatLng(24.029448183079836,-104.68697492033243))
                .add(new LatLng(24.02891413454657,-104.68603782355785))//ParqueGuadiana
                .add(new LatLng(24.028117650930962,-104.68458976596595))
                .add(new LatLng(24.028019659526127,-104.68445230275391))
                .add(new LatLng(24.02781755201772,-104.6842085570097))
                .add(new LatLng(24.026805478577472,-104.68303207308054))
                .add(new LatLng(24.02577502345269,-104.68181602656841))
                .add(new LatLng(24.025240040966587,-104.68119375407696))
                .add(new LatLng(24.024575826641843,-104.68043770641088))
                .add(new LatLng(24.02446803312206,-104.68030460178852))
                .add(new LatLng(24.024127809232322,-104.67992641031742))
                .add(new LatLng(24.02367397182281,-104.67942718416452))
                .add(new LatLng(24.02319563403333,-104.67884950339794))
                .add(new LatLng(24.02289736104083,-104.67847432941198))
                .add(new LatLng(24.022409833263204,-104.67774476855993))
                .add(new LatLng(24.022313062454252,-104.67764887958765))
                .add(new LatLng(24.022210166830657,-104.67759892344476))
                .add(new LatLng(24.022125339126646,-104.67759590595962))
                .add(new LatLng(24.022087365695892,-104.67755801975727))
                .add(new LatLng(24.022059191852907,-104.67746816575527))
                .add(new LatLng(24.022098084004092,-104.67738870531322))
                .add(new LatLng(24.022163312546198,-104.67735886573792))
                .add(new LatLng(24.022189955180632,-104.67712383717297))
                .add(new LatLng(24.02191066660037,-104.67690289020538))
                .add(new LatLng(24.021455290168884,-104.67646837234497))
                .add(new LatLng(24.02102747371966,-104.67603821307422))
                .add(new LatLng(24.020927333228506,-104.67591550201176))
                .add(new LatLng(24.020847098283287,-104.6756734326482))
                .add(new LatLng(24.020829030101687,-104.67512827366592))
                .add(new LatLng(24.02079687485697,-104.67449191957714))
                .add(new LatLng(24.020624767600925,-104.67324368655682))
                .add(new LatLng(24.02068448455751,-104.67323094606398))
                .add(new LatLng(24.0207879938832,-104.67320445924997))
                .add(new LatLng(24.02114231363645,-104.67311896383762))
                .add(new LatLng(24.021558186396685,-104.67316959053277))
                .add(new LatLng(24.021633214844204,-104.67315919697283))
                .add(new LatLng(24.022812840025946,-104.67331644147635))
                .add(new LatLng(24.024165782060333,-104.67346664518118))
                .add(new LatLng(24.024204979806523,-104.67271495610476))
                .add(new LatLng(24.024253058275757,-104.6720702201128))
                .add(new LatLng(24.024362076903714,-104.67134300619364))
                .add(new LatLng(24.024497737595343,-104.67060372233391))//Constitucion
                .add(new LatLng(24.024641053926686,-104.66994121670724))
                .add(new LatLng(24.024794781950632,-104.66922942548992))
                .add(new LatLng(24.024930442185568,-104.66866783797741))
                .add(new LatLng(24.0250027125909,-104.66818604618312))
                .add(new LatLng(24.02510805582077,-104.66752119362354))
                .add(new LatLng(24.025276788546872,-104.66661460697651))
                .add(new LatLng(24.025397136798762,-104.6658894047141))
                .add(new LatLng(24.025553926209792,-104.66501131653786))
                .add(new LatLng(24.02569203547397,-104.66426398605108))
                .add(new LatLng(24.025875466395846,-104.66315422207117))
                .add(new LatLng(24.026005307157284,-104.6623807400465))
                .add(new LatLng(24.026087376250146,-104.66183725744487))
                .add(new LatLng(24.026213848294397,-104.6611315011978))
                .add(new LatLng(24.026236815359297,-104.66103427112103))
                .add(new LatLng(24.02633572680525,-104.66044116765259))
                .add(new LatLng(24.026480878448805,-104.65963449329139))
                .add(new LatLng(24.026535693056356,-104.65926904231308))
                .add(new LatLng(24.0266119435051,-104.65881641954184))
                .add(new LatLng(24.026715141831538,-104.65805198997259))
                .add(new LatLng(24.026867642470847,-104.65711891651154))
                .add(new LatLng(24.02703851646619,-104.65604837983847))
                .add(new LatLng(24.027163150222748,-104.65539962053299))
                .add(new LatLng(24.0273104445065,-104.65449303388596))
                .add(new LatLng(24.027455288824637,-104.6536736190319))
                .add(new LatLng(24.02758022840135,-104.65297322720289))
                .add(new LatLng(24.027709454992603,-104.65215817093849))
                .add(new LatLng(24.02784572459856,-104.6513632312417))
                .add(new LatLng(24.027984137623438,-104.65052001178265))//DespuesLazaroCardenas JesusArritola
                .add(new LatLng(24.028134186973165,-104.64967545121908))
                .add(new LatLng(24.02843030425749,-104.64798901230097))
                .add(new LatLng(24.028580353086305,-104.64715484529734))
                .add(new LatLng(24.02870529156929,-104.64646719396116))
                .add(new LatLng(24.028784909166642,-104.64647993445396))
                .add(new LatLng(24.02981442150214,-104.64678570628166))
                .add(new LatLng(24.029863416529654,-104.64679442346096))
                .add(new LatLng(24.030305595807853,-104.6469295397401))
                .add(new LatLng(24.030596808615083,-104.64699190109967))
                .add(new LatLng(24.030781763620013,-104.6470472216606))
                .add(new LatLng(24.03093977132726,-104.64635856449604))
                .add(new LatLng(24.031097778840227,-104.64568365365267))
                .add(new LatLng(24.031277527460816,-104.64490447193386))
                .add(new LatLng(24.03149432784753,-104.64376788586377))
                .add(new LatLng(24.031627531293545,-104.64297730475664))
                .add(new LatLng(24.031700104147415,-104.64256625622511))
                .add(new LatLng(24.031778495032118,-104.64207541197538))
                .add(new LatLng(24.031949362497908,-104.64112658053637))
                .add(new LatLng(24.032079809550012,-104.64025050401688)) ///Heroico


                .width(30f)
                .color(Color.argb(80, 37, 10, 205));

        RUTAS.add(new Ruta("Ruta Santa Fe - Libramiento", inicioLibramiento,finLibramiento, baseHipodromo,
                rutaInicioSantaFeLibramiento, rutaVueltaLibramiento, paradasHipodromo,"20"));



    }


    /***********************************************************************************************
     ********************************** METODOS SETTER AND GETTER **********************************
     **********************************************************************************************/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public MarkerOptions getInicio() {
        return inicio;
    }

    public void setInicio(MarkerOptions inicio) {
        this.inicio = inicio;
    }

    public MarkerOptions getFin() {
        return fin;
    }

    public void setFin(MarkerOptions fin) {
        this.fin = fin;
    }

    public MarkerOptions getBase() {
        return base;
    }

    public void setBase(MarkerOptions base) {
        this.base = base;
    }

    public PolylineOptions getRutaIda() {
        return rutaIda;
    }

    public void setRutaIda(PolylineOptions rutaIda) {
        this.rutaIda = rutaIda;
    }

    public PolylineOptions getRutaVuelta() {
        return rutaVuelta;
    }

    public void setRutaVuelta(PolylineOptions rutaVuelta) {
        this.rutaVuelta = rutaVuelta;
    }

    public List<LatLng> getParadasOficiales() {
        return paradasOficiales;
    }

    public void setParadasOficiales(List<LatLng> paradasOficiales) {
        this.paradasOficiales = paradasOficiales;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    /***********************************************************************************************
     ****************************** DEVUELVE RUTA EN BASE A SU NOMBRE ******************************
     **********************************************************************************************/

    public Ruta dameRuta(String rutaAObtener) {

        for (int i=0; i<RUTAS.size(); i++){
            Ruta rutaAEnviar = RUTAS.get(i);
            if (rutaAEnviar.getNombre().equals(rutaAObtener)){
                return rutaAEnviar;
            }

        }
         return null;
    }

    /***********************************************************************************************
     ********************************* DEVUELVE RUTA EN BASE A IDE *********************************
     **********************************************************************************************/

    public Ruta dameRutaById(String rutaAObtener) {

        for (int i=0; i<RUTAS.size(); i++){
            Ruta rutaAEnviar = RUTAS.get(i);
            if (rutaAEnviar.getIdentificador().equals(rutaAObtener)){
                return rutaAEnviar;
            }

        }
        return null;
    }


} /*TERMINA CLASE RUTA*/
