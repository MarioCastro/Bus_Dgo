package com.sooreck.busdgo;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import java.util.Stack;

/**
 * Fragmento que contiene los diferentes tipos de rutas
 */

public class FragmentoRutas extends Fragment {

    DataPassListener mCallback;

    public interface DataPassListener{
        public void passData(String dato);
    }
    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorTipoRuta adaptador;

    public FragmentoRutas() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_rutas, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        reciclador.setLayoutManager(layoutManager);

        adaptador = new AdaptadorTipoRuta(TipoRuta.TIPOSRUTAS);
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = reciclador.getChildAdapterPosition(v);
                mCallback.passData(TipoRuta.TIPOSRUTAS.get(posicion).getNombre());

                Fragment fragmentoGenerico = null;
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter,R.anim.pop_exit);

                fragmentoGenerico = new FragmentoIntoRutas();
                Bundle bundle = new Bundle();
                bundle.putString("tipo",TipoRuta.TIPOSRUTAS.get(posicion).getNombre());
                fragmentoGenerico.setArguments(bundle);
                if (fragmentoGenerico != null) {
                    ft.replace(R.id.contenedor_principal, fragmentoGenerico,"FragmentoIntoRutas").addToBackStack(null).commit();
                    /*fragmentManager
                            .beginTransaction()
                            .replace(R.id.contenedor_principal, fragmentoGenerico)
                            .commit();*/
                }

                // Setear título actual
                //getActivity().setTitle(itemDrawer.getTitle());

            }
        });
        reciclador.setAdapter(adaptador);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
           mCallback  = (DataPassListener)activity;
        }
        catch (ClassCastException e){
            e.printStackTrace();
        }
    }
}
