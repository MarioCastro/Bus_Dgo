package com.sooreck.busdgo;

/**
 * Created by SoOreck on 30/06/2017.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragmento que se muestra cuando no se tiene agregadas ninguna ruta a favoritos
 */

public class FragmentoNoFav extends Fragment {

    public FragmentoNoFav() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragmento_no_hay_rutas_fav, container, false);

        return view;
    }

}
