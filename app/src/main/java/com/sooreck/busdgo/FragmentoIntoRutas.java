package com.sooreck.busdgo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.LinearLayoutManager;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Fragmento que contiene los diferentes tipos de rutas
 */

public class FragmentoIntoRutas extends Fragment {
    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorRutas adaptador;
    private TextView textViewNombreRuta;
    private AlphaAnimation alphaAnimationShowIcon;

    private List<Camion> items;

    public FragmentoIntoRutas() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragmento_into_rutas, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        reciclador.setLayoutManager(layoutManager);

        alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);

        items = Camion.NOHAY;
        Bundle args = getArguments();
        if (args != null){
            String tipoRutaDato = args.getString("tipo");
            textViewNombreRuta = (TextView) view.findViewById(R.id.textViewNombreRuta);
            textViewNombreRuta.setText(tipoRutaDato);

            if (tipoRutaDato != null){
                if (tipoRutaDato.equals("Rutas Libertad/Nazas")){
                    items = Camion.RUTASLYN;
                }
                if (tipoRutaDato.equals("Rutas 20 de Nov")){
                    items = Camion.RUTAS20NOV;
                }
                if (tipoRutaDato.equals("Rutas Blancas")){
                    items = Camion.RUTASBLANCAS;
                }
                if (tipoRutaDato.equals("Rutas Azul/Amarillo")){
                    items = Camion.RUTASAZULESAMARILLAS;
                }
                if (tipoRutaDato.equals("Rutas Amarillas")){
                    items = Camion.RUTASAMARILLAS;
                }
            }

        }

        adaptador = new AdaptadorRutas(items);
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(v.getId() == R.id.img_ruta_favorita){
                    int ps = (int)v.getTag();
                    Log.e("!!!!!!!!!!!!!",String.valueOf(ps));
                    Toast.makeText(getActivity(), String.valueOf(ps), Toast.LENGTH_SHORT).show();
                    ImageView imgv = (ImageView) v;
                    imgv.setImageResource(R.drawable.ic_star_white_36dp);
                    v.startAnimation(alphaAnimationShowIcon);
                    v.startAnimation(alphaAnimationShowIcon);

                }
                else {
                    int posicion = reciclador.getChildAdapterPosition(v);
                    Fragment fragmentoGenerico = null;
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

                    fragmentoGenerico = new FragmentoRutaMapa();

                    Bundle bundle = new Bundle();
                    bundle.putString("ruta", String.valueOf(items.get(posicion).getNombre()));
                /*TODO Como enviamos el nombre de la ruta hay que sacarla de la clase Ruta
                * y de la lista RUTAS verificar en un for cual es la que tiene ese nombre
                * todo esto se hara en el fragmento al que lo enviamos en este caso al fragmento Ruta Mapa*/
                    fragmentoGenerico.setArguments(bundle);

                    if (fragmentoGenerico != null) {
                        ft.replace(R.id.contenedor_principal, fragmentoGenerico,"FragmentoRutaMapa").addToBackStack(null).commit();
                    /*fragmentManager
                            .beginTransaction()
                            .replace(R.id.contenedor_principal, fragmentoGenerico)
                            .commit();*/
                    }
                }


            }
        });
        reciclador.setAdapter(adaptador);

        return view;
    }

}

