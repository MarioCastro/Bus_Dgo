package com.sooreck.busdgo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SoOreck on 16/06/2016.
 */
public class Camion {

    private String nombre;
    private String precios;
    private int imagen;
    private String tipo; /*Equivalente al nombre de TipoRuta*/
    private int identificadorRuta;

    public Camion(String nombre, String precios, int imagen, String tipo, int identificador){

        this.nombre = nombre;
        this.precios = precios;
        this.imagen = imagen;
        this.tipo = tipo;
        this.identificadorRuta = identificador;

    }
    public static final List<Camion> NOHAY = new ArrayList<>();
    public static final List<Camion> RUTASLYN = new ArrayList<>();
    public static final List<Camion> RUTAS20NOV = new ArrayList<>();
    public static final List<Camion> RUTASBLANCAS = new ArrayList<>();
    public static final List<Camion> RUTASAZULESAMARILLAS = new ArrayList<>();
    public static final List<Camion> RUTASAMARILLAS = new ArrayList<>();

    static {
        NOHAY.add(new Camion("No hay rutas","",R.drawable.header,"",-1));
        RUTASLYN.add(new Camion("Ruta Libertad", "Normal $8.50 - Credencial $5.00", R.drawable.header,"Libertad/Nazas",0));
        RUTASLYN.add(new Camion("Ruta Nazas", "Normal $8.50 - Credencial $5.00", R.drawable.header,"Libertad/Nazas",1));
        RUTAS20NOV.add(new Camion("Ruta 20 de Noviembre - Tecno", "Normal $9.00 - Credencial $4.00", R.drawable.header,"Rutas 20 de Nov",2));
        RUTAS20NOV.add(new Camion("Ruta 20 de Noviembre - Cruz Roja", "Normal $9.00 - Credencial $4.00", R.drawable.header,"Rutas 20 de Nov",3));
        RUTASBLANCAS.add(new Camion("Ruta 1", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Blancas",4));
        RUTASBLANCAS.add(new Camion("Ruta 2", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Blancas",5));
        RUTASBLANCAS.add(new Camion("Ruta 3", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Blancas",6));
        RUTASAZULESAMARILLAS.add(new Camion("Ruta 5 de Febrero - Azteca", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Azul/Amarillo",7));
        RUTASAZULESAMARILLAS.add(new Camion("Ruta Plazuela - Azteca", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Azul/Amarillo",8));
        RUTASAMARILLAS.add(new Camion("Ruta Hipodromo", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Amarillas",19));
        RUTASAMARILLAS.add(new Camion("Ruta Santa Fe - Libramiento", "Normal $9.00 - Credencial $4.00", R.drawable.header2,"Rutas Amarillas",20));

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecios() {
        return precios;
    }

    public void setPrecios(String precios) {
        this.precios = precios;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdentificadorRuta() {
        return identificadorRuta;
    }

    public void setIdentificadorRuta(int identificadorRuta) {
        this.identificadorRuta = identificadorRuta;
    }
}
