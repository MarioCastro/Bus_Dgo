package com.sooreck.busdgo;

/**
 * Created by SoOreck on 16/06/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Adaptador para los tipos de rutas usadas en la sección "Rutas para dividirlo por categorias"
 */


public class AdaptadorTipoRuta extends RecyclerView.Adapter<AdaptadorTipoRuta.ViewHolder> implements View.OnClickListener{

    private final List<TipoRuta> items;
    private View.OnClickListener listener;


    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

public static class ViewHolder extends RecyclerView.ViewHolder {
    // Campos respectivos de un item
    public TextView nombre;
    public TextView color;
    public ImageView imagen;

    public ViewHolder(View v) {
        super(v);

        nombre = (TextView) v.findViewById(R.id.nombre_ruta);
        color = (TextView) v.findViewById(R.id.color_ruta);
        imagen = (ImageView) v.findViewById(R.id.miniatura_tipo_camion);
    }
}


    public AdaptadorTipoRuta(List<TipoRuta> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_tipos, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        TipoRuta item = items.get(i);

        Glide.with(viewHolder.itemView.getContext())
                .load(item.getImagen())
                .centerCrop()
                .into(viewHolder.imagen);
        viewHolder.nombre.setText(item.getNombre());
        viewHolder.color.setText(item.getColorAutobus());
        viewHolder.itemView.setOnClickListener(this);

    }


}

