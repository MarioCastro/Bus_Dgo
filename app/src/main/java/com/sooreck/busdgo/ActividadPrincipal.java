package com.sooreck.busdgo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

public class ActividadPrincipal extends AppCompatActivity implements FragmentoRutas.DataPassListener {

    private DrawerLayout drawerLayout;
    public static NavigationView navigationView;
    public static Menu Menu;

    private static final String LOG_TAG = "PlaceSelectionListener";
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    public static final int REQUEST_SELECT_PLACE = 1000;
    public static final int REQUEST_REPORT = 2;
    public static float densidad;
    private AlphaAnimation alphaAnimationShowIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);

        agregarToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
         navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            prepararDrawer(navigationView);
            // Seleccionar item por defecto
            seleccionarItem(navigationView.getMenu().getItem(0));
        }

        SharedPreferences prefs =  getSharedPreferences("StarterData", Context.MODE_PRIVATE);
        String user = prefs.getString("user","");
        View headerLayout = navigationView != null ? navigationView.getHeaderView(0) : null;
        TextView userHeader = (TextView) (headerLayout != null ? headerLayout.findViewById(R.id.user_header) : null);
        if (userHeader != null) {
            userHeader.setText(user);
        }
        ImageView header_image = (ImageView) (headerLayout != null ? headerLayout.findViewById(R.id.drawer_header_image) : null);
        if (header_image != null) {
            Random rndm = new Random();
            int rnd = rndm.nextInt(7);
            switch (rnd){
                case 0:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_0));
                    break;
                case 1:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_1));
                    break;
                case 2:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_2));
                    break;
                case 3:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_3));
                    break;
                case 4:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_4));
                    break;
                case 5:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_5));
                    break;
                case 6:
                    header_image.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.drawer_header_6));
                    break;
            }

        }

        densidad = getResources().getDisplayMetrics().density;
        alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);
    }

    /***********************************************************************************************
     ***************************** METODO PARA AGREGAR LA TOOLBAR *********************************/
    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    /***********************************************************************************************
     ***** METODO PARA RECUPERAR DATOS DE UNA ACTIVIDAD LLAMADA DESDE UN FRAGMENT O ACTIVIDAD *****/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActividadPrincipal.REQUEST_SELECT_PLACE){
            Place place = PlacePicker.getPlace(data, this);
            if (place != null){
                FragmentoDondeVamos fragment = (FragmentoDondeVamos) getSupportFragmentManager().findFragmentByTag("FragmentoDondeVamos");
                fragment.marcarPuntoObtenido(place.getLatLng());
            }

        }

        if (requestCode == ActividadPrincipal.REQUEST_REPORT){
            navigationView.setCheckedItem(R.id.item_inicio);
            setTitle(R.string.item_inicio);

        }

    }

    /***********************************************************************************************
     ************************* EVENTO AL PRESIONAR EL BOTON DE REGRESAR ***************************/
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        FragmentManager manager = getSupportFragmentManager();
        String tagFragmentActual = "";
        if (manager.getBackStackEntryCount() == 0){
            tagFragmentActual="FragmentoInicio";
        } else tagFragmentActual=manager.getFragments().get(manager.getBackStackEntryCount()-1).getTag();

        if (tagFragmentActual != null){
            if (tagFragmentActual.equals("")){
                navigationView.setCheckedItem(R.id.item_inicio);
                setTitle(R.string.item_inicio);
                super.onBackPressed();
            } else

            if (tagFragmentActual.equals("FragmentoRutasXD") || tagFragmentActual.equals("FragmentoMarcarRuta") ||
                    tagFragmentActual.equals("FragmentoDondeVamos") || tagFragmentActual.equals("FragmentoAcercaDe")){
             Fragment fragmentoGenerico = null;

            if (manager.getBackStackEntryCount() > 0){

                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

            }
            String tag="FragmentoInicio";
            fragmentoGenerico = new FragmentoInicioMenu();
                manager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico,tag)
                        .commit();

                // Setear título actual
                navigationView.setCheckedItem(R.id.item_inicio);
                setTitle(R.string.item_inicio);

            } else {
                navigationView.setCheckedItem(R.id.item_inicio);
                setTitle(R.string.item_inicio);
                super.onBackPressed();
            }
        }
       else{
            navigationView.setCheckedItem(R.id.item_inicio);
            setTitle(R.string.item_inicio);
            super.onBackPressed();
        }

        /*
        if (manager.getBackStackEntryCount() == 0){
            if (manager.getFragments().get(0).getTag().equals("")){

            }

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActividadPrincipal.this);
                    alertDialogBuilder.setTitle("Bus Durango");
                    alertDialogBuilder.setIcon(R.drawable.ic_doulbecheck);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("¿Desea salir de la aplicación?")
                            .setCancelable(true)
                            .setPositiveButton("SI",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            })
                            .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

        } else super.onBackPressed(); */

    }

    /***********************************************************************************************
     ************************** METODO PARA INFLAR EL MENU DE OPCIONES ****************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Menu = menu;
        getMenuInflater().inflate(R.menu.actividad_principal, menu);
        return true;
    }

    /***********************************************************************************************
     ****************** EVENTO AL SELECCIONAR UNA OPCION DE LA BARRA DE TAREAS ********************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_search:
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder
                            (PlaceAutocomplete.MODE_OVERLAY)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .build(ActividadPrincipal.this);
                    startActivityForResult(intent, REQUEST_SELECT_PLACE);
                } catch (GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.action_fav:
                FragmentoRutaMapa fragment = (FragmentoRutaMapa) getSupportFragmentManager().findFragmentByTag("FragmentoRutaMapa");
                fragment.favoritosEvent(item);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /***********************************************************************************************
     ******** METODO PARA AGREGAR Y PONER EL NAVIGATIONVIEW Y SELECCIONAR EL PRIMER ITEM **********/
    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

    }

    private void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0){

            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

        }
        String tag = "";
        switch (itemDrawer.getItemId()) {
            case R.id.item_inicio:
                fragmentoGenerico = new FragmentoInicioMenu();
                tag="FragmentoInicio";
                break;
            case R.id.item_rutas:
                fragmentoGenerico = new FragmentoRutas();
                tag="FragmentoRutas";
                break;
            case R.id.item_rutas_favoritas:
                SharedPreferences prefss =  getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
                String rutasfavs = prefss.getString("rutas","");
                ArrayList<String> rutas;

                if (!rutasfavs.equals("")){
                    Type listType = new TypeToken<ArrayList<String>>(){}.getType();
                    rutas = new Gson().fromJson(rutasfavs,listType);
                    if (rutas.size()>0){
                        fragmentoGenerico = new FragmentoRutasFavoritas();
                        tag="FragmentoRutasFavoritas";
                    }else {
                        fragmentoGenerico = new FragmentoNoFav();
                        tag="FragmentoNoFav";
                    }

                }else{
                    fragmentoGenerico = new FragmentoNoFav();
                    tag="FragmentoNoFav";
                }

                break;
            case R.id.item_configuracion:
                fragmentoGenerico = new FragmentoMarcarRuta();
                tag="FragmentoMarcarRuta";
                break;
            case R.id.item_adondevamos:
                fragmentoGenerico = new FragmentoDondeVamos();
                tag="FragmentoDondeVamos";
                break;
            case R.id.item_dondeestoy:
                FragmentManager fragmentManagerw = getSupportFragmentManager();
                DialogFragment dialogo = new DialogoNoRutas();
                dialogo.show(fragmentManagerw, "NoRutasDialog");
                break;
            case R.id.item_reportarproblema:
                SharedPreferences prefs =  getSharedPreferences("StarterData", Context.MODE_PRIVATE);
                String user = prefs.getString("user","");
                navigationView.setCheckedItem(R.id.item_inicio);

                Intent i = new Intent(Intent.ACTION_SENDTO);
                String TextoUri = "mailto:"+ Uri.encode("soporte.busdurango@gmail.com") + "?subject="+Uri.encode("REPORTE "+user);
                Uri uri = Uri.parse(TextoUri);
                i.setData(uri);
                startActivityForResult(Intent.createChooser(i,"Enviar Email"),REQUEST_REPORT);
                break;
            case R.id.item_acercade:
                fragmentoGenerico = new FragmentoAcercaDe();
                tag="FragmentoAcercaDe";
                break;
        }

        if (itemDrawer.getItemId() == navigationView.getMenu().getItem(0).getItemId()){
            if (fragmentoGenerico != null) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico, tag)
                        .commit();

            }
        } else {
            if (fragmentoGenerico != null) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.contenedor_principal, fragmentoGenerico,tag)
                        .addToBackStack(null)
                        .commit();
            }
        }

        // Setear título actual
        if (!(itemDrawer.getItemId() == R.id.item_reportarproblema)){
            setTitle(itemDrawer.getTitle());
        }

    }

    private void emptyBackStack(){
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0){

            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

        }
    }

    @Override
    public void passData(String dato) {
        FragmentoIntoRutas fir = new FragmentoIntoRutas();
        Bundle args = new Bundle();
        args.putString("tipo",dato);
        fir.setArguments(args);
    }

    public static void showItemMenu(boolean show,boolean show2){
        if (Menu == null)
            return;
        Menu.setGroupVisible(R.id.a_donde_vamos_group,show);
        Menu.setGroupVisible(R.id.rutas_fav_group,show2);
    }//Mostrar o esconder grupo item Menu

    private void iniciaActividadInicio(){
        Intent i = new Intent(ActividadPrincipal.this, ActividadPrincipal.class);
        finish();
        startActivity(i);
    }

    public static void switchIcon(boolean isFav){
        if (isFav)
            Menu.findItem(R.id.action_fav).setIcon(R.drawable.ic_star_white_24dp);
        else Menu.findItem(R.id.action_fav).setIcon(R.drawable.ic_star_outline_white_24dp);

    }
}
