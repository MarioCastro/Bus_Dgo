package com.sooreck.busdgo;

/**
 * Created by SoOreck on 08/10/2016.
 */
public class ItemInicioMenu {
    private String titulo;
    private int imagen;

    public ItemInicioMenu(){

    }

    public ItemInicioMenu(String titulo, int imagen){
        this.titulo = titulo;
        this.imagen = imagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getImagen() {
        return imagen;
    }
}
