package com.sooreck.busdgo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by SoOreck on 17/06/2016.
 */
public class FragmentoMarcarRuta extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener{

    private static final int LOCATION_REQUEST_CODE = 1;
    GoogleMap mapa;
    EditText etnewdirection;

    MarkerOptions nuevascoordenadas;
    Marker longclick;

    Circle marcador;

    Polyline rutarClickeada;
    int colorRuta;
    float widtRuta;

    ArrayList <LatLng> puntosTemp;
    ArrayList <LatLng> puntos;
    PolylineOptions rutaQueSeEstaMarcando;
    Polyline ruta;
    LatLng punto;

    private static final int REQUEST_WRITE = 1 ;


    public FragmentoMarcarRuta() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_marcar_ruta, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);

        etnewdirection = (EditText) view.findViewById(R.id.et_nuevo_punto);

        FloatingActionButton fbAdd = (FloatingActionButton) view.findViewById(R.id.add);
        FloatingActionButton fbRemove = (FloatingActionButton) view.findViewById(R.id.remove);

        fbAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               puntos.add(punto);
                //Todo escribir en el archivo.
                escribirAchivo(String.valueOf(punto.latitude+","+String.valueOf(punto.longitude)));
                Toast.makeText(getActivity(), "Punto Guardado", Toast.LENGTH_SHORT).show();

            }
        });

        fbRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escribirAchivo("//////");
                LatLng pila = puntos.get(puntos.size()-2);
                puntos.remove(puntos.get(puntos.size()-1));
                marcaRuta(pila);
                Toast.makeText(getActivity(), "Removido Punto Final", Toast.LENGTH_SHORT).show();

            }
        });


        return view;
    }


    @Override
    public void onMapReady(final GoogleMap map) {
        //Toast.makeText(getActivity(), String.valueOf("Iniciando Mapa"), Toast.LENGTH_SHORT).show();
        mapa = map;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapa.setOnMarkerClickListener(this);
        mapa.setOnMapClickListener(this);
        puntos = new ArrayList<>();

        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE);

            }
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Mostrar diálogo explicativo
            } else {
                // Solicitar permiso
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }
        }

       /* exampleLocation = new LatLng(24.009232, -104.661077);
        map.addMarker(new MarkerOptions().position(exampleLocation)
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                .title("Ejemplo de localizacion")
                .snippet("Here!")
                .draggable(false)
        );*/
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(24.009232, -104.661077), 13.0f));

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(final LatLng point) {
                if (longclick != null){
                    longclick.remove();
                    longclick = null;
                }
                if (marcador != null){
                    marcador.remove();
                    marcador = null;
                }

                Projection proj = map.getProjection();
                Point coord = proj.toScreenLocation(point);

               /* Toast.makeText(
                        getActivity(),
                                "Lat: " + point.latitude + "\n" +
                                "Lng: " + point.longitude + "\n" +
                                "X: " + coord.x + " - Y: " + coord.y,
                        Toast.LENGTH_SHORT).show();*/

                nuevascoordenadas = new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.house_flag))
                        .title("Nuvo punto")
                        .snippet("prosigue la ruta")
                        .draggable(false);
                longclick = map.addMarker(nuevascoordenadas);
                pintarCirculo(new LatLng(point.latitude, point.longitude),40f);
                etnewdirection.setText("");
                etnewdirection.setText(String.valueOf(point.latitude) + ", " + String.valueOf(point.longitude));
                punto = point;
                marcaRuta(point);

            }
        });

        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    Location location = map.getMyLocation();
                    // etnewdirection.setText(String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));

                } catch (NullPointerException ex) {
                    Toast.makeText(
                            getActivity(),
                            "Revisa que este activado tu GPS\n" +
                                    "e intenta de nuevo",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        map.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
            @Override
            public void onPolylineClick(Polyline polyline) {
                rutarClickeada = polyline;
                colorRuta = rutarClickeada.getColor();
                widtRuta = rutarClickeada.getWidth();
                rutarClickeada.setColor(Color.rgb(255,255,255));
            }
        });
    }


    /***********************************************************************************************
     ****************** EVENTO QUE SE EJECUTA AL RECIBIR UNA RESPUESTA DE PERMISOS ****************/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            // ¿Permisos asignados?
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mapa.setMyLocationEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Error de permisos", Toast.LENGTH_SHORT).show();
            }

        }

        if (requestCode == REQUEST_WRITE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

            }
        }
    }

    /***********************************************************************************************
     ******************* METODO PARA PINTAR CIRCULO AL REDEDOR DE UN MARCADOR *********************/
    private void pintarCirculo(LatLng center, double radius){
        if (marcador != null){
            marcador.remove();
            marcador = null;
        }
        CircleOptions circleOptions = new CircleOptions()
                .center(center)
                .radius(radius)
                .strokeColor(Color.parseColor("#FFFFFF"))
                .strokeWidth(4)
                .fillColor(Color.argb(32, 33, 150, 243));
        marcador = mapa.addCircle(circleOptions);
    }

    /***********************************************************************************************
     ********** EVENTO QUE SE EJECUTA AL DAR CLIK EN UN MARCADOR PARA PINTAR UN CIRCULO ***********/
    @Override
    public boolean onMarkerClick(Marker marker) {
        LatLng pos = new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);
        pintarCirculo(pos,40);
        return false;
    }

    /***********************************************************************************************
     ******** EVENTO QUE SE EJECUTA AL DAR CLIK EN EL MAPA PARA QUITAR EL CIRCULO SI EXISTE *******/
    @Override
    public void onMapClick(LatLng latLng) {
        if (marcador != null){
            marcador.remove();
            marcador = null;
        }
        if (rutarClickeada != null){
            rutarClickeada.setColor(colorRuta);
            rutarClickeada.setWidth(widtRuta);
            rutarClickeada = null;
        }
    }


    private void marcaRuta(LatLng latLng){
        if (ruta != null){
            ruta.remove();
            ruta=null;
        }
        rutaQueSeEstaMarcando = new PolylineOptions();
        rutaQueSeEstaMarcando.addAll(puntos);
        rutaQueSeEstaMarcando.add(latLng);
        ruta = mapa.addPolyline(rutaQueSeEstaMarcando);
    }


    private void escribirAchivo(String datos){
        String contenido = leerArchivo();
        try {
            File tarjeta = Environment.getExternalStorageDirectory();
            File file = new File(tarjeta.getAbsolutePath(), "ruta.txt");
            OutputStreamWriter osw = new OutputStreamWriter(
                    new FileOutputStream(file));
            osw.write(contenido+".add(new LatLng("+datos+"))\n");
            osw.flush();
            osw.close();
        } catch (IOException ioe) {
        }

    }

    private String leerArchivo(){
        String todo="";
        File tarjeta = Environment.getExternalStorageDirectory();
        File file = new File(tarjeta.getAbsolutePath(), "ruta.txt");
        try {
            FileInputStream fIn = new FileInputStream(file);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            while (linea != null) {
                todo = todo + linea + "\n";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
        }

        return todo;
    }



}//termina clase