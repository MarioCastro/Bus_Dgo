package com.sooreck.busdgo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by SoOreck on 08/10/2016.
 */
public class FragmentoInicioMenu extends Fragment {

    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorInicioMenu adaptador;
    private TextView tvMsg;
    private String[] msgs = {"Bienvenido :)","Buen día :D","Busca una ruta","Actualizaciones periodicas",
                            "Disfruta de la aplicacion busca una ruta cerca en dos puntos","Compartela","Aqui nomas haciendo pruebas haber de que tama;o aguanta esta cosa jaja prueba pruena yo digo simon simonm"};

    public FragmentoInicioMenu() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_inicio_menu, container, false);
        reciclador = (RecyclerView) view.findViewById(R.id.reciclador_inicio_menu);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        reciclador.setLayoutManager(layoutManager);
        CardView cv = (CardView) view.findViewById(R.id.card_footer);
        tvMsg = (TextView) view.findViewById(R.id.tv_msg);

        Random rndm = new Random();
        int rnd = rndm.nextInt(7);
        tvMsg.setText(msgs[rnd]);

        tvMsg.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tvMsg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                //reciclador.setPadding(0,0,0,tvMsg.getHeight()+60);
                reciclador.setPadding(0,0,0,tvMsg.getHeight()+((int)(16*ActividadPrincipal.densidad)));

            }
        });

        final ArrayList<ItemInicioMenu> items = new ArrayList<>();
        items.add(new ItemInicioMenu("Rutas",R.drawable.ic_menu_rutas) );
        items.add(new ItemInicioMenu("Encuentra una ruta",R.drawable.ic_menu_donde_vamos) );
        items.add(new ItemInicioMenu("Rutas favoritas",R.drawable.ic_menu_fav) );
        items.add(new ItemInicioMenu("Lugares de interes",R.drawable.ic_menu_lugares) );
        items.add(new ItemInicioMenu("Ajustes",R.drawable.ic_menu_ajustes) );
        items.add(new ItemInicioMenu("Acerca de",R.drawable.ic_menu_acercade));

        adaptador = new AdaptadorInicioMenu(items);
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentoGenerico = null;
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter,R.anim.pop_exit);

                int posicion = reciclador.getChildAdapterPosition(v);
                String tag = "";
                switch (posicion){
                    case 0:
                        fragmentoGenerico = new FragmentoRutas();
                        tag="FragmentoRutas";
                        ActividadPrincipal.navigationView.setCheckedItem(R.id.item_rutas);
                        getActivity().setTitle(R.string.item_rutas);
                        break;
                    case 1:
                        fragmentoGenerico = new FragmentoDondeVamos();
                        tag="FragmentoDondeVamos";
                        ActividadPrincipal.navigationView.setCheckedItem(R.id.item_adondevamos);
                        getActivity().setTitle(R.string.item_adondevamos);
                        break;
                    case 2:
                        SharedPreferences prefss =  getActivity().getSharedPreferences("rutasFav", Context.MODE_PRIVATE);
                        String rutasfavs = prefss.getString("rutas","");
                        ArrayList<String> rutas;

                        if (!rutasfavs.equals("")){
                            Type listType = new TypeToken<ArrayList<String>>(){}.getType();
                            rutas = new Gson().fromJson(rutasfavs,listType);
                            if (rutas.size()>0){
                                fragmentoGenerico = new FragmentoRutasFavoritas();
                                tag="FragmentoRutasFavoritas";
                            }else {
                                fragmentoGenerico = new FragmentoNoFav();
                                tag="FragmentoNoFav";
                            }

                        }else{
                            fragmentoGenerico = new FragmentoNoFav();
                            tag="FragmentoNoFav";
                        }
                        ActividadPrincipal.navigationView.setCheckedItem(R.id.item_rutas_favoritas);
                        getActivity().setTitle(R.string.item_rutas_favoritas);
                        break;
                    case 5:
                        fragmentoGenerico = new FragmentoAcercaDe();
                        tag="FragmentoAcercaDe";
                        ActividadPrincipal.navigationView.setCheckedItem(R.id.item_acercade);
                        getActivity().setTitle(R.string.item_acercade);
                        break;
                }
                if (fragmentoGenerico != null) {
                   // ft.replace(R.id.contenedor_principal, fragmentoGenerico).addToBackStack(null).commit();
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.contenedor_principal, fragmentoGenerico,tag)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
        reciclador.setAdapter(adaptador);

        return view;
    }



}
