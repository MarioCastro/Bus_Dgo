package com.sooreck.busdgo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by SoOreck on 08/10/2016.
 */
public class AdaptadorInicioMenu extends RecyclerView.Adapter<AdaptadorInicioMenu.ViewHolder> implements View.OnClickListener {

    private List<ItemInicioMenu> items;
    private View.OnClickListener listener;

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView titulo;
        public ImageView img;

        public ViewHolder(View v) {
            super(v);
            titulo = (TextView) v.findViewById(R.id.title_menu);
            img = (ImageView) v.findViewById(R.id.img_menu);
        }
    }

    public AdaptadorInicioMenu(List<ItemInicioMenu> items) {
        this.items = items;
    }

    @Override
    public AdaptadorInicioMenu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_inicio_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorInicioMenu.ViewHolder holder, int position) {
        ItemInicioMenu item = items.get(position);
        holder.titulo.setText(item.getTitulo());
        Glide.with(holder.itemView.getContext())
                .load(item.getImagen())
                //.centerCrop()
                .crossFade()
                .into(holder.img);
        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}